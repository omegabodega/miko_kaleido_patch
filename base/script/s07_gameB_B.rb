require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_s07_gameB_B(s)
  s.label :addr_0x93113
  s.ins MOV, byte(0), ushort(137), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's07_gameB_B01 start.', byte(0)
  s.label :game_07_gameB後半01
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x664a, [112, 0, 0, 100]
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x6f17, [3, 100, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4889), byte(0), line(s, 4889)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 51, 100, 0, 0]
  s.ins CALL, :f_se_fadeout, [3, 500]
  s.ins MSGSET, uint(4890), byte(127), line(s, 4890)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [0, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x93246
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x93246
  s.ins MOV, byte(0), ushort(138), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B02 start.', byte(0)
  s.label :game_07_gameB後半02
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x6592, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [12, :null]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 0, 9, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x6f17, [3, 70, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4891), byte(0), line(s, 4891)
  s.ins MSGSYNC, byte(0), 2485
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 100, 0]
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins CALL, :f_se_play, [1, 61, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [37, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4892), byte(127), line(s, 4892)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4893), byte(127), line(s, 4893)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4894), byte(127), line(s, 4894)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [0, 2000]
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x93481
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x93481
  s.ins MOV, byte(0), ushort(139), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B03 start.', byte(0)
  s.label :game_07_gameB後半03
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x660e, [0, :null]
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_se_play, [0, 31, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [0, 57, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [700]
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :f_se_play, [3, 58, 100, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [473]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 6000]
  s.ins CALL, :addr_0x542b, [30, 6000]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2200]
  s.ins CALL, :addr_0x542b, [30, 2200]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4895), byte(3), line(s, 4895)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 6000]
  s.ins CALL, :addr_0x542b, [30, 6000]
  s.ins MSGSET, uint(4896), byte(3), line(s, 4896)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 50, 2000]
  s.ins CALL, :f_se_play, [0, 61, 100, 0, 0]
  s.ins CALL, :f_se_play, [4, 62, 100, 0, 0]
  s.ins MSGSET, uint(4897), byte(127), line(s, 4897)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4898), byte(127), line(s, 4898)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4899), byte(127), line(s, 4899)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4900), byte(127), line(s, 4900)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [17, :null]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 45, 1, 0, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-899, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 137, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 12288, 0]
  s.ins CALL, :addr_0x532a, [3, 3, 1]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 140, 1, 0, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [900, 0, 12288, 0]
  s.ins CALL, :addr_0x532a, [3, 3, 1]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4901), byte(1), line(s, 4901)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4902), byte(0), line(s, 4902)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4903), byte(2), line(s, 4903)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4904), byte(4), line(s, 4904)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [27, :null]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 12, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4905), byte(2), line(s, 4905)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4906), byte(4), line(s, 4906)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4907), byte(2), line(s, 4907)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 141, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4908), byte(2), line(s, 4908)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4909), byte(4), line(s, 4909)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4910), byte(2), line(s, 4910)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x637e, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 36, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4911), byte(2), line(s, 4911)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 61, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x639f, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [37, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4912), byte(127), line(s, 4912)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [4, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x93cd6
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x93cd6
  s.ins MOV, byte(0), ushort(140), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B04 start.', byte(0)
  s.label :game_07_gameB後半04
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x664a, [459, 0, 0, 100]
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :f_se_play, [3, 62, 100, 1000, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4913), byte(127), line(s, 4913)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4914), byte(4), line(s, 4914)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 61, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [37, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4915), byte(127), line(s, 4915)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x93e4c
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x93e4c
  s.ins MOV, byte(0), ushort(141), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B05 start.', byte(0)
  s.label :game_07_gameB後半05
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :addr_0x660e, [0, :null]
  s.ins CALL, :f_se_play, [1, 10, 100, 0, 0]
  s.ins CALL, :addr_0x5eae, [11, :null, 0, 2500, 200]
  s.ins CALL, :f_se_play, [0, 11, 100, 0, 0]
  s.ins CALL, :f_se_play, [2, 4, 100, 0, 0]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [599, Register.new(190)]
  s.ins MOV, byte(0), ushort(190), 599
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [125, 125, 250]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4916), byte(1), line(s, 4916)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4917), byte(127), line(s, 4917)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4918), byte(127), line(s, 4918)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4919), byte(127), line(s, 4919)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4920), byte(127), line(s, 4920)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4921), byte(1), line(s, 4921)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4922), byte(127), line(s, 4922)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4923), byte(127), line(s, 4923)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4924), byte(127), line(s, 4924)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 16, 100, 0, 0]
  s.ins MSGSET, uint(4925), byte(127), line(s, 4925)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 139, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4926), byte(2), line(s, 4926)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(190)]
  s.ins MOV, byte(0), ushort(190), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 7, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4927), byte(0), line(s, 4927)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4928), byte(0), line(s, 4928)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4929), byte(127), line(s, 4929)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4930), byte(127), line(s, 4930)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4931), byte(0), line(s, 4931)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-4999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [3000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [476]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4932), byte(127), line(s, 4932)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4933), byte(127), line(s, 4933)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins MSGSET, uint(4934), byte(0), line(s, 4934)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [203]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4935), byte(2), line(s, 4935)
  s.ins MSGSYNC, byte(0), 1162
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 3000]
  s.ins CALL, :addr_0x542b, [30, 3000]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4936), byte(0), line(s, 4936)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4937), byte(0), line(s, 4937)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [206, 0, 0, 100]
  s.ins MSGSET, uint(4938), byte(2), line(s, 4938)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Register.new(26)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MSGSET, uint(4939), byte(0), line(s, 4939)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4940), byte(0), line(s, 4940)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [221, 0, -199, 150]
  s.ins MSGSET, uint(4941), byte(2), line(s, 4941)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4942), byte(2), line(s, 4942)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [18, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [80, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [3, 62, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins MSGSET, uint(4943), byte(127), line(s, 4943)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4944), byte(127), line(s, 4944)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [459, 0, 0, 100]
  s.ins MSGSET, uint(4945), byte(127), line(s, 4945)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 70, 2000]
  s.ins MSGSET, uint(4946), byte(0), line(s, 4946)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4947), byte(4), line(s, 4947)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 100, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [37, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4948), byte(127), line(s, 4948)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4949), byte(127), line(s, 4949)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x94a86
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x94a86
  s.ins MOV, byte(0), ushort(142), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B06 start.', byte(0)
  s.label :game_07_gameB後半06
  s.ins CALL, :addr_0x6592, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [16, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [95, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 108, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 212, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins CALL, :addr_0x6f17, [3, 100, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4950), byte(2), line(s, 4950)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :f_se_play, [0, 60, 100, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4951), byte(3), line(s, 4951)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 0, 10, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4952), byte(0), line(s, 4952)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4953), byte(0), line(s, 4953)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4954), byte(0), line(s, 4954)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4955), byte(127), line(s, 4955)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_se_play, [0, 11, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x94e4f
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x94e4f
  s.ins MOV, byte(0), ushort(143), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B07 start.', byte(0)
  s.label :game_07_gameB後半07
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x664a, [235, 0, 0, 100]
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x6f17, [3, 100, 0]
  s.ins CALL, :f_se_play, [4, 59, 100, 1000, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4956), byte(2), line(s, 4956)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4957), byte(4), line(s, 4957)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4958), byte(2), line(s, 4958)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4959), byte(2), line(s, 4959)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4960), byte(2), line(s, 4960)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :f_se_fadeout, [4, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x95017
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x95017
  s.ins MOV, byte(0), ushort(144), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B08 start.', byte(0)
  s.label :game_07_gameB後半08
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x660e, [0, :null]
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_se_play, [0, 31, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [0, 57, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [700]
  s.ins CALL, :f_bgm_play, [9, 100, 0, 1]
  s.ins CALL, :f_se_play, [3, 58, 100, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [480]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 6000]
  s.ins CALL, :addr_0x542b, [30, 6000]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4961), byte(1), line(s, 4961)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4962), byte(127), line(s, 4962)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4963), byte(127), line(s, 4963)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4964), byte(127), line(s, 4964)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins MSGSET, uint(4965), byte(127), line(s, 4965)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 67, 100, 0, 0]
  s.ins MSGSET, uint(4966), byte(2), line(s, 4966)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4967), byte(3), line(s, 4967)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 52, 100, 0, 0]
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins MSGSET, uint(4968), byte(0), line(s, 4968)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [459, 0, 0, 100]
  s.ins MSGSET, uint(4969), byte(2), line(s, 4969)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4970), byte(3), line(s, 4970)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4971), byte(4), line(s, 4971)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 62, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [17, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-5999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [500, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4972), byte(127), line(s, 4972)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4973), byte(127), line(s, 4973)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4974), byte(127), line(s, 4974)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [158, 0, 0, 100]
  s.ins MSGSET, uint(4975), byte(127), line(s, 4975)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4976), byte(127), line(s, 4976)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4977), byte(1), line(s, 4977)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4978), byte(1), line(s, 4978)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4979), byte(0), line(s, 4979)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 60, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [400, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 400
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [480, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4980), byte(0), line(s, 4980)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4981), byte(0), line(s, 4981)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 100, 1000]
  s.ins CALL, :addr_0x574d, [37, :null, 0, 0, 100, 0]
  s.ins CALL, :f_se_play, [0, 61, 100, 0, 0]
  s.ins MSGSET, uint(4982), byte(127), line(s, 4982)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4983), byte(127), line(s, 4983)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x95781
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x95781
  s.ins MOV, byte(0), ushort(145), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B09 start.', byte(0)
  s.label :game_07_gameB後半09
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x664a, [217, 0, 0, 100]
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x6f17, [3, 100, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4984), byte(2), line(s, 4984)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4985), byte(2), line(s, 4985)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4986), byte(127), line(s, 4986)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x958df
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x958df
  s.ins MOV, byte(0), ushort(146), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B10 start.', byte(0)
  s.label :game_07_gameB後半10
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x6592, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [267]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [400, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins LAYERSELECT, 31, 31
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [599, Register.new(190)]
  s.ins MOV, byte(0), ushort(190), 599
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x6f17, [3, 100, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4987), byte(4), line(s, 4987)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4988), byte(3), line(s, 4988)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4989), byte(3), line(s, 4989)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4990), byte(3), line(s, 4990)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4991), byte(3), line(s, 4991)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4992), byte(127), line(s, 4992)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4993), byte(127), line(s, 4993)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x95bf3
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x95bf3
  s.ins MOV, byte(0), ushort(147), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B11 start.', byte(0)
  s.label :game_07_gameB後半11
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x664a, [91, 0, 0, 100]
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x6f17, [3, 100, 0]
  s.ins CALL, :f_se_play, [0, 61, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4994), byte(127), line(s, 4994)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [168, 0, 0, 100]
  s.ins MSGSET, uint(4995), byte(1), line(s, 4995)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4996), byte(1), line(s, 4996)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4997), byte(4), line(s, 4997)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4998), byte(1), line(s, 4998)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 64, 100, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [157]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [105, 0, 0, 0]
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :addr_0x4df4, [100]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [5, 5, 400]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [1200, 400, 12291, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-499, 400, 12291, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 400, 12291, 0]
  s.ins MSGSET, uint(4999), byte(1), line(s, 4999)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x574d, [37, :null, 0, 0, 100, 0]
  s.ins MSGSET, uint(5000), byte(127), line(s, 5000)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x95ed2
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x95ed2
  s.ins MOV, byte(0), ushort(148), 1
  s.ins JUMP, :addr_0x886c
  s.ins DEBUG, 's07_gameB_B12 start.', byte(0)
  s.label :game_07_gameB後半12
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins CALL, :addr_0x664a, [91, 0, 0, 100]
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x6f17, [3, 100, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(5001), byte(2), line(s, 5001)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(5002), byte(127), line(s, 5002)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4eba, [2000, 1]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x95fd4
  s.ins CALL, :f_reset, []
  s.ins RETSUB
end
