require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_s02_intA01(s)
  s.label :addr_0xe942
  s.ins MOV, byte(0), ushort(68), 1
  s.ins TROPHY, byte(1)
  s.ins JUMP, :addr_0x78af
  s.ins DEBUG, 's02_intA01_01 start.', byte(0)
  s.label :game_02_intA01_01_風華、初めての死刑囚
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x5892, [422, 0, 0, 100]
  s.ins CALL, :f_bgm_play, [8, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(192), byte(0), line(s, 192)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(193), byte(1), line(s, 193)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(194), byte(0), line(s, 194)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(195), byte(1), line(s, 195)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(196), byte(3), line(s, 196)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(197), byte(2), line(s, 197)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [423, 0, 0, 100]
  s.ins MSGSET, uint(198), byte(0), line(s, 198)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(199), byte(3), line(s, 199)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(200), byte(1), line(s, 200)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [422, 0, 0, 100]
  s.ins MSGSET, uint(201), byte(0), line(s, 201)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(202), byte(0), line(s, 202)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(203), byte(3), line(s, 203)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [423, 0, 0, 100]
  s.ins MSGSET, uint(204), byte(0), line(s, 204)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 47, 100, 0, 0]
  s.ins MSGSET, uint(205), byte(3), line(s, 205)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(206), byte(3), line(s, 206)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(207), byte(0), line(s, 207)
  s.ins MSGSYNC, byte(0), 1546
  s.ins CALL, :addr_0x5914, [424, 0, 0, 100]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(208), byte(2), line(s, 208)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(209), byte(2), line(s, 209)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [422, 0, 0, 100]
  s.ins MSGSET, uint(210), byte(0), line(s, 210)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(211), byte(1), line(s, 211)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(212), byte(3), line(s, 212)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(213), byte(1), line(s, 213)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(214), byte(1), line(s, 214)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [423, 0, 0, 100]
  s.ins MSGSET, uint(215), byte(0), line(s, 215)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [424, 0, 0, 100]
  s.ins MSGSET, uint(216), byte(0), line(s, 216)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0xf074
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0xf074
  s.ins MOV, byte(0), ushort(70), 1
  s.ins JUMP, :addr_0x8394
  s.ins DEBUG, 's02_intA01_02 start.', byte(0)
  s.label :game_02_intA01_02_火凛、初めての死刑囚
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x58f7, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [30, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 0, 15, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins CALL, :addr_0x5906, []
  s.ins CALL, :f_bgm_play, [7, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(217), byte(1), line(s, 217)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x58f7, []
  s.ins LAYERSELECT, 66, 66
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [491, Register.new(225), 1900]
  s.ins MOV, byte(0), ushort(225), 491
  s.ins CALL, :f_se_play, [0, 10, 100, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 200
  s.ins MOV, byte(0), ushort(13), 11
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [150]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 65, 65
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [495, Register.new(224), 1900]
  s.ins MOV, byte(0), ushort(224), 495
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [554, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 554
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 65, 65
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [495, Register.new(224), 1900]
  s.ins MOV, byte(0), ushort(224), 495
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins CALL, :f_se_play, [0, 17, 100, 0, 0]
  s.ins MSGSET, uint(218), byte(1), line(s, 218)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(219), byte(1), line(s, 219)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x58f7, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 65, 65
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(224)]
  s.ins MOV, byte(0), ushort(224), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 67, 67
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [493, Register.new(226), 1900]
  s.ins MOV, byte(0), ushort(226), 493
  s.ins CALL, :f_se_play, [0, 10, 100, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 200
  s.ins MOV, byte(0), ushort(13), 11
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [150]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 66, 66
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(225)]
  s.ins MOV, byte(0), ushort(225), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 67, 67
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(226)]
  s.ins MOV, byte(0), ushort(226), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [555, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 555
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 200
  s.ins MOV, byte(0), ushort(13), 11
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MSGSET, uint(220), byte(127), line(s, 220)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 86, 100, 0, 0]
  s.ins MSGSET, uint(221), byte(127), line(s, 221)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(222), byte(2), line(s, 222)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(223), byte(0), line(s, 223)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(224), byte(3), line(s, 224)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 1000]
  s.ins MSGSET, uint(225), byte(3), line(s, 225)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 45, 100, 0, 0]
  s.ins CALL, :addr_0x60c6, []
  s.ins MSGSET, uint(226), byte(2), line(s, 226)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x617d, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 0, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-699, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 0, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 0, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [700, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 0, 24, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-249, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(227), byte(0), line(s, 227)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(228), byte(0), line(s, 228)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(229), byte(3), line(s, 229)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(230), byte(1), line(s, 230)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(231), byte(0), line(s, 231)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 5, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(232), byte(1), line(s, 232)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 48, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 68, 68
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [484, Register.new(227), 1900]
  s.ins MOV, byte(0), ushort(227), 484
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(233), byte(1), line(s, 233)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 68, 68
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(227)]
  s.ins MOV, byte(0), ushort(227), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(234), byte(1), line(s, 234)
  s.ins MSGSYNC, byte(0), 2901
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 8042
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 125, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [12, 35, 70]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(235), byte(3), line(s, 235)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(236), byte(1), line(s, 236)
  s.ins MSGSYNC, byte(0), 5205
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 68, 68
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [484, Register.new(227), 1900]
  s.ins MOV, byte(0), ushort(227), 484
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 5, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_se_play, [0, 48, 100, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(237), byte(2), line(s, 237)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 68, 68
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(227)]
  s.ins MOV, byte(0), ushort(227), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 123, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(238), byte(1), line(s, 238)
  s.ins MSGSYNC, byte(0), 4224
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 119, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 7082
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(239), byte(0), line(s, 239)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 46, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1500
  s.ins LAYERSELECT, 35, 35
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [597, Register.new(194)]
  s.ins MOV, byte(0), ushort(194), 597
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 36, 36
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [597, Register.new(195)]
  s.ins MOV, byte(0), ushort(195), 597
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 36, 36
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-109, 400, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 400, 0, 0]
  s.ins MSGSET, uint(240), byte(1), line(s, 240)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x58f7, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 35, 35
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(194)]
  s.ins MOV, byte(0), ushort(194), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 36, 36
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(195)]
  s.ins MOV, byte(0), ushort(195), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 66, 66
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [491, Register.new(225), 1900]
  s.ins MOV, byte(0), ushort(225), 491
  s.ins CALL, :f_se_play, [0, 10, 100, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 200
  s.ins MOV, byte(0), ushort(13), 11
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [150]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 66, 66
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(225)]
  s.ins MOV, byte(0), ushort(225), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 65, 65
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [495, Register.new(224), 1900]
  s.ins MOV, byte(0), ushort(224), 495
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [554, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 554
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 65, 65
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [495, Register.new(224), 1900]
  s.ins MOV, byte(0), ushort(224), 495
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins CALL, :f_se_play, [0, 17, 100, 0, 0]
  s.ins MSGSET, uint(241), byte(1), line(s, 241)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x103e2
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x103e2
  s.ins MOV, byte(0), ushort(71), 1
  s.ins JUMP, :addr_0x8394
  s.ins DEBUG, 's02_intA01_03 start.', byte(0)
  s.label :game_02_intA01_03_水無、初めての死刑囚
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x574d, [33, 0, 0, 0, 100, 0]
  s.ins CALL, :f_bgm_play, [0, 100, 0, 1]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), -199
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 0, 107, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(242), byte(2), line(s, 242)
  s.ins MSGSYNC, byte(0), 1952
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 104, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 97, 100, 0, 0]
  s.ins MSGSET, uint(243), byte(127), line(s, 243)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 5, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(244), byte(127), line(s, 244)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(245), byte(127), line(s, 245)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [466, 0, 0, 100]
  s.ins CALL, :f_se_play, [0, 98, 100, 0, 0]
  s.ins MSGSET, uint(246), byte(2), line(s, 246)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins MSGSET, uint(247), byte(3), line(s, 247)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(248), byte(1), line(s, 248)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(249), byte(0), line(s, 249)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [465, 0, 0, 100]
  s.ins MSGSET, uint(250), byte(2), line(s, 250)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(251), byte(2), line(s, 251)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(252), byte(3), line(s, 252)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(253), byte(0), line(s, 253)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(254), byte(1), line(s, 254)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(255), byte(2), line(s, 255)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(256), byte(2), line(s, 256)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [467, 0, 0, 100]
  s.ins MSGSET, uint(257), byte(2), line(s, 257)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 99, 100, 0, 0]
  s.ins MSGSET, uint(258), byte(127), line(s, 258)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins MSGSET, uint(259), byte(127), line(s, 259)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(260), byte(3), line(s, 260)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(261), byte(0), line(s, 261)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(262), byte(1), line(s, 262)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [469, 0, 0, 100]
  s.ins MSGSET, uint(263), byte(2), line(s, 263)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(264), byte(3), line(s, 264)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [465, 0, 0, 100]
  s.ins MSGSET, uint(265), byte(2), line(s, 265)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [467, 0, 0, 100]
  s.ins MSGSET, uint(266), byte(2), line(s, 266)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [469, 0, 0, 100]
  s.ins MSGSET, uint(267), byte(2), line(s, 267)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(268), byte(2), line(s, 268)
  s.ins MSGSYNC, byte(0), 2944
  s.ins CALL, :addr_0x5914, [468, 0, 0, 100]
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x10ca0
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x10ca0
  s.ins MOV, byte(0), ushort(72), 1
  s.ins JUMP, :addr_0x8394
  s.ins DEBUG, 's02_intA01_04 start.', byte(0)
  s.label :game_02_intA01_04_土麗美、初めての死刑囚
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x5892, [426, 0, 0, 100]
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(269), byte(3), line(s, 269)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(270), byte(2), line(s, 270)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(271), byte(0), line(s, 271)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(272), byte(1), line(s, 272)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [521, Register.new(233), 2000]
  s.ins MOV, byte(0), ushort(233), 521
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [428]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(273), byte(3), line(s, 273)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(233)]
  s.ins MOV, byte(0), ushort(233), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(274), byte(0), line(s, 274)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(275), byte(1), line(s, 275)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(276), byte(2), line(s, 276)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [521, Register.new(233), 2000]
  s.ins MOV, byte(0), ushort(233), 521
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [426]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(277), byte(3), line(s, 277)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(233)]
  s.ins MOV, byte(0), ushort(233), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(278), byte(0), line(s, 278)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(279), byte(1), line(s, 279)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(280), byte(2), line(s, 280)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [521, Register.new(233), 2000]
  s.ins MOV, byte(0), ushort(233), 521
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [428]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(281), byte(3), line(s, 281)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(282), byte(3), line(s, 282)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4ed5, [100, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(233)]
  s.ins MOV, byte(0), ushort(233), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(283), byte(1), line(s, 283)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(284), byte(1), line(s, 284)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [426, 0, 0, 100]
  s.ins MSGSET, uint(285), byte(3), line(s, 285)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(286), byte(0), line(s, 286)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(287), byte(2), line(s, 287)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(288), byte(3), line(s, 288)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [1000, 0]
  s.ins CALL, :addr_0x5914, [427, 0, 0, 100]
  s.ins MSGSET, uint(289), byte(3), line(s, 289)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [0, 100, 0, 1]
  s.ins CALL, :addr_0x5914, [426, 0, 0, 100]
  s.ins MSGSET, uint(290), byte(3), line(s, 290)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x604e, [1300]
  s.ins MSGSET, uint(291), byte(127), line(s, 291)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(292), byte(127), line(s, 292)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x60a7, []
  s.ins MSGSET, uint(293), byte(3), line(s, 293)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(294), byte(1), line(s, 294)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(295), byte(3), line(s, 295)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(296), byte(0), line(s, 296)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(297), byte(3), line(s, 297)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(298), byte(2), line(s, 298)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(299), byte(3), line(s, 299)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [428, 0, 0, 100]
  s.ins MSGSET, uint(300), byte(3), line(s, 300)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(301), byte(1), line(s, 301)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(302), byte(3), line(s, 302)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(303), byte(2), line(s, 303)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(304), byte(0), line(s, 304)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [426, 0, 0, 100]
  s.ins MSGSET, uint(305), byte(3), line(s, 305)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(306), byte(1), line(s, 306)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(307), byte(2), line(s, 307)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(308), byte(0), line(s, 308)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 45, 100, 0, 0]
  s.ins CALL, :addr_0x5914, [430, 0, 0, 100]
  s.ins MSGSET, uint(309), byte(3), line(s, 309)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x11b64
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x11b64
  s.ins MOV, byte(0), ushort(73), 1
  s.ins JUMP, :addr_0x8394
  s.ins DEBUG, 's02_intA01_05 start.', byte(0)
  s.label :game_02_intA01_05_風華、２回目の死刑囚
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x574d, [33, 0, 0, 0, 150, 0]
  s.ins CALL, :f_bgm_play, [8, 100, 0, 1]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 0, 115, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(310), byte(0), line(s, 310)
  s.ins MSGSYNC, byte(0), 3797
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 105, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(311), byte(127), line(s, 311)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(312), byte(127), line(s, 312)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(313), byte(127), line(s, 313)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(314), byte(0), line(s, 314)
  s.ins MSGSYNC, byte(0), 3413
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 117, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-999, 0, 0, 0]
  s.ins LAYERCTRL, -6, 22, byte(0)
  s.ins CALL, :addr_0x4df4, [0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [1600, 500, 133, -1]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [450, 500, 5, -1]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins LAYERCTRL, -6, 22, byte(1), 1
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-449, 500, 5, -1]
  s.ins MSGSET, uint(315), byte(1), line(s, 315)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 105, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(316), byte(0), line(s, 316)
  s.ins MSGSYNC, byte(0), 1856
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 6293
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 105, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(317), byte(0), line(s, 317)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x6bb1, [1, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 500, 5, -1]
  s.ins MSGSET, uint(318), byte(1), line(s, 318)
  s.ins MSGSYNC, byte(0), 1664
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 200]
  s.ins MSGSYNC, byte(0), 6037
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 30, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(319), byte(1), line(s, 319)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 123, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(320), byte(0), line(s, 320)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(321), byte(1), line(s, 321)
  s.ins MSGSYNC, byte(0), 7061
  s.ins CALL, :f_se_play, [0, 90, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 129, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 200]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(322), byte(127), line(s, 322)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(323), byte(0), line(s, 323)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 90, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 126, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 200]
  s.ins MSGSET, uint(324), byte(1), line(s, 324)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(325), byte(1), line(s, 325)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 47, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGSET, uint(326), byte(1), line(s, 326)
  s.ins MSGSYNC, byte(0), 3968
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 1600, 1, 800]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 200]
  s.ins MSGSET, uint(327), byte(127), line(s, 327)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 26, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(328), byte(1), line(s, 328)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(329), byte(0), line(s, 329)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-449, 750, 5, -1]
  s.ins MSGSET, uint(330), byte(1), line(s, 330)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(331), byte(0), line(s, 331)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(332), byte(1), line(s, 332)
  s.ins MSGSYNC, byte(0), 3754
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 19, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(333), byte(0), line(s, 333)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(334), byte(0), line(s, 334)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(335), byte(1), line(s, 335)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 105, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(336), byte(0), line(s, 336)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 124, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGSET, uint(337), byte(1), line(s, 337)
  s.ins MSGSYNC, byte(0), 2389
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(338), byte(0), line(s, 338)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(339), byte(0), line(s, 339)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(340), byte(1), line(s, 340)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 1600, 1, 800]
  s.ins MSGSET, uint(341), byte(0), line(s, 341)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(342), byte(1), line(s, 342)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 19, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(343), byte(1), line(s, 343)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 105, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(344), byte(0), line(s, 344)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(345), byte(0), line(s, 345)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(346), byte(0), line(s, 346)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(347), byte(1), line(s, 347)
  s.ins MSGSYNC, byte(0), 3562
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(348), byte(127), line(s, 348)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :addr_0x57d6, [31, 1, 0, 0, 150, 0]
  s.ins CALL, :f_bgm_play, [6, 100, 0, 1]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 23, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MSGSET, uint(349), byte(1), line(s, 349)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(350), byte(1), line(s, 350)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(351), byte(1), line(s, 351)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 25, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(352), byte(0), line(s, 352)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(353), byte(0), line(s, 353)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(354), byte(0), line(s, 354)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(355), byte(1), line(s, 355)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [12, 35, 70]
  s.ins MSGSET, uint(356), byte(0), line(s, 356)
  s.ins MSGSYNC, byte(0), 6464
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(357), byte(0), line(s, 357)
  s.ins MSGSYNC, byte(0), 1813
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [431, 0, 0, 100]
  s.ins MSGSET, uint(358), byte(1), line(s, 358)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(359), byte(0), line(s, 359)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(360), byte(1), line(s, 360)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [435, 0, 0, 100]
  s.ins MSGSET, uint(361), byte(1), line(s, 361)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(362), byte(1), line(s, 362)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(363), byte(1), line(s, 363)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(364), byte(1), line(s, 364)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(365), byte(127), line(s, 365)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(366), byte(127), line(s, 366)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(367), byte(127), line(s, 367)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(368), byte(127), line(s, 368)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(369), byte(127), line(s, 369)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(370), byte(127), line(s, 370)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(371), byte(127), line(s, 371)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(372), byte(127), line(s, 372)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(373), byte(0), line(s, 373)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(374), byte(1), line(s, 374)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(375), byte(0), line(s, 375)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(376), byte(1), line(s, 376)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(377), byte(0), line(s, 377)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(378), byte(1), line(s, 378)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(379), byte(0), line(s, 379)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(380), byte(0), line(s, 380)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(381), byte(1), line(s, 381)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [436, 0, 0, 100]
  s.ins MSGSET, uint(382), byte(1), line(s, 382)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(383), byte(0), line(s, 383)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(384), byte(1), line(s, 384)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x13c97
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x13c97
  s.ins MOV, byte(0), ushort(74), 1
  s.ins JUMP, :addr_0x8394
  s.ins DEBUG, 's02_intA01_06 start.', byte(0)
  s.label :game_02_intA01_06_火凛、２回目の死刑囚
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x574d, [30, 0, 0, 0, 100, 0]
  s.ins CALL, :f_bgm_play, [0, 100, 0, 1]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 0, 103, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 0, 207, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(385), byte(2), line(s, 385)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 90, 100, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 200]
  s.ins MSGSET, uint(386), byte(1), line(s, 386)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 226, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(387), byte(2), line(s, 387)
  s.ins MSGSYNC, byte(0), 4906
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(388), byte(1), line(s, 388)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(389), byte(1), line(s, 389)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 225, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(390), byte(2), line(s, 390)
  s.ins MSGSYNC, byte(0), 6442
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 23, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(391), byte(2), line(s, 391)
  s.ins MSGSYNC, byte(0), 4714
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 129, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(392), byte(1), line(s, 392)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 104, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(393), byte(1), line(s, 393)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(394), byte(2), line(s, 394)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d0e, [15, 1000]
  s.ins MSGSET, uint(395), byte(2), line(s, 395)
  s.ins MSGSYNC, byte(0), 2944
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 211, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 6528
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [12, 500, 3, 250]
  s.ins MSGSET, uint(396), byte(1), line(s, 396)
  s.ins MSGSYNC, byte(0), 5269
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGSET, uint(397), byte(2), line(s, 397)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x57d6, [31, :null, 0, 0, 100, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(398), byte(1), line(s, 398)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(399), byte(2), line(s, 399)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 104, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 2, 400]
  s.ins MSGSET, uint(400), byte(1), line(s, 400)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 216, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(401), byte(2), line(s, 401)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(402), byte(1), line(s, 402)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(403), byte(2), line(s, 403)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(404), byte(1), line(s, 404)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(405), byte(2), line(s, 405)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(406), byte(1), line(s, 406)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(407), byte(2), line(s, 407)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(408), byte(2), line(s, 408)
  s.ins MSGSYNC, byte(0), 4800
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 226, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 19, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(409), byte(1), line(s, 409)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 123, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(410), byte(127), line(s, 410)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(411), byte(127), line(s, 411)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(412), byte(1), line(s, 412)
  s.ins MSGSYNC, byte(0), 1493
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 3776
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 104, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(413), byte(2), line(s, 413)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(414), byte(2), line(s, 414)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(415), byte(127), line(s, 415)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 19, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(416), byte(127), line(s, 416)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(417), byte(1), line(s, 417)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 123, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(418), byte(1), line(s, 418)
  s.ins MSGSYNC, byte(0), 4330
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(419), byte(1), line(s, 419)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(420), byte(1), line(s, 420)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGSET, uint(421), byte(2), line(s, 421)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(422), byte(2), line(s, 422)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(423), byte(1), line(s, 423)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(424), byte(2), line(s, 424)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(425), byte(1), line(s, 425)
  s.ins MSGSYNC, byte(0), 3029
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(426), byte(2), line(s, 426)
  s.ins MSGSYNC, byte(0), 5418
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(427), byte(2), line(s, 427)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(428), byte(1), line(s, 428)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 19, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x604e, [1800]
  s.ins MSGSET, uint(429), byte(127), line(s, 429)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(430), byte(127), line(s, 430)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(431), byte(127), line(s, 431)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(432), byte(127), line(s, 432)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 23, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(433), byte(127), line(s, 433)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(434), byte(127), line(s, 434)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x60a7, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(435), byte(1), line(s, 435)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(436), byte(2), line(s, 436)
  s.ins MSGSYNC, byte(0), 810
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6ccf, [45, 500, 250]
  s.ins MSGSYNC, byte(0), 4160
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 221, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(437), byte(2), line(s, 437)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(438), byte(1), line(s, 438)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(439), byte(2), line(s, 439)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(440), byte(2), line(s, 440)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(441), byte(1), line(s, 441)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 210, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(442), byte(2), line(s, 442)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 226, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(443), byte(2), line(s, 443)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x604e, [1800]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6bb1, [2, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6bb1, [1, 1]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(444), byte(127), line(s, 444)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(445), byte(127), line(s, 445)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(446), byte(127), line(s, 446)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(447), byte(127), line(s, 447)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(448), byte(127), line(s, 448)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(449), byte(127), line(s, 449)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(450), byte(127), line(s, 450)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(451), byte(127), line(s, 451)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x60a7, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(452), byte(1), line(s, 452)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(453), byte(1), line(s, 453)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(454), byte(2), line(s, 454)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(455), byte(1), line(s, 455)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 221, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(456), byte(2), line(s, 456)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 23, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(457), byte(1), line(s, 457)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(458), byte(2), line(s, 458)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(459), byte(2), line(s, 459)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 500, 5, -1]
  s.ins CALL, :addr_0x4df4, [500]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 2, 400]
  s.ins MSGSET, uint(460), byte(127), line(s, 460)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 45, 100, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x60c6, []
  s.ins MSGSET, uint(461), byte(2), line(s, 461)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(462), byte(2), line(s, 462)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(463), byte(2), line(s, 463)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(464), byte(2), line(s, 464)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x163d2
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x163d2
  s.ins MOV, byte(0), ushort(75), 1
  s.ins JUMP, :addr_0x8394
  s.ins DEBUG, 's02_intA01_07 start.', byte(0)
  s.label :game_02_intA01_07_水無、２回目の死刑囚
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x574d, [30, 0, 0, 0, 120, 0]
  s.ins CALL, :f_bgm_play, [8, 100, 0, 1]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 0, 203, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(465), byte(2), line(s, 465)
  s.ins MSGSYNC, byte(0), 1397
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 0, 101, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-999, 0, 0, 0]
  s.ins LAYERCTRL, -6, 22, byte(0)
  s.ins CALL, :addr_0x4df4, [0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [1600, 500, 133, -1]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [450, 500, 5, -1]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins LAYERCTRL, -6, 22, byte(1), 1
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-449, 500, 5, -1]
  s.ins MSGSET, uint(466), byte(3), line(s, 466)
  s.ins MSGSYNC, byte(0), 4138
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(467), byte(2), line(s, 467)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 210, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(468), byte(2), line(s, 468)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(469), byte(3), line(s, 469)
  s.ins MSGSYNC, byte(0), 2602
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(470), byte(2), line(s, 470)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(471), byte(3), line(s, 471)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(472), byte(2), line(s, 472)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 95, 100, 0, 0]
  s.ins MSGSET, uint(473), byte(127), line(s, 473)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(474), byte(127), line(s, 474)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(475), byte(127), line(s, 475)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(476), byte(3), line(s, 476)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(477), byte(2), line(s, 477)
  s.ins MSGSYNC, byte(0), 4309
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 209, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(478), byte(3), line(s, 478)
  s.ins MSGSYNC, byte(0), 2773
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 204, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(479), byte(2), line(s, 479)
  s.ins MSGSYNC, byte(0), 917
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 2901
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6ccf, [45, 500, 250]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x574d, [36, :null, 0, 0, 100, 0]
  s.ins MSGSET, uint(480), byte(127), line(s, 480)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(481), byte(127), line(s, 481)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(482), byte(127), line(s, 482)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(483), byte(127), line(s, 483)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x574d, [30, :null, 1500, 0, 130, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 23, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(484), byte(2), line(s, 484)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 210, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(485), byte(2), line(s, 485)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(486), byte(3), line(s, 486)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(487), byte(2), line(s, 487)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 37, 37
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [90, Register.new(196)]
  s.ins MOV, byte(0), ushort(196), 90
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5184, [0, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins LAYERCTRL, -6, 23, byte(1), 1
  s.ins LAYERSELECT, 37, 37
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x5184, [255, 500, 0, 0]
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :addr_0x4df4, [300]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-799, 50000, 12288, 0]
  s.ins LAYERSELECT, 37, 37
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x5184, [0, 2000, 0, 0]
  s.ins MSGSET, uint(488), byte(127), line(s, 488)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(489), byte(127), line(s, 489)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 2, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(490), byte(3), line(s, 490)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(491), byte(2), line(s, 491)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [2500, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [1]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 101, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-349, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), 100
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [350, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins MSGSET, uint(492), byte(127), line(s, 492)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(493), byte(127), line(s, 493)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(494), byte(3), line(s, 494)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(495), byte(2), line(s, 495)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 226, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(496), byte(2), line(s, 496)
  s.ins MSGSYNC, byte(0), 4074
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(497), byte(3), line(s, 497)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(498), byte(3), line(s, 498)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(499), byte(2), line(s, 499)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(500), byte(3), line(s, 500)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 200, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(501), byte(2), line(s, 501)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(502), byte(3), line(s, 502)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(503), byte(3), line(s, 503)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(504), byte(2), line(s, 504)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(505), byte(3), line(s, 505)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 210, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGSET, uint(506), byte(2), line(s, 506)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-4499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-1499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [4500, 300000, 12288, 0]
  s.ins CALL, :f_se_play, [3, 95, 100, 0, 0]
  s.ins MSGSET, uint(507), byte(127), line(s, 507)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(508), byte(2), line(s, 508)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(509), byte(3), line(s, 509)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 2, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(510), byte(2), line(s, 510)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(511), byte(3), line(s, 511)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_se_play, [0, 45, 100, 0, 0]
  s.ins CALL, :addr_0x60c6, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [2500, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [1]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, :null, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-349, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [350, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(512), byte(2), line(s, 512)
  s.ins MSGSYNC, byte(0), 821
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGSYNC, byte(0), 2101
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(513), byte(3), line(s, 513)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6ccf, [15, 500, 250]
  s.ins MSGSET, uint(514), byte(2), line(s, 514)
  s.ins MSGSYNC, byte(0), 2730
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(515), byte(3), line(s, 515)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :addr_0x617d, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 204, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGSET, uint(516), byte(2), line(s, 516)
  s.ins MSGSYNC, byte(0), 6122
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 210, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(517), byte(3), line(s, 517)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(518), byte(2), line(s, 518)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(519), byte(3), line(s, 519)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(520), byte(2), line(s, 520)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [521, Register.new(233), 2000]
  s.ins MOV, byte(0), ushort(233), 521
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(521), byte(127), line(s, 521)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(522), byte(127), line(s, 522)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(523), byte(127), line(s, 523)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(233)]
  s.ins MOV, byte(0), ushort(233), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [2500, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [1]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-349, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [350, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(524), byte(3), line(s, 524)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [6, 100, 0, 1]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(525), byte(2), line(s, 525)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(526), byte(3), line(s, 526)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 225, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(527), byte(2), line(s, 527)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 123, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(528), byte(3), line(s, 528)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(529), byte(2), line(s, 529)
  s.ins MSGSYNC, byte(0), 4352
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(530), byte(127), line(s, 530)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(531), byte(3), line(s, 531)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(532), byte(2), line(s, 532)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(533), byte(3), line(s, 533)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 1600, 1, 800]
  s.ins MSGSET, uint(534), byte(3), line(s, 534)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(535), byte(2), line(s, 535)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(536), byte(2), line(s, 536)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(537), byte(3), line(s, 537)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [521, Register.new(233), 2000]
  s.ins MOV, byte(0), ushort(233), 521
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(538), byte(127), line(s, 538)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(539), byte(127), line(s, 539)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(540), byte(127), line(s, 540)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(541), byte(127), line(s, 541)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(542), byte(127), line(s, 542)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(543), byte(127), line(s, 543)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(544), byte(127), line(s, 544)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(545), byte(127), line(s, 545)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(546), byte(127), line(s, 546)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(547), byte(3), line(s, 547)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(548), byte(2), line(s, 548)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 74, 74
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(233)]
  s.ins MOV, byte(0), ushort(233), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [2500, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [1]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 101, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-349, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [350, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(549), byte(3), line(s, 549)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(550), byte(2), line(s, 550)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(551), byte(2), line(s, 551)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(552), byte(3), line(s, 552)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(553), byte(3), line(s, 553)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(554), byte(2), line(s, 554)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(555), byte(3), line(s, 555)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(556), byte(2), line(s, 556)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(557), byte(3), line(s, 557)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(558), byte(127), line(s, 558)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(559), byte(127), line(s, 559)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-999, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x6dde, []
  s.ins CALL, :addr_0x4df4, [0]
  s.ins MSGSET, uint(560), byte(127), line(s, 560)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(561), byte(2), line(s, 561)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 23, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(562), byte(3), line(s, 562)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [50, 1000, 133, -1]
  s.ins CALL, :f_set_sprite_zoom, [5, 1000, 133, -1]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [500, 1000, 133, -1]
  s.ins CALL, :f_set_sprite_zoom, [0, 1000, 133, -1]
  s.ins MSGSET, uint(563), byte(127), line(s, 563)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 5, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(564), byte(2), line(s, 564)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(565), byte(2), line(s, 565)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 6, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(566), byte(2), line(s, 566)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 122, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(567), byte(3), line(s, 567)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(568), byte(127), line(s, 568)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(569), byte(127), line(s, 569)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x19473
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x19473
  s.ins MOV, byte(0), ushort(76), 1
  s.ins JUMP, :addr_0x8394
  s.ins DEBUG, 's02_intA01_08 start.', byte(0)
  s.label :game_02_intA01_08_土麗美、２回目の死刑囚
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x574d, [30, 0, 0, 0, 100, 0]
  s.ins CALL, :f_bgm_play, [7, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(570), byte(127), line(s, 570)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(571), byte(127), line(s, 571)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(572), byte(127), line(s, 572)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [2500, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [1]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 0, 115, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-349, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 0, 100, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [350, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MSGSET, uint(573), byte(3), line(s, 573)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 123, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(574), byte(0), line(s, 574)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(575), byte(127), line(s, 575)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(576), byte(127), line(s, 576)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(577), byte(0), line(s, 577)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(578), byte(3), line(s, 578)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(579), byte(0), line(s, 579)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(580), byte(3), line(s, 580)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(581), byte(0), line(s, 581)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 119, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(582), byte(3), line(s, 582)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :f_se_play, [0, 48, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 68, 68
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [484, Register.new(227), 1900]
  s.ins MOV, byte(0), ushort(227), 484
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [546, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 546
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(583), byte(3), line(s, 583)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4ed5, [100, 1000]
  s.ins MSGSET, uint(584), byte(0), line(s, 584)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(585), byte(0), line(s, 585)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins LAYERSELECT, 68, 68
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(227)]
  s.ins MOV, byte(0), ushort(227), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(586), byte(3), line(s, 586)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(587), byte(3), line(s, 587)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 42, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [547, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 547
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(588), byte(3), line(s, 588)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(589), byte(0), line(s, 589)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(590), byte(3), line(s, 590)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(591), byte(0), line(s, 591)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(592), byte(3), line(s, 592)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(593), byte(0), line(s, 593)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(594), byte(3), line(s, 594)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [546, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 546
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins CALL, :f_se_play, [0, 47, 100, 0, 0]
  s.ins MSGSET, uint(595), byte(3), line(s, 595)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(596), byte(0), line(s, 596)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(597), byte(3), line(s, 597)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :f_se_play, [3, 70, 70, 2000, 0]
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [545, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 545
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(598), byte(3), line(s, 598)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(599), byte(3), line(s, 599)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(600), byte(3), line(s, 600)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(601), byte(0), line(s, 601)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 46, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1500
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 35, 35
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [592, Register.new(194)]
  s.ins MOV, byte(0), ushort(194), 592
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 36, 36
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [592, Register.new(195)]
  s.ins MOV, byte(0), ushort(195), 592
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 36, 36
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-109, 400, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 400, 0, 0]
  s.ins MSGSET, uint(602), byte(3), line(s, 602)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x6f17, [3, 100, 1000]
  s.ins CALL, :addr_0x58f7, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 35, 35
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(194)]
  s.ins MOV, byte(0), ushort(194), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 36, 36
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(195)]
  s.ins MOV, byte(0), ushort(195), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins CALL, :addr_0x5906, []
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [30, :null]
  s.ins CALL, :addr_0x5906, []
  s.ins CALL, :f_se_play, [0, 49, 100, 0, 0]
  s.ins CALL, :f_bgm_play, [0, 100, 0, 1]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 0, 3, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins MSGSET, uint(603), byte(1), line(s, 603)
  s.ins MSGSYNC, byte(0), 2517
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 0, 211, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [700, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins MSGSET, uint(604), byte(2), line(s, 604)
  s.ins MSGSYNC, byte(0), 3776
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 4778
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), -20
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 44, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-249, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [0, 0]
  s.ins CALL, :addr_0x51ea, [160, 0, 0, 0]
  s.ins MSGSET, uint(605), byte(0), line(s, 605)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(606), byte(1), line(s, 606)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 46, 100, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 3, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ea, [0, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 200]
  s.ins MSGSET, uint(607), byte(0), line(s, 607)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), -199
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 106, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-699, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [3, 1]
  s.ins MSGSET, uint(608), byte(3), line(s, 608)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(609), byte(2), line(s, 609)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(610), byte(1), line(s, 610)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 17, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 65, 65
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [495, Register.new(224), 1900]
  s.ins MOV, byte(0), ushort(224), 495
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [548, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 548
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(611), byte(0), line(s, 611)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(612), byte(3), line(s, 612)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins LAYERSELECT, 65, 65
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(224)]
  s.ins MOV, byte(0), ushort(224), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(613), byte(2), line(s, 613)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(614), byte(127), line(s, 614)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(615), byte(127), line(s, 615)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 15, 100, 0, 0]
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [549, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 549
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d0e, [50, 1000]
  s.ins MSGSET, uint(616), byte(127), line(s, 616)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(617), byte(1), line(s, 617)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSET, uint(618), byte(0), line(s, 618)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d0e, [50, 1000]
  s.ins MSGSET, uint(619), byte(2), line(s, 619)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(620), byte(3), line(s, 620)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(621), byte(2), line(s, 621)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [553, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 553
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSET, uint(622), byte(0), line(s, 622)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(623), byte(1), line(s, 623)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :addr_0x56ef, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 15, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [10, 800, 133, -1]
  s.ins MSGSET, uint(624), byte(1), line(s, 624)
  s.ins MSGSYNC, byte(0), 1717
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-10, 200, 133, -1]
  s.ins MSGSYNC, byte(0), 2314
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-15, 1000, 2, 500]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(625), byte(127), line(s, 625)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGSET, uint(626), byte(127), line(s, 626)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(627), byte(1), line(s, 627)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 20
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 0, 141, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-299, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [0, 0]
  s.ins CALL, :addr_0x51ea, [160, 0, 0, 0]
  s.ins MSGSET, uint(628), byte(0), line(s, 628)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 112, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(629), byte(3), line(s, 629)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(630), byte(0), line(s, 630)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(631), byte(2), line(s, 631)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(632), byte(1), line(s, 632)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 46, 100, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 3, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ea, [0, 0, 0, 0]
  s.ins MSGSET, uint(633), byte(0), line(s, 633)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(634), byte(3), line(s, 634)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(635), byte(1), line(s, 635)
  s.ins MSGSYNC, byte(0), 736
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 200]
  s.ins MSGSYNC, byte(0), 4053
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [200, 1000, 128, 0]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [553, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 553
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(636), byte(0), line(s, 636)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(637), byte(2), line(s, 637)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(638), byte(127), line(s, 638)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(639), byte(127), line(s, 639)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(640), byte(2), line(s, 640)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(641), byte(0), line(s, 641)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 15, 100, 0, 0]
  s.ins CALL, :f_se_play, [1, 17, 100, 0, 0]
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [125, 125, 500]
  s.ins MSGSET, uint(642), byte(127), line(s, 642)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(643), byte(127), line(s, 643)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 220, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(644), byte(1), line(s, 644)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 219, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(645), byte(2), line(s, 645)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(646), byte(3), line(s, 646)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(647), byte(3), line(s, 647)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(648), byte(0), line(s, 648)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(649), byte(3), line(s, 649)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x6bb1, [2, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 5, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(650), byte(1), line(s, 650)
  s.ins MSGSYNC, byte(0), 3818
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(651), byte(127), line(s, 651)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(652), byte(127), line(s, 652)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(653), byte(127), line(s, 653)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(654), byte(127), line(s, 654)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 9, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(655), byte(2), line(s, 655)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(656), byte(1), line(s, 656)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(657), byte(3), line(s, 657)
  s.ins MSGSYNC, byte(0), 6122
  s.ins CALL, :f_se_play, [0, 47, 100, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 124, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(658), byte(2), line(s, 658)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x1bd58
  s.ins CALL, :f_reset, []
  s.ins RETSUB
end
