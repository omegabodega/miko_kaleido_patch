def write_voices(snr)
  snr.voice 'karin*', 1, 1
  snr.voice 'mina*', 1, 2
  snr.voice 'doremi*', 1, 3
  snr.voice 'fuuka*', 1, 0
  snr.voice '*', 1, 4
  snr.write_voices
end
