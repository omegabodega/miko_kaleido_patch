require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_s03_gameA(s)
  s.label :addr_0x292c7
  s.ins MOV, byte(0), ushort(89), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's03_gameA_01 start.', byte(0)
  s.label :game_03_gameA_01（死：風、ピ：火）風華属性
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [134]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1256), byte(0), line(s, 1256)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1257), byte(1), line(s, 1257)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1258), byte(1), line(s, 1258)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [96, 0, 0, 100]
  s.ins MSGSET, uint(1259), byte(0), line(s, 1259)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1260), byte(3), line(s, 1260)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1261), byte(0), line(s, 1261)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1262), byte(2), line(s, 1262)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1263), byte(3), line(s, 1263)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1264), byte(3), line(s, 1264)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1265), byte(0), line(s, 1265)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1266), byte(3), line(s, 1266)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1267), byte(0), line(s, 1267)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1268), byte(2), line(s, 1268)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1269), byte(0), line(s, 1269)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1270), byte(0), line(s, 1270)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1271), byte(0), line(s, 1271)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1272), byte(2), line(s, 1272)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [102, 0, 0, 100]
  s.ins MSGSET, uint(1273), byte(0), line(s, 1273)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1274), byte(0), line(s, 1274)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1275), byte(2), line(s, 1275)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1276), byte(1), line(s, 1276)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1277), byte(3), line(s, 1277)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1278), byte(0), line(s, 1278)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1279), byte(0), line(s, 1279)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1280), byte(0), line(s, 1280)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1281), byte(0), line(s, 1281)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1282), byte(2), line(s, 1282)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1283), byte(0), line(s, 1283)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1284), byte(0), line(s, 1284)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1285), byte(0), line(s, 1285)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1286), byte(2), line(s, 1286)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1287), byte(0), line(s, 1287)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1288), byte(0), line(s, 1288)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1289), byte(0), line(s, 1289)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1290), byte(1), line(s, 1290)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1291), byte(0), line(s, 1291)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1292), byte(1), line(s, 1292)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1293), byte(0), line(s, 1293)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1294), byte(0), line(s, 1294)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1295), byte(2), line(s, 1295)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1296), byte(0), line(s, 1296)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1297), byte(0), line(s, 1297)
  s.ins MSGSYNC, byte(0), 3306
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 4309
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 4661
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 5024
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 5482
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), -199
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :addr_0x5038, [374, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 374
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1298), byte(127), line(s, 1298)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1299), byte(127), line(s, 1299)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1300), byte(0), line(s, 1300)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1301), byte(0), line(s, 1301)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1302), byte(0), line(s, 1302)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1303), byte(2), line(s, 1303)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 73, 60, 0, 0]
  s.ins MSGSET, uint(1304), byte(0), line(s, 1304)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1305), byte(0), line(s, 1305)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1306), byte(0), line(s, 1306)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1307), byte(2), line(s, 1307)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1308), byte(0), line(s, 1308)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1309), byte(1), line(s, 1309)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1310), byte(0), line(s, 1310)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1311), byte(0), line(s, 1311)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1312), byte(1), line(s, 1312)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 22, 22
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [383, Register.new(181)]
  s.ins MOV, byte(0), ushort(181), 383
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5184, [0, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [382, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 382
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 22, 22
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [650, 400, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-199, 400, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [170, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [255, 400, 0, 0]
  s.ins MSGSET, uint(1313), byte(0), line(s, 1313)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1314), byte(0), line(s, 1314)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 22, 22
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(181)]
  s.ins MOV, byte(0), ushort(181), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [404, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 404
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [650, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [170, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1315), byte(0), line(s, 1315)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1316), byte(0), line(s, 1316)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1317), byte(0), line(s, 1317)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1318), byte(1), line(s, 1318)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [102]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [35, 35, 2000]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2000]
  s.ins CALL, :addr_0x542b, [30, 2000]
  s.ins MSGSET, uint(1319), byte(0), line(s, 1319)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1320), byte(0), line(s, 1320)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1321), byte(0), line(s, 1321)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1322), byte(0), line(s, 1322)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1323), byte(0), line(s, 1323)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1324), byte(2), line(s, 1324)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [398, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 398
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1325), byte(0), line(s, 1325)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1326), byte(2), line(s, 1326)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1327), byte(0), line(s, 1327)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1328), byte(2), line(s, 1328)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [412, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 412
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(1329), byte(0), line(s, 1329)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [Register.new(180), Register.new(180)]
  s.ins MOV, byte(0), ushort(180), Register.new(180)
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [160, 0, 128, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-79, 0, 128, 0]
  s.ins CALL, :f_set_sprite_zoom, [20, 0, 128, 0]
  s.ins MSGSET, uint(1330), byte(0), line(s, 1330)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [Register.new(180), Register.new(180)]
  s.ins MOV, byte(0), ushort(180), Register.new(180)
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [160, 0, 128, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-79, 0, 128, 0]
  s.ins CALL, :f_set_sprite_zoom, [20, 0, 128, 0]
  s.ins MSGSET, uint(1331), byte(0), line(s, 1331)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1332), byte(2), line(s, 1332)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(180)]
  s.ins MOV, byte(0), ushort(180), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [102]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [35, 35, 2000]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2000]
  s.ins CALL, :addr_0x542b, [30, 2000]
  s.ins MSGSET, uint(1333), byte(0), line(s, 1333)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1334), byte(0), line(s, 1334)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 38, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1335), byte(3), line(s, 1335)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1336), byte(3), line(s, 1336)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1337), byte(3), line(s, 1337)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 31, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1338), byte(0), line(s, 1338)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1339), byte(3), line(s, 1339)
  s.ins MSGSYNC, byte(0), 2880
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 133, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x2b3ce
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x2b3ce
  s.ins MOV, byte(0), ushort(90), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_02 start.', byte(0)
  s.label :game_03_gameA_02（死：風、ピ：水）風華属性
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [134]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1340), byte(0), line(s, 1340)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1341), byte(1), line(s, 1341)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1342), byte(0), line(s, 1342)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1343), byte(0), line(s, 1343)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1344), byte(3), line(s, 1344)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1345), byte(2), line(s, 1345)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [123, 0, 0, 100]
  s.ins MSGSET, uint(1346), byte(0), line(s, 1346)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1347), byte(0), line(s, 1347)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1348), byte(1), line(s, 1348)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1349), byte(3), line(s, 1349)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1350), byte(2), line(s, 1350)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1351), byte(0), line(s, 1351)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1352), byte(0), line(s, 1352)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1353), byte(0), line(s, 1353)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1354), byte(1), line(s, 1354)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1355), byte(0), line(s, 1355)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [451, 0, 0, 100]
  s.ins MSGSET, uint(1356), byte(0), line(s, 1356)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1357), byte(3), line(s, 1357)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1358), byte(2), line(s, 1358)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1359), byte(4), line(s, 1359)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [123, 0, 0, 100]
  s.ins MSGSET, uint(1360), byte(0), line(s, 1360)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1361), byte(0), line(s, 1361)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1362), byte(0), line(s, 1362)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1363), byte(0), line(s, 1363)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1364), byte(1), line(s, 1364)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1365), byte(0), line(s, 1365)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1366), byte(3), line(s, 1366)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1367), byte(0), line(s, 1367)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1368), byte(0), line(s, 1368)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1369), byte(2), line(s, 1369)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1370), byte(0), line(s, 1370)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1371), byte(1), line(s, 1371)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1372), byte(2), line(s, 1372)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1373), byte(0), line(s, 1373)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1374), byte(0), line(s, 1374)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1375), byte(0), line(s, 1375)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1376), byte(0), line(s, 1376)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [451, 0, 0, 100]
  s.ins MSGSET, uint(1377), byte(1), line(s, 1377)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [Register.new(26), 0, 0, 130]
  s.ins MSGSET, uint(1378), byte(127), line(s, 1378)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1379), byte(127), line(s, 1379)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1380), byte(4), line(s, 1380)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1381), byte(1), line(s, 1381)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1382), byte(1), line(s, 1382)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1383), byte(1), line(s, 1383)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1384), byte(4), line(s, 1384)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [19, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 31, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1385), byte(2), line(s, 1385)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [102]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1386), byte(0), line(s, 1386)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1387), byte(2), line(s, 1387)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [96]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1388), byte(0), line(s, 1388)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1389), byte(1), line(s, 1389)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1390), byte(0), line(s, 1390)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1391), byte(0), line(s, 1391)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 32, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1392), byte(1), line(s, 1392)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1393), byte(1), line(s, 1393)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1394), byte(4), line(s, 1394)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1395), byte(127), line(s, 1395)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1396), byte(127), line(s, 1396)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 71, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 106, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1397), byte(3), line(s, 1397)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1398), byte(0), line(s, 1398)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1399), byte(3), line(s, 1399)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1400), byte(3), line(s, 1400)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 1, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 30, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1401), byte(2), line(s, 1401)
  s.ins MSGSYNC, byte(0), 1066
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1402), byte(2), line(s, 1402)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 19, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1403), byte(1), line(s, 1403)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1404), byte(1), line(s, 1404)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1405), byte(0), line(s, 1405)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1406), byte(0), line(s, 1406)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 9, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1407), byte(3), line(s, 1407)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1408), byte(3), line(s, 1408)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 126, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1409), byte(0), line(s, 1409)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1410), byte(3), line(s, 1410)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1411), byte(3), line(s, 1411)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1412), byte(3), line(s, 1412)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [130]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 1500]
  s.ins CALL, :addr_0x542b, [30, 1500]
  s.ins MSGSET, uint(1413), byte(0), line(s, 1413)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSET, uint(1414), byte(0), line(s, 1414)
  s.ins MSGSYNC, byte(0), 821
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 1544
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 2242
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1415), byte(0), line(s, 1415)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1416), byte(0), line(s, 1416)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 31, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins MSGSET, uint(1417), byte(3), line(s, 1417)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1418), byte(127), line(s, 1418)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1419), byte(3), line(s, 1419)
  s.ins MSGSYNC, byte(0), 2240
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x2d279
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x2d279
  s.ins MOV, byte(0), ushort(91), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_03 start.', byte(0)
  s.label :game_03_gameA_03（死：風、ピ：土）土麗美属性
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [2, 100, 0, 1]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [134]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1420), byte(0), line(s, 1420)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1421), byte(1), line(s, 1421)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1422), byte(1), line(s, 1422)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1423), byte(0), line(s, 1423)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1424), byte(3), line(s, 1424)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1425), byte(3), line(s, 1425)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1426), byte(2), line(s, 1426)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1427), byte(1), line(s, 1427)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [123, 0, 0, 100]
  s.ins MSGSET, uint(1428), byte(0), line(s, 1428)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 28, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 25, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1429), byte(1), line(s, 1429)
  s.ins MSGSYNC, byte(0), 288
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGSYNC, byte(0), 1456
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 30, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1430), byte(2), line(s, 1430)
  s.ins MSGSYNC, byte(0), 3626
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, 1, 3, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1431), byte(3), line(s, 1431)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1432), byte(3), line(s, 1432)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1433), byte(1), line(s, 1433)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1434), byte(3), line(s, 1434)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1435), byte(2), line(s, 1435)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1436), byte(2), line(s, 1436)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1437), byte(1), line(s, 1437)
  s.ins MSGSYNC, byte(0), 1066
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 1946
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 200]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [1]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1438), byte(3), line(s, 1438)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1439), byte(3), line(s, 1439)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1440), byte(1), line(s, 1440)
  s.ins MSGSYNC, byte(0), 4181
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 212, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1441), byte(2), line(s, 1441)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1442), byte(2), line(s, 1442)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1443), byte(1), line(s, 1443)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1444), byte(1), line(s, 1444)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 59, 100, 1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [123]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1445), byte(0), line(s, 1445)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins MSGSET, uint(1446), byte(0), line(s, 1446)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1447), byte(2), line(s, 1447)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins MSGSET, uint(1448), byte(0), line(s, 1448)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1449), byte(0), line(s, 1449)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1450), byte(1), line(s, 1450)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1451), byte(3), line(s, 1451)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins MSGSET, uint(1452), byte(0), line(s, 1452)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1453), byte(0), line(s, 1453)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1454), byte(0), line(s, 1454)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1455), byte(0), line(s, 1455)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1456), byte(1), line(s, 1456)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1457), byte(2), line(s, 1457)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1458), byte(0), line(s, 1458)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1459), byte(3), line(s, 1459)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Register.new(26)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-50, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-99, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins MSGSET, uint(1460), byte(0), line(s, 1460)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1461), byte(1), line(s, 1461)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1462), byte(2), line(s, 1462)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1463), byte(0), line(s, 1463)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1464), byte(0), line(s, 1464)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1465), byte(0), line(s, 1465)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1466), byte(1), line(s, 1466)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [96, -50, -99, 130]
  s.ins MSGSET, uint(1467), byte(0), line(s, 1467)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1468), byte(0), line(s, 1468)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1469), byte(3), line(s, 1469)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1470), byte(3), line(s, 1470)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Register.new(26)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MSGSET, uint(1471), byte(0), line(s, 1471)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1472), byte(0), line(s, 1472)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1473), byte(0), line(s, 1473)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1474), byte(3), line(s, 1474)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1475), byte(3), line(s, 1475)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1476), byte(0), line(s, 1476)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1477), byte(2), line(s, 1477)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1478), byte(1), line(s, 1478)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1479), byte(0), line(s, 1479)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 130, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins MSGSET, uint(1480), byte(3), line(s, 1480)
  s.ins MSGSYNC, byte(0), 1562
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1481), byte(3), line(s, 1481)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1482), byte(1), line(s, 1482)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1483), byte(3), line(s, 1483)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1484), byte(3), line(s, 1484)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1485), byte(3), line(s, 1485)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1486), byte(3), line(s, 1486)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 7, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1487), byte(0), line(s, 1487)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1488), byte(0), line(s, 1488)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGSET, uint(1489), byte(3), line(s, 1489)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 131, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1490), byte(3), line(s, 1490)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 141, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1491), byte(3), line(s, 1491)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 15, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1492), byte(1), line(s, 1492)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1493), byte(2), line(s, 1493)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1494), byte(0), line(s, 1494)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1495), byte(0), line(s, 1495)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1496), byte(3), line(s, 1496)
  s.ins MSGSYNC, byte(0), 208
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGSYNC, byte(0), 2544
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1497), byte(0), line(s, 1497)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1498), byte(3), line(s, 1498)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1499), byte(1), line(s, 1499)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [571, Register.new(211)]
  s.ins MOV, byte(0), ushort(211), 571
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(1500), byte(2), line(s, 1500)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 23, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x5c62, [1]
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1501), byte(127), line(s, 1501)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 21, 100, 0, 0]
  s.ins CALL, :addr_0x5cac, [0, 265, 0, 0, 100]
  s.ins MSGSET, uint(1502), byte(127), line(s, 1502)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1503), byte(1), line(s, 1503)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1504), byte(3), line(s, 1504)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1505), byte(0), line(s, 1505)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 70, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 144, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1506), byte(2), line(s, 1506)
  s.ins MSGSYNC, byte(0), 7509
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 133, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 35, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1507), byte(2), line(s, 1507)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 43, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1508), byte(2), line(s, 1508)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 7, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1509), byte(0), line(s, 1509)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 137, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1510), byte(3), line(s, 1510)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1511), byte(2), line(s, 1511)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 136, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1512), byte(2), line(s, 1512)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x2fb30
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x2fb30
  s.ins MOV, byte(0), ushort(92), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_04 start.', byte(0)
  s.label :game_03_gameA_04（死：火、ピ：風）風華属性
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [176]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :f_se_play, [4, 69, 50, 1000, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1513), byte(1), line(s, 1513)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 70, 100, 0, 0]
  s.ins MSGSET, uint(1514), byte(2), line(s, 1514)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1515), byte(1), line(s, 1515)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1516), byte(127), line(s, 1516)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1517), byte(1), line(s, 1517)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1518), byte(1), line(s, 1518)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1519), byte(2), line(s, 1519)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1520), byte(2), line(s, 1520)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1521), byte(2), line(s, 1521)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [179, 0, 0, 100]
  s.ins MSGSET, uint(1522), byte(1), line(s, 1522)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1523), byte(1), line(s, 1523)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1524), byte(2), line(s, 1524)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1525), byte(3), line(s, 1525)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1526), byte(0), line(s, 1526)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1527), byte(2), line(s, 1527)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1528), byte(2), line(s, 1528)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1529), byte(3), line(s, 1529)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1530), byte(1), line(s, 1530)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1531), byte(1), line(s, 1531)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1532), byte(1), line(s, 1532)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1533), byte(1), line(s, 1533)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [4, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 117, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1534), byte(3), line(s, 1534)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 102, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1535), byte(0), line(s, 1535)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1536), byte(3), line(s, 1536)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1537), byte(3), line(s, 1537)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1538), byte(0), line(s, 1538)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1539), byte(3), line(s, 1539)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1540), byte(3), line(s, 1540)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, 1, 1, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1541), byte(1), line(s, 1541)
  s.ins MSGSYNC, byte(0), 1952
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1542), byte(3), line(s, 1542)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 112, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1543), byte(2), line(s, 1543)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [176]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1544), byte(1), line(s, 1544)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1545), byte(1), line(s, 1545)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1546), byte(1), line(s, 1546)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1547), byte(3), line(s, 1547)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1548), byte(1), line(s, 1548)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 3, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1549), byte(0), line(s, 1549)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1550), byte(3), line(s, 1550)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1551), byte(3), line(s, 1551)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1552), byte(3), line(s, 1552)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [176]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1553), byte(1), line(s, 1553)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1554), byte(1), line(s, 1554)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1555), byte(1), line(s, 1555)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 13, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1556), byte(2), line(s, 1556)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1557), byte(2), line(s, 1557)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1558), byte(0), line(s, 1558)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1559), byte(0), line(s, 1559)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1560), byte(0), line(s, 1560)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1561), byte(0), line(s, 1561)
  s.ins MSGSYNC, byte(0), 2698
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1562), byte(0), line(s, 1562)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1563), byte(3), line(s, 1563)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1564), byte(2), line(s, 1564)
  s.ins MSGSYNC, byte(0), 3626
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 2, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1565), byte(0), line(s, 1565)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_se_play, [0, 23, 100, 0, 0]
  s.ins CALL, :addr_0x5c62, [1]
  s.ins CALL, :f_se_fadeout, [0, 0]
  s.ins MSGSET, uint(1566), byte(127), line(s, 1566)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins CALL, :f_se_play, [0, 21, 100, 0, 0]
  s.ins CALL, :addr_0x5cac, [1, 134, 0, 0, 100]
  s.ins MSGSET, uint(1567), byte(127), line(s, 1567)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1568), byte(0), line(s, 1568)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [130, 0, 0, 100]
  s.ins MSGSET, uint(1569), byte(0), line(s, 1569)
  s.ins MSGSYNC, byte(0), 4053
  s.ins CALL, :addr_0x5914, [134, 0, 0, 100]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1570), byte(2), line(s, 1570)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [130, 0, 0, 100]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(1571), byte(0), line(s, 1571)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1572), byte(0), line(s, 1572)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1573), byte(0), line(s, 1573)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSET, uint(1574), byte(0), line(s, 1574)
  s.ins MSGSYNC, byte(0), 1344
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 2608
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 3525
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 4597
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 5621
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 6432
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1575), byte(3), line(s, 1575)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1576), byte(0), line(s, 1576)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 33, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins MSGSET, uint(1577), byte(3), line(s, 1577)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 35, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1578), byte(0), line(s, 1578)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, :null, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1579), byte(0), line(s, 1579)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1580), byte(127), line(s, 1580)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1581), byte(127), line(s, 1581)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1582), byte(127), line(s, 1582)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x3186a
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x3186a
  s.ins MOV, byte(0), ushort(93), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_05 start.', byte(0)
  s.label :game_03_gameA_05（死：火、ピ：水）火凛属性
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [146]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1583), byte(1), line(s, 1583)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1584), byte(1), line(s, 1584)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1585), byte(3), line(s, 1585)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1586), byte(0), line(s, 1586)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1587), byte(2), line(s, 1587)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1588), byte(3), line(s, 1588)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [142, 0, 0, 100]
  s.ins MSGSET, uint(1589), byte(1), line(s, 1589)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1590), byte(0), line(s, 1590)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1591), byte(3), line(s, 1591)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1592), byte(0), line(s, 1592)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1593), byte(2), line(s, 1593)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1594), byte(0), line(s, 1594)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [170, 0, 0, 100]
  s.ins MSGSET, uint(1595), byte(1), line(s, 1595)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1596), byte(127), line(s, 1596)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1597), byte(1), line(s, 1597)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [3000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 111, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1598), byte(3), line(s, 1598)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 113, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MSGSET, uint(1599), byte(0), line(s, 1599)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, 1, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1600), byte(1), line(s, 1600)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 220, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins MSGSET, uint(1601), byte(2), line(s, 1601)
  s.ins MSGSYNC, byte(0), 3776
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 208, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [2, 2, 1500]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1602), byte(3), line(s, 1602)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1603), byte(0), line(s, 1603)
  s.ins MSGSYNC, byte(0), 1674
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 1600, 1, 800]
  s.ins MSGSYNC, byte(0), 2570
  s.ins CALL, :f_se_play, [0, 75, 100, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [2, 2, 1500]
  s.ins MSGSYNC, byte(0), 4128
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 75, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 7, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-399, 250]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [2, 2, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1604), byte(2), line(s, 1604)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1605), byte(1), line(s, 1605)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 75, 100, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 9, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [2, 2, 0]
  s.ins MSGSET, uint(1606), byte(2), line(s, 1606)
  s.ins MSGSYNC, byte(0), 2101
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x55d3, [400, -249]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 106, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-249, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1607), byte(3), line(s, 1607)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [170]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1608), byte(1), line(s, 1608)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1609), byte(0), line(s, 1609)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1610), byte(1), line(s, 1610)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1611), byte(2), line(s, 1611)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1612), byte(1), line(s, 1612)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1613), byte(2), line(s, 1613)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [144, 0, 0, 100]
  s.ins MSGSET, uint(1614), byte(1), line(s, 1614)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1615), byte(1), line(s, 1615)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1616), byte(2), line(s, 1616)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1617), byte(1), line(s, 1617)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1618), byte(0), line(s, 1618)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1619), byte(1), line(s, 1619)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1620), byte(1), line(s, 1620)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1621), byte(3), line(s, 1621)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [142, 0, 0, 100]
  s.ins MSGSET, uint(1622), byte(1), line(s, 1622)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1623), byte(1), line(s, 1623)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1624), byte(1), line(s, 1624)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1625), byte(2), line(s, 1625)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1626), byte(1), line(s, 1626)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1627), byte(1), line(s, 1627)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1628), byte(0), line(s, 1628)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1629), byte(1), line(s, 1629)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [170, 0, 0, 100]
  s.ins MSGSET, uint(1630), byte(1), line(s, 1630)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1631), byte(3), line(s, 1631)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1632), byte(0), line(s, 1632)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1633), byte(2), line(s, 1633)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1634), byte(2), line(s, 1634)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1635), byte(1), line(s, 1635)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1636), byte(1), line(s, 1636)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1637), byte(2), line(s, 1637)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1638), byte(1), line(s, 1638)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 3, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1639), byte(3), line(s, 1639)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1640), byte(3), line(s, 1640)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1641), byte(0), line(s, 1641)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 1, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1642), byte(1), line(s, 1642)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1643), byte(0), line(s, 1643)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1644), byte(0), line(s, 1644)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1645), byte(1), line(s, 1645)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(1646), byte(0), line(s, 1646)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x33130
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x33130
  s.ins MOV, byte(0), ushort(94), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_06 start.', byte(0)
  s.label :game_03_gameA_06（死：火、ピ：土）火凛属性
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [176]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1647), byte(1), line(s, 1647)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1648), byte(2), line(s, 1648)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1649), byte(1), line(s, 1649)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1650), byte(3), line(s, 1650)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1651), byte(0), line(s, 1651)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1652), byte(2), line(s, 1652)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1653), byte(3), line(s, 1653)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1654), byte(3), line(s, 1654)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1655), byte(1), line(s, 1655)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1656), byte(2), line(s, 1656)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1657), byte(0), line(s, 1657)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1658), byte(3), line(s, 1658)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [142, 0, 0, 100]
  s.ins MSGSET, uint(1659), byte(1), line(s, 1659)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1660), byte(1), line(s, 1660)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [148, 0, 0, 100]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(1661), byte(1), line(s, 1661)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1662), byte(0), line(s, 1662)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1663), byte(1), line(s, 1663)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1664), byte(0), line(s, 1664)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1665), byte(1), line(s, 1665)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1666), byte(0), line(s, 1666)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1667), byte(1), line(s, 1667)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1668), byte(0), line(s, 1668)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1669), byte(1), line(s, 1669)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), 100
  s.ins CALL, :addr_0x5038, [375, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 375
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(1670), byte(0), line(s, 1670)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1671), byte(0), line(s, 1671)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1672), byte(2), line(s, 1672)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1673), byte(0), line(s, 1673)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [379, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 379
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-194, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(1674), byte(0), line(s, 1674)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 22, 22
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [380, Register.new(181)]
  s.ins MOV, byte(0), ushort(181), 380
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-194, 0, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins LAYERSELECT, 22, 22
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-249, 400, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-199, 400, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [255, 400, 0, 0]
  s.ins LAYERSELECT, 22, 22
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSET, uint(1675), byte(0), line(s, 1675)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1676), byte(2), line(s, 1676)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1677), byte(3), line(s, 1677)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1678), byte(1), line(s, 1678)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x33b19
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x33b19
  s.ins MOV, byte(0), ushort(95), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_07 start.', byte(0)
  s.label :game_03_gameA_07（死：水、ピ：風）水無属性
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [4, 69, 50, 1000, 0]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [232]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1679), byte(3), line(s, 1679)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [201, 0, 0, 100]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSET, uint(1680), byte(2), line(s, 1680)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1681), byte(0), line(s, 1681)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1682), byte(1), line(s, 1682)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1683), byte(3), line(s, 1683)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1684), byte(1), line(s, 1684)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1685), byte(0), line(s, 1685)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1686), byte(0), line(s, 1686)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [220, 0, 0, 100]
  s.ins MSGSET, uint(1687), byte(2), line(s, 1687)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1688), byte(2), line(s, 1688)
  s.ins MSGSYNC, byte(0), 4938
  s.ins CALL, :addr_0x5914, [201, 0, 0, 100]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 16, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1689), byte(1), line(s, 1689)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1690), byte(1), line(s, 1690)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, 1, 20, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1691), byte(3), line(s, 1691)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [4, 1000]
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1692), byte(1), line(s, 1692)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 112, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1693), byte(2), line(s, 1693)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 12, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1694), byte(0), line(s, 1694)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1695), byte(0), line(s, 1695)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1696), byte(0), line(s, 1696)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1697), byte(3), line(s, 1697)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1698), byte(3), line(s, 1698)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1699), byte(3), line(s, 1699)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 31, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1700), byte(2), line(s, 1700)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1701), byte(1), line(s, 1701)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1702), byte(1), line(s, 1702)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 32, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1703), byte(2), line(s, 1703)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1704), byte(0), line(s, 1704)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1705), byte(1), line(s, 1705)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1706), byte(1), line(s, 1706)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1707), byte(1), line(s, 1707)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 112, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1708), byte(2), line(s, 1708)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1709), byte(2), line(s, 1709)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1710), byte(1), line(s, 1710)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1711), byte(1), line(s, 1711)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1712), byte(1), line(s, 1712)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 140, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1713), byte(2), line(s, 1713)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1714), byte(1), line(s, 1714)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1715), byte(1), line(s, 1715)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1716), byte(1), line(s, 1716)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1717), byte(3), line(s, 1717)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1718), byte(1), line(s, 1718)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1719), byte(1), line(s, 1719)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1720), byte(1), line(s, 1720)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1721), byte(1), line(s, 1721)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1722), byte(0), line(s, 1722)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1723), byte(3), line(s, 1723)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [451]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1724), byte(1), line(s, 1724)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1725), byte(0), line(s, 1725)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1726), byte(4), line(s, 1726)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [2, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [201]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1727), byte(2), line(s, 1727)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1728), byte(2), line(s, 1728)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1729), byte(0), line(s, 1729)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1730), byte(0), line(s, 1730)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1731), byte(3), line(s, 1731)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1732), byte(2), line(s, 1732)
  s.ins MSGSYNC, byte(0), 6306
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 2600]
  s.ins CALL, :addr_0x542b, [30, 2600]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1733), byte(1), line(s, 1733)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1734), byte(0), line(s, 1734)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1735), byte(3), line(s, 1735)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1736), byte(1), line(s, 1736)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1737), byte(2), line(s, 1737)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1738), byte(0), line(s, 1738)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1739), byte(2), line(s, 1739)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 7, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1740), byte(3), line(s, 1740)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 23, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1741), byte(1), line(s, 1741)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1742), byte(1), line(s, 1742)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1743), byte(0), line(s, 1743)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGSET, uint(1744), byte(3), line(s, 1744)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1745), byte(3), line(s, 1745)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGSET, uint(1746), byte(1), line(s, 1746)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGSET, uint(1747), byte(0), line(s, 1747)
  s.ins MSGSYNC, byte(0), 1312
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1748), byte(4), line(s, 1748)
  s.ins MSGSYNC, byte(0), 1397
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSYNC, byte(0), 2709
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSYNC, byte(0), 4000
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [201]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1749), byte(2), line(s, 1749)
  s.ins MSGSYNC, byte(0), 544
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [35, 35, 3500]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 3500]
  s.ins CALL, :addr_0x542b, [30, 3500]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1750), byte(2), line(s, 1750)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1751), byte(2), line(s, 1751)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(1752), byte(1), line(s, 1752)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1753), byte(2), line(s, 1753)
  s.ins MSGSYNC, byte(0), 5486
  s.ins CALL, :addr_0x542b, [30, 1800]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1754), byte(1), line(s, 1754)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1755), byte(2), line(s, 1755)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x542b, [30, 2200]
  s.ins MSGSET, uint(1756), byte(1), line(s, 1756)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x637e, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-299, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x542b, [30, 2200]
  s.ins MSGSET, uint(1757), byte(2), line(s, 1757)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :f_se_play, [3, 59, 100, 1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x639f, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1758), byte(1), line(s, 1758)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1759), byte(1), line(s, 1759)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1760), byte(2), line(s, 1760)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1761), byte(2), line(s, 1761)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1762), byte(2), line(s, 1762)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1763), byte(1), line(s, 1763)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x362e0
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x362e0
  s.ins MOV, byte(0), ushort(96), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_08 start.', byte(0)
  s.label :game_03_gameA_08（死：水、ピ：火）火凛属性
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [232]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1764), byte(2), line(s, 1764)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1765), byte(0), line(s, 1765)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1766), byte(3), line(s, 1766)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1767), byte(0), line(s, 1767)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1768), byte(1), line(s, 1768)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1769), byte(0), line(s, 1769)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1770), byte(3), line(s, 1770)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1771), byte(2), line(s, 1771)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 15, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 3, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1772), byte(0), line(s, 1772)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1773), byte(3), line(s, 1773)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1774), byte(3), line(s, 1774)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1775), byte(0), line(s, 1775)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1776), byte(3), line(s, 1776)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1777), byte(0), line(s, 1777)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [451, 0, 0, 100]
  s.ins MSGSET, uint(1778), byte(1), line(s, 1778)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1779), byte(4), line(s, 1779)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1780), byte(1), line(s, 1780)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1781), byte(4), line(s, 1781)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1782), byte(1), line(s, 1782)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 115, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1783), byte(1), line(s, 1783)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1784), byte(0), line(s, 1784)
  s.ins MSGSYNC, byte(0), 565
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1785), byte(3), line(s, 1785)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 122, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1786), byte(1), line(s, 1786)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 31, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1787), byte(2), line(s, 1787)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [740, -574]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1788), byte(1), line(s, 1788)
  s.ins MSGSYNC, byte(0), 5674
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1789), byte(1), line(s, 1789)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1790), byte(1), line(s, 1790)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1791), byte(2), line(s, 1791)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 21, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1792), byte(3), line(s, 1792)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1793), byte(0), line(s, 1793)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1794), byte(1), line(s, 1794)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [205]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1795), byte(2), line(s, 1795)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [10, 100, 0, 1]
  s.ins MSGSET, uint(1796), byte(2), line(s, 1796)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1797), byte(3), line(s, 1797)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1798), byte(2), line(s, 1798)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1799), byte(2), line(s, 1799)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1800), byte(2), line(s, 1800)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1801), byte(1), line(s, 1801)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1802), byte(2), line(s, 1802)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1803), byte(2), line(s, 1803)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1804), byte(2), line(s, 1804)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1805), byte(2), line(s, 1805)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1806), byte(2), line(s, 1806)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1807), byte(2), line(s, 1807)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1808), byte(3), line(s, 1808)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1809), byte(2), line(s, 1809)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1810), byte(3), line(s, 1810)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1811), byte(2), line(s, 1811)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1812), byte(2), line(s, 1812)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1813), byte(0), line(s, 1813)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1814), byte(1), line(s, 1814)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1815), byte(1), line(s, 1815)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1816), byte(1), line(s, 1816)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1817), byte(1), line(s, 1817)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1818), byte(1), line(s, 1818)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1819), byte(1), line(s, 1819)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1820), byte(3), line(s, 1820)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1821), byte(2), line(s, 1821)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1822), byte(2), line(s, 1822)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1823), byte(1), line(s, 1823)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1824), byte(1), line(s, 1824)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1825), byte(1), line(s, 1825)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1826), byte(1), line(s, 1826)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1827), byte(1), line(s, 1827)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1828), byte(2), line(s, 1828)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1829), byte(1), line(s, 1829)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1830), byte(3), line(s, 1830)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 141, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1831), byte(0), line(s, 1831)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 113, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1832), byte(3), line(s, 1832)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1833), byte(0), line(s, 1833)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1834), byte(0), line(s, 1834)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 32, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1835), byte(0), line(s, 1835)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1836), byte(0), line(s, 1836)
  s.ins MSGSYNC, byte(0), 5376
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1837), byte(0), line(s, 1837)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1838), byte(0), line(s, 1838)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1839), byte(0), line(s, 1839)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 28, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1840), byte(1), line(s, 1840)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 136, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1841), byte(0), line(s, 1841)
  s.ins MSGSYNC, byte(0), 4480
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 25, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1842), byte(2), line(s, 1842)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 141, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1843), byte(0), line(s, 1843)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1844), byte(0), line(s, 1844)
  s.ins MSGSYNC, byte(0), 853
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 4309
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 21, 21
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [389, Register.new(180)]
  s.ins MOV, byte(0), ushort(180), 389
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x38334
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x38334
  s.ins MOV, byte(0), ushort(97), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_09 start.', byte(0)
  s.label :game_03_gameA_09（死：水、ピ：土）水無属性
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [201]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x542b, [30, 3300]
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins CALL, :f_sprite_transform, [35, 35, 3300]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1845), byte(2), line(s, 1845)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1846), byte(2), line(s, 1846)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(1847), byte(0), line(s, 1847)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1848), byte(2), line(s, 1848)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1849), byte(127), line(s, 1849)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1850), byte(127), line(s, 1850)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [220, 0, 0, 100]
  s.ins MSGSET, uint(1851), byte(2), line(s, 1851)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1852), byte(2), line(s, 1852)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1853), byte(3), line(s, 1853)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1854), byte(2), line(s, 1854)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1855), byte(0), line(s, 1855)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1856), byte(2), line(s, 1856)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1857), byte(2), line(s, 1857)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1858), byte(3), line(s, 1858)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins CALL, :addr_0x5914, [201, 0, -299, 150]
  s.ins MSGSET, uint(1859), byte(2), line(s, 1859)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1860), byte(2), line(s, 1860)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1861), byte(1), line(s, 1861)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1862), byte(0), line(s, 1862)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1863), byte(3), line(s, 1863)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1864), byte(2), line(s, 1864)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1865), byte(3), line(s, 1865)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1866), byte(2), line(s, 1866)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1867), byte(3), line(s, 1867)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1868), byte(0), line(s, 1868)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1869), byte(2), line(s, 1869)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [220, 0, 0, 100]
  s.ins MSGSET, uint(1870), byte(2), line(s, 1870)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1871), byte(3), line(s, 1871)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(1872), byte(3), line(s, 1872)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [201, 0, 0, 100]
  s.ins MSGSET, uint(1873), byte(2), line(s, 1873)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [205, 0, 0, 100]
  s.ins MSGSET, uint(1874), byte(2), line(s, 1874)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1875), byte(0), line(s, 1875)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 14, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1876), byte(1), line(s, 1876)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1877), byte(1), line(s, 1877)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1878), byte(1), line(s, 1878)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 16, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1879), byte(0), line(s, 1879)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 119, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1880), byte(2), line(s, 1880)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 33, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGSET, uint(1881), byte(3), line(s, 1881)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1882), byte(1), line(s, 1882)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1883), byte(3), line(s, 1883)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1884), byte(1), line(s, 1884)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [205]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1885), byte(2), line(s, 1885)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1886), byte(2), line(s, 1886)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1887), byte(2), line(s, 1887)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1888), byte(0), line(s, 1888)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1889), byte(3), line(s, 1889)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [569, Register.new(211)]
  s.ins MOV, byte(0), ushort(211), 569
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(1890), byte(1), line(s, 1890)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(211)]
  s.ins MOV, byte(0), ushort(211), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_bgm_play, [2, 100, 0, 1]
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [300, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1891), byte(127), line(s, 1891)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1892), byte(127), line(s, 1892)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [227, 0, 0, 100]
  s.ins MSGSET, uint(1893), byte(2), line(s, 1893)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1894), byte(2), line(s, 1894)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [220, 0, 0, 100]
  s.ins MSGSET, uint(1895), byte(2), line(s, 1895)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1896), byte(1), line(s, 1896)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1897), byte(1), line(s, 1897)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1898), byte(1), line(s, 1898)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1899), byte(1), line(s, 1899)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1900), byte(0), line(s, 1900)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1901), byte(2), line(s, 1901)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1902), byte(2), line(s, 1902)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1903), byte(2), line(s, 1903)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [373, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 373
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(1904), byte(1), line(s, 1904)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x398f8
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x398f8
  s.ins MOV, byte(0), ushort(98), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_10 start.', byte(0)
  s.label :game_03_gameA_10（死：土、ピ：風）土麗美属性
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [4, 69, 50, 1000, 0]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [265]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1905), byte(0), line(s, 1905)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1906), byte(3), line(s, 1906)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1907), byte(0), line(s, 1907)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1908), byte(0), line(s, 1908)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1909), byte(1), line(s, 1909)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1910), byte(2), line(s, 1910)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [259, 0, 0, 100]
  s.ins MSGSET, uint(1911), byte(3), line(s, 1911)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1912), byte(3), line(s, 1912)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1913), byte(2), line(s, 1913)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1914), byte(3), line(s, 1914)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [256, 0, 0, 100]
  s.ins MSGSET, uint(1915), byte(3), line(s, 1915)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1916), byte(3), line(s, 1916)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1917), byte(3), line(s, 1917)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1918), byte(1), line(s, 1918)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1919), byte(0), line(s, 1919)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [4, 0]
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :addr_0x5914, [243, 0, 0, 100]
  s.ins MSGSET, uint(1920), byte(3), line(s, 1920)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Register.new(26)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-249, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MSGSET, uint(1921), byte(3), line(s, 1921)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1922), byte(3), line(s, 1922)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1923), byte(2), line(s, 1923)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [256, 0, 0, 100]
  s.ins MSGSET, uint(1924), byte(3), line(s, 1924)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1925), byte(3), line(s, 1925)
  s.ins MSGSYNC, byte(0), 5749
  s.ins CALL, :addr_0x5914, [243, 0, -249, 150]
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1926), byte(3), line(s, 1926)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1927), byte(1), line(s, 1927)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1928), byte(0), line(s, 1928)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1929), byte(0), line(s, 1929)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1930), byte(2), line(s, 1930)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [299, Register.new(186)]
  s.ins MOV, byte(0), ushort(186), 299
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSET, uint(1931), byte(3), line(s, 1931)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1932), byte(3), line(s, 1932)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1933), byte(3), line(s, 1933)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 73, 60, 0, 0]
  s.ins MSGSET, uint(1934), byte(2), line(s, 1934)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(186)]
  s.ins MOV, byte(0), ushort(186), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [243]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1935), byte(3), line(s, 1935)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1936), byte(3), line(s, 1936)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1937), byte(3), line(s, 1937)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1938), byte(3), line(s, 1938)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [287, Register.new(186)]
  s.ins MOV, byte(0), ushort(186), 287
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSET, uint(1939), byte(3), line(s, 1939)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1940), byte(0), line(s, 1940)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 27, 27
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(186)]
  s.ins MOV, byte(0), ushort(186), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [259]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1941), byte(3), line(s, 1941)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1942), byte(1), line(s, 1942)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [486, Register.new(247), 1400]
  s.ins MOV, byte(0), ushort(247), 486
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [243]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1943), byte(3), line(s, 1943)
  s.ins MSGSYNC, byte(0), 181
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 1536
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 2464
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGSYNC, byte(0), 3498
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSYNC, byte(0), 8352
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Register.new(26)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-249, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MSGSET, uint(1944), byte(3), line(s, 1944)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 88, 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(247)]
  s.ins MOV, byte(0), ushort(247), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 144, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [5, 500, 2, 250]
  s.ins MSGSET, uint(1945), byte(2), line(s, 1945)
  s.ins MSGSYNC, byte(0), 2288
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 141, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [12, 500, 3, 250]
  s.ins MSGSYNC, byte(0), 3792
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [12, 500, 3, 250]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, 1, 134, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1946), byte(3), line(s, 1946)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 37, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1947), byte(2), line(s, 1947)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 35, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1948), byte(0), line(s, 1948)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1949), byte(2), line(s, 1949)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 135, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1950), byte(2), line(s, 1950)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 109, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1951), byte(1), line(s, 1951)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1952), byte(2), line(s, 1952)
  s.ins MSGSYNC, byte(0), 4416
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1953), byte(2), line(s, 1953)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 136, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1954), byte(2), line(s, 1954)
  s.ins MSGSYNC, byte(0), 4586
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 39, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1955), byte(2), line(s, 1955)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 136, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1956), byte(2), line(s, 1956)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 142, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1957), byte(2), line(s, 1957)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1958), byte(3), line(s, 1958)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 38, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1959), byte(2), line(s, 1959)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 36, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1960), byte(2), line(s, 1960)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1961), byte(3), line(s, 1961)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 26, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1962), byte(2), line(s, 1962)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 138, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1963), byte(2), line(s, 1963)
  s.ins MSGSYNC, byte(0), 5066
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 134, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 125, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1964), byte(2), line(s, 1964)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 137, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1965), byte(3), line(s, 1965)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 104, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1966), byte(2), line(s, 1966)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x3b62d
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x3b62d
  s.ins MOV, byte(0), ushort(99), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_11 start.', byte(0)
  s.label :game_03_gameA_11（死：土、ピ：火）土麗美属性
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [265]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1967), byte(3), line(s, 1967)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1968), byte(3), line(s, 1968)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1969), byte(0), line(s, 1969)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1970), byte(1), line(s, 1970)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1971), byte(2), line(s, 1971)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [240, 0, 0, 100]
  s.ins MSGSET, uint(1972), byte(3), line(s, 1972)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1973), byte(3), line(s, 1973)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1974), byte(0), line(s, 1974)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1975), byte(3), line(s, 1975)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1976), byte(2), line(s, 1976)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1977), byte(3), line(s, 1977)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1978), byte(3), line(s, 1978)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1979), byte(3), line(s, 1979)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1980), byte(0), line(s, 1980)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1981), byte(1), line(s, 1981)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [259, 0, 0, 100]
  s.ins MSGSET, uint(1982), byte(3), line(s, 1982)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1983), byte(3), line(s, 1983)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1984), byte(3), line(s, 1984)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1985), byte(2), line(s, 1985)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1986), byte(3), line(s, 1986)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1987), byte(1), line(s, 1987)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1988), byte(3), line(s, 1988)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1989), byte(3), line(s, 1989)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1990), byte(0), line(s, 1990)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1991), byte(1), line(s, 1991)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 40, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1992), byte(127), line(s, 1992)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1993), byte(127), line(s, 1993)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1994), byte(127), line(s, 1994)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 23, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1995), byte(127), line(s, 1995)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, 1, 111, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1996), byte(3), line(s, 1996)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1997), byte(1), line(s, 1997)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 7, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 13, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1998), byte(2), line(s, 1998)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1999), byte(0), line(s, 1999)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2000), byte(127), line(s, 2000)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2001), byte(127), line(s, 2001)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2002), byte(127), line(s, 2002)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 59, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [259]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(2003), byte(3), line(s, 2003)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2004), byte(1), line(s, 2004)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2005), byte(3), line(s, 2005)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2006), byte(1), line(s, 2006)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2007), byte(3), line(s, 2007)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2008), byte(0), line(s, 2008)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(2009), byte(1), line(s, 2009)
  s.ins MSGSYNC, byte(0), 698
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGSYNC, byte(0), 2512
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 9, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 30, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(2010), byte(2), line(s, 2010)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2011), byte(2), line(s, 2011)
  s.ins MSGSYNC, byte(0), 5685
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2012), byte(2), line(s, 2012)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2013), byte(4), line(s, 2013)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(2014), byte(1), line(s, 2014)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2015), byte(1), line(s, 2015)
  s.ins MSGSYNC, byte(0), 4405
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2016), byte(0), line(s, 2016)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 132, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2017), byte(2), line(s, 2017)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [240]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(2018), byte(3), line(s, 2018)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2019), byte(1), line(s, 2019)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2020), byte(3), line(s, 2020)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [238, 0, 0, 100]
  s.ins MSGSET, uint(2021), byte(3), line(s, 2021)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2022), byte(1), line(s, 2022)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2023), byte(3), line(s, 2023)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [240, 0, 0, 100]
  s.ins MSGSET, uint(2024), byte(3), line(s, 2024)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2025), byte(1), line(s, 2025)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 25, 25
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [325, Register.new(184)]
  s.ins MOV, byte(0), ushort(184), 325
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins MSGSET, uint(2026), byte(2), line(s, 2026)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2027), byte(2), line(s, 2027)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 26, 26
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [337, Register.new(185)]
  s.ins MOV, byte(0), ushort(185), 337
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [110, 0, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins LAYERSELECT, 26, 26
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 200, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 200, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [170, 200, 0, 0]
  s.ins CALL, :addr_0x5184, [255, 200, 0, 0]
  s.ins MSGSET, uint(2028), byte(2), line(s, 2028)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x3ce0f
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x3ce0f
  s.ins MOV, byte(0), ushort(100), 1
  s.ins JUMP, :addr_0x847e
  s.ins DEBUG, 's03_gameA_12 start.', byte(0)
  s.label :game_03_gameA_12（死：土、ピ：水）水無属性
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :f_se_play, [4, 69, 50, 1000, 0]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x546f, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [265]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(2029), byte(0), line(s, 2029)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2030), byte(3), line(s, 2030)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2031), byte(2), line(s, 2031)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2032), byte(1), line(s, 2032)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [259, 0, 0, 100]
  s.ins MSGSET, uint(2033), byte(3), line(s, 2033)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2034), byte(0), line(s, 2034)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2035), byte(3), line(s, 2035)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2036), byte(3), line(s, 2036)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2037), byte(1), line(s, 2037)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2038), byte(3), line(s, 2038)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2039), byte(3), line(s, 2039)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2040), byte(2), line(s, 2040)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [2, 100, 0, 1]
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_se_fadeout, [4, 0]
  s.ins MSGSET, uint(2041), byte(3), line(s, 2041)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2042), byte(3), line(s, 2042)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2043), byte(0), line(s, 2043)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2044), byte(3), line(s, 2044)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2045), byte(3), line(s, 2045)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2046), byte(1), line(s, 2046)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2047), byte(3), line(s, 2047)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2048), byte(3), line(s, 2048)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2049), byte(3), line(s, 2049)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2050), byte(3), line(s, 2050)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2051), byte(2), line(s, 2051)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2052), byte(0), line(s, 2052)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2053), byte(3), line(s, 2053)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2054), byte(1), line(s, 2054)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2055), byte(3), line(s, 2055)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2056), byte(2), line(s, 2056)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2057), byte(3), line(s, 2057)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2058), byte(3), line(s, 2058)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [269, 0, 0, 100]
  s.ins MSGSET, uint(2059), byte(3), line(s, 2059)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2060), byte(3), line(s, 2060)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2061), byte(3), line(s, 2061)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2062), byte(3), line(s, 2062)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2063), byte(3), line(s, 2063)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2064), byte(2), line(s, 2064)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2065), byte(0), line(s, 2065)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2066), byte(3), line(s, 2066)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [259, 0, 0, 100]
  s.ins MSGSET, uint(2067), byte(3), line(s, 2067)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2068), byte(3), line(s, 2068)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2069), byte(3), line(s, 2069)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2070), byte(3), line(s, 2070)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2071), byte(2), line(s, 2071)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2072), byte(2), line(s, 2072)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [269, 0, 0, 100]
  s.ins MSGSET, uint(2073), byte(3), line(s, 2073)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2074), byte(3), line(s, 2074)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2075), byte(3), line(s, 2075)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2076), byte(3), line(s, 2076)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2077), byte(2), line(s, 2077)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2078), byte(2), line(s, 2078)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [571, Register.new(211)]
  s.ins MOV, byte(0), ushort(211), 571
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(2079), byte(1), line(s, 2079)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :f_se_play, [0, 22, 100, 0, 0]
  s.ins CALL, :addr_0x5c62, [0]
  s.ins CALL, :addr_0x5cac, [0, 232, 0, 0, 100]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(2080), byte(2), line(s, 2080)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2081), byte(127), line(s, 2081)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2082), byte(127), line(s, 2082)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 115, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 29, 1, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(2083), byte(3), line(s, 2083)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2084), byte(0), line(s, 2084)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 142, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(2085), byte(1), line(s, 2085)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 44, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2086), byte(1), line(s, 2086)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 81, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [227]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(2087), byte(2), line(s, 2087)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(2088), byte(2), line(s, 2088)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 12, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 142, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(2089), byte(3), line(s, 2089)
  s.ins MSGSYNC, byte(0), 3077
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2090), byte(1), line(s, 2090)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2091), byte(3), line(s, 2091)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 31, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2092), byte(2), line(s, 2092)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2093), byte(2), line(s, 2093)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(2094), byte(2), line(s, 2094)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 23, 23
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [366, Register.new(182)]
  s.ins MOV, byte(0), ushort(182), 366
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 24, 24
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [366, Register.new(183)]
  s.ins MOV, byte(0), ushort(183), 366
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 24, 24
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [200, 400, 0, 0]
  s.ins CALL, :addr_0x5184, [0, 400, 0, 0]
  s.ins CALL, :addr_0x542b, [30, 500]
  s.ins MSGSET, uint(2095), byte(1), line(s, 2095)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 24, 24
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(183)]
  s.ins MOV, byte(0), ushort(183), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(2096), byte(1), line(s, 2096)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x3e694
  s.ins CALL, :f_reset, []
  s.ins RETSUB
end
