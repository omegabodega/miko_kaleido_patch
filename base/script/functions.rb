require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_functions(s)
  s.label :f_init
  s.ins STORE_MANY, -1, ushort(71), ushort(179), ushort(180), ushort(181), ushort(182), ushort(183), ushort(184), ushort(185), ushort(186), ushort(187), ushort(188), ushort(189), ushort(190), ushort(191), ushort(192), ushort(193), ushort(194), ushort(195), ushort(196), ushort(197), ushort(198), ushort(199), ushort(200), ushort(201), ushort(202), ushort(203), ushort(204), ushort(205), ushort(206), ushort(207), ushort(208), ushort(209), ushort(210), ushort(211), ushort(212), ushort(213), ushort(214), ushort(215), ushort(216), ushort(217), ushort(218), ushort(219), ushort(220), ushort(221), ushort(222), ushort(223), ushort(224), ushort(225), ushort(226), ushort(227), ushort(228), ushort(229), ushort(230), ushort(231), ushort(232), ushort(233), ushort(234), ushort(235), ushort(236), ushort(237), ushort(238), ushort(239), ushort(240), ushort(241), ushort(242), ushort(243), ushort(244), ushort(245), ushort(246), ushort(247), ushort(248), ushort(249)
  s.ins LAYERSELECT, 10, 90
  s.ins LAYERUNLOAD, -6, byte(0)
  s.ins RETURN
  s.label :f_init_current_sprite_values
  s.ins STORE_MANY, 1, ushort(5), ushort(R_fuuka_current_sprite_rx20), ushort(R_karin_current_sprite_rx20), ushort(R_mina_current_sprite_rx20), ushort(R_doremi_current_sprite_rx20), ushort(R_char4_current_sprite_rx20)
  s.ins STORE_MANY, 0, ushort(5), ushort(R_fuuka_current_sprite_px2), ushort(R_karin_current_sprite_px2), ushort(R_mina_current_sprite_px2), ushort(R_doremi_current_sprite_px2), ushort(R_char4_current_sprite_px2)
  s.ins STORE_MANY, 0, ushort(5), ushort(R_fuuka_current_face), ushort(R_karin_current_face), ushort(R_mina_current_face), ushort(R_doremi_current_face), ushort(R_char4_current_face)
  s.ins STORE_MANY, 0, ushort(5), ushort(R_fuuka_current_sprite_px4), ushort(R_karin_current_sprite_px4), ushort(R_mina_current_sprite_px4), ushort(R_doremi_current_sprite_px4), ushort(R_char4_current_sprite_px4)
  s.ins STORE_MANY, 1, ushort(5), ushort(R_fuuka_current_sprite_px5), ushort(R_karin_current_sprite_px5), ushort(R_mina_current_sprite_px5), ushort(R_doremi_current_sprite_px5), ushort(R_char4_current_sprite_px5)
  s.ins STORE_MANY, 0, ushort(5), ushort(R_fuuka_relative_z), ushort(R_karin_relative_z), ushort(R_mina_relative_z), ushort(R_doremi_relative_z), ushort(R_char4_relative_z)
  s.ins LAYERSELECT, 1, 5
  s.ins LAYERUNLOAD, -6, byte(0)
  s.ins 0x8b, byte(127), byte(127), byte(0), byte(0), byte(0)
  s.ins RETURN
  s.label :f_init_graphics
  s.ins MOV, byte(0), ushort(R_bg_current), 0
  s.ins MOV, byte(0), ushort(24), 0
  s.ins MOV, byte(0), ushort(26), -1
  s.ins LAYERUNLOAD, 0, byte(0)
  s.ins LAYERUNLOAD, 6, byte(0)
  s.ins CALL, :f_init_current_sprite_values, []
  s.ins CALL, :f_init, []
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins MOV, byte(0), ushort(21), -1
  s.ins MOV, byte(0), ushort(22), 1
  s.ins MOV, byte(0), ushort(27), 1000
  s.ins MOV, byte(0), ushort(28), 1000
  s.ins MOV, byte(0), ushort(29), 1000
  s.ins RETURN
  s.label :f_reset
  s.ins CALL, :addr_0x4eeb, []
  s.ins CALL, :f_init_graphics, []
  s.ins BGMSTOP, 0
  s.ins SESTOPALL, 0
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins MSGINIT, 0
  s.ins WAIT, byte(0), 30
  s.ins RETURN
  s.label :f_update_text_positioning_mode
  s.ins PUSH, byte(1), Register.new(2)
  s.ins MOV, byte(0), ushort(2), Register.new(R_text_positioning_mode_rx11)
  s.ins CALC, ushort(2), byte(0), Register.new(2), byte(0), 2, byte(0), Register.new(R_text_positioning_mode_flag), byte(24), byte(255)
  s.ins MOV, byte(8), ushort(2), Register.new(R_text_positioning_mode_rx12)
  s.ins MSGINIT, Register.new(2)
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :addr_0x4df4
  s.ins JUMP_COND, byte(1), Register.new(10), 0, :addr_0x4e0c
  s.ins CALC, ushort(4096), byte(0), Parameter.new(0), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins WAIT, byte(0), Parameter.new(0)
  s.label :addr_0x4e0c
  s.ins RETURN
  s.label :addr_0x4e0d
  s.ins PUSH, byte(1), Register.new(2)
  s.ins JUMP_COND, byte(4), Register.new(12), 100, :addr_0x4e1b
  s.ins MSGCLOSE, byte(0)
  s.label :addr_0x4e1b
  s.ins CALC, ushort(2), byte(0), Register.new(12), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins WIPE, Register.new(11), 2, Register.new(2), byte(15), Register.new(13), Register.new(14), Register.new(15), Register.new(16)
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :f_perform_transition
  s.ins PUSH, byte(1), Register.new(2)
  s.ins JUMP_COND, byte(5), Register.new(R_rx3f_transition_related), 0, :f_perform_transition_no_default
  s.ins JUMP_COND, byte(1), Register.new(R_rx3f_transition_related), Parameter.new(0), :f_perform_transition_no_default
  s.ins MOV, byte(0), ushort(12), Register.new(R_default_transition_duration)
  s.label :f_perform_transition_no_default
  s.ins DEBUG, 'trans:%d', byte(1), Register.new(12)
  s.ins CALC, ushort(2), byte(0), Register.new(12), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins TRANSSET, Register.new(11), 2, Register.new(2), byte(15), Register.new(13), Register.new(14), Register.new(15), Register.new(16)
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :addr_0x4e7c
  s.ins MOV, byte(0), ushort(10), 1
  s.ins PAGEBACK
  s.ins RETURN
  s.label :addr_0x4e83
  s.ins JUMP_COND, byte(0), Register.new(10), 0, :addr_0x4e96
  s.ins CALL, :addr_0x4e0d, []
  s.ins MOV, byte(0), ushort(10), 0
  s.label :addr_0x4e96
  s.ins RETURN
  s.label :f_bgm_play
  s.ins MOV, byte(4), ushort(4097), 10
  s.ins CALC, ushort(4098), byte(0), Parameter.new(2), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins CALC, ushort(4099), byte(0), 1, byte(0), 0, byte(0), Parameter.new(3), byte(24), byte(255)
  s.ins BGMPLAY, Parameter.new(0), Parameter.new(2), Parameter.new(3), Parameter.new(1)
  s.ins RETURN
  s.label :addr_0x4eba
  s.ins CALC, ushort(4096), byte(0), Parameter.new(0), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins BGMSTOP, Parameter.new(0)
  s.ins JUMP_COND, byte(0), Parameter.new(1), 0, :addr_0x4ed4
  s.ins WAIT, byte(0), Parameter.new(0)
  s.label :addr_0x4ed4
  s.ins RETURN
  s.label :addr_0x4ed5
  s.ins MOV, byte(4), ushort(4096), 10
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins BGMVOL, Parameter.new(0), Parameter.new(1)
  s.ins RETURN
  s.label :addr_0x4eeb
  s.ins LAYERCTRL, -4, 86, byte(4), 256
  s.ins LAYERCTRL, -4, 87, byte(4), 256
  s.ins LAYERCTRL, -4, 88, byte(4), 256
  s.ins RETURN
  s.label :addr_0x4f01
  s.ins LAYERCTRL, -4, 86, byte(1), Parameter.new(0)
  s.ins RETURN
  s.label :addr_0x4f08
  s.ins UNARY, 2, 0, 16
  s.ins LAYERCTRL, -4, 87, byte(1), Parameter.new(0)
  s.ins RETURN
  s.label :addr_0x4f13
  s.ins CALC, ushort(4096), byte(0), 1000, byte(0), 1000, byte(0), Parameter.new(0), byte(0), 10, byte(3), byte(26), byte(2), byte(255)
  s.ins LAYERCTRL, -4, 88, byte(1), Parameter.new(0)
  s.ins RETURN
  s.label :f_show_background
  s.ins JUMP_COND, byte(1), Parameter.new(0), :null, :l_f_show_background_px0_not_null
  s.ins MOV, byte(0), ushort(4096), Register.new(R_bg_current)
  s.label :l_f_show_background_px0_not_null
  s.ins JUMP_COND, byte(1), Parameter.new(1), :null, :l_f_show_background_px1_not_null
  s.ins MOV, byte(0), ushort(4097), Register.new(24)
  s.label :l_f_show_background_px1_not_null
  s.ins CALL, :f_lookup_1, [Parameter.new(0), Parameter.new(1)]
  s.ins JUMP_COND, byte(5), Register.new(26), 0, :addr_0x4f66
  s.ins JUMP_COND, byte(1), Register.new(10), 0, :addr_0x4f66
  s.ins PAGEBACK
  s.ins JUMP, :addr_0x4f6d
  s.label :addr_0x4f66
  s.ins CALL, :f_perform_transition, [-1]
  s.label :addr_0x4f6d
  s.ins LAYERLOAD, 0, byte(2), 4, byte(3), Parameter.new(0), Register.new(25)
  s.ins LAYERCTRL, 0, 5, byte(1), 2000
  s.ins LAYERCTRL, 0, 2, byte(1), 3333
  s.ins JUMP_COND, byte(5), Register.new(26), 0, :addr_0x4fa6
  s.ins LAYERLOAD, 6, byte(0), 0, 0
  s.ins LAYERUNLOAD, 6, byte(1)
  s.ins JUMP_COND, byte(1), Register.new(10), 0, :addr_0x4fa6
  s.ins CALL, :addr_0x4e0d, []
  s.ins MOV, byte(0), ushort(26), -1
  s.label :addr_0x4fa6
  s.ins JUMP_COND, byte(0), Register.new(24), 3, :addr_0x4fbd
  s.ins JUMP_COND, byte(0), Register.new(24), 4, :addr_0x4fbd
  s.ins JUMP, :addr_0x4fd4
  s.label :addr_0x4fbd
  s.ins MOV, byte(0), ushort(27), 600
  s.ins MOV, byte(0), ushort(28), 600
  s.ins MOV, byte(0), ushort(29), 600
  s.ins JUMP, :addr_0x4fe6
  s.label :addr_0x4fd4
  s.ins MOV, byte(0), ushort(27), 1000
  s.ins MOV, byte(0), ushort(28), 1000
  s.ins MOV, byte(0), ushort(29), 1000
  s.label :addr_0x4fe6
  s.ins MOV, byte(0), ushort(R_bg_current), Parameter.new(0)
  s.ins MOV, byte(0), ushort(24), Parameter.new(1)
  s.ins RETURN
  s.label :addr_0x4ff1
  s.ins CALL, :f_perform_transition, [0]
  s.ins LAYERLOAD, 6, byte(2), 4, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, 6, 25, byte(1), 1
  s.ins LAYERCTRL, 6, 5, byte(1), 1500
  s.ins MOV, byte(0), ushort(26), Parameter.new(0)
  s.ins CALL, :addr_0x6e44, [6]
  s.ins RETURN
  s.label :addr_0x5016
  s.ins CALL, :f_perform_transition, [1]
  s.ins JUMP_COND, byte(5), Register.new(26), 0, :addr_0x5037
  s.ins CALL, :f_set_sprite_properties, [6]
  s.ins LAYERLOAD, 6, byte(0), 4, 0
  s.ins MOV, byte(0), ushort(26), -1
  s.label :addr_0x5037
  s.ins RETURN
  s.label :addr_0x5038
  s.ins JUMP_COND, byte(5), Parameter.new(0), 0, :addr_0x506c
  s.ins CALL, :f_perform_transition, [0]
  s.ins LAYERLOAD, -6, byte(2), 6, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -6, 22, byte(1), 1

  # EDIT: Add a negative X offset to certain displayed images that are bigger in translation
  offsets = [
    [0, 0],
    [563, 240], # TG_event12_水無2
    [564, 260], # TG_gameB前06_風華
    [568, 235], # TG_初めてのゲーム開始10_風華
    [571, 130], # TG_無罪汎用赤
    [572, 80] # TG_無罪汎用黒
  ]
  s.ins FIND_IN_LIST, ushort(R_temp), Parameter.new(0), 0, PaddedVarlens.new(offsets.map(&:first))
  s.ins TABLEGET, ushort(R_temp), Register.new(R_temp), PaddedVarlens.new(offsets.map(&:last))
  s.ins LAYERCTRL, -6, 20, byte(1), Register.new(R_temp)

  s.ins JUMP_COND, byte(2), Parameter.new(1), 0, :addr_0x506c
  s.ins LAYERCTRL, -6, 25, byte(1), 1
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x6e44, [-6]
  s.label :addr_0x506c
  s.ins RETURN
  s.label :addr_0x506d
  s.ins JUMP_COND, byte(5), Parameter.new(0), 0, :addr_0x5088
  s.ins CALL, :f_perform_transition, [1]
  s.ins CALL, :f_set_sprite_properties, [-6]
  s.ins LAYERLOAD, -6, byte(0), 6, 0
  s.label :addr_0x5088
  s.ins RETURN
  s.label :addr_0x5089
  s.ins MOV, byte(0), ushort(20), 1
  s.ins JUMP_COND, byte(5), Parameter.new(0), 0, :addr_0x50ce
  s.ins JUMP_COND, byte(0), Parameter.new(0), Parameter.new(1), :addr_0x50ce
  s.ins CALL, :f_perform_transition, [0]
  s.ins LAYERLOAD, -6, byte(2), 6, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -6, 22, byte(1), 1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins JUMP_COND, byte(2), Parameter.new(1), 0, :addr_0x50ce
  s.ins LAYERCTRL, -6, 25, byte(1), 1
  s.ins LAYERCTRL, -6, 5, byte(1), Parameter.new(2)
  s.ins CALL, :addr_0x6e44, [-6]
  s.label :addr_0x50ce
  s.ins RETURN
  s.label :addr_0x50cf
  s.ins CALL, :addr_0x506d, [Parameter.new(0)]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins RETURN
  s.label :f_set_sprite_x_offset
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins MOV, byte(4), ushort(4097), Register.new(20)
  s.ins MOV, byte(8), ushort(4098), 256
  s.ins LAYERCTRL, -6, 0, byte(15), Parameter.new(0), Parameter.new(1), Parameter.new(2), Parameter.new(3)
  s.ins RETURN
  s.label :f_set_sprite_y_offset
  s.ins UNARY, 2, 0, 16
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins MOV, byte(4), ushort(4097), Register.new(20)
  s.ins MOV, byte(8), ushort(4098), 256
  s.ins LAYERCTRL, -6, 1, byte(15), Parameter.new(0), Parameter.new(1), Parameter.new(2), Parameter.new(3)
  s.ins RETURN
  s.label :f_set_sprite_zoom
  s.ins MOV, byte(4), ushort(4096), 10
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins MOV, byte(4), ushort(4097), Register.new(20)
  s.ins MOV, byte(8), ushort(4098), 256
  s.ins LAYERCTRL, -6, 12, byte(15), Parameter.new(0), Parameter.new(1), Parameter.new(2), Parameter.new(3)
  s.ins LAYERCTRL, -6, 13, byte(15), Parameter.new(0), Parameter.new(1), Parameter.new(2), Parameter.new(3)
  s.ins RETURN
  s.label :addr_0x5153
  s.ins CALC, ushort(4096), byte(0), Parameter.new(0), byte(0), 1000, byte(3), byte(0), 360, byte(4), byte(11), byte(255)
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins MOV, byte(4), ushort(4097), Register.new(20)
  s.ins MOV, byte(8), ushort(4098), 256
  s.ins LAYERCTRL, -6, 18, byte(15), Parameter.new(0), Parameter.new(1), Parameter.new(2), Parameter.new(3)
  s.ins RETURN
  s.label :addr_0x5184
  s.ins CALC, ushort(4096), byte(0), Parameter.new(0), byte(0), 1000, byte(3), byte(0), 255, byte(4), byte(255)
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins MOV, byte(4), ushort(4097), Register.new(20)
  s.ins MOV, byte(8), ushort(4098), 256
  s.ins LAYERCTRL, -6, 9, byte(15), Parameter.new(0), Parameter.new(1), Parameter.new(2), Parameter.new(3)
  s.ins RETURN
  s.ins LAYERCTRL, -6, 5, byte(1), Parameter.new(0)
  s.ins RETURN
  s.label :addr_0x51ba
  s.ins MOV, byte(132), ushort(4096), Parameter.new(0), 200
  s.ins LAYERCTRL, -6, 66, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -6, 67, byte(1), Parameter.new(0)
  s.ins RETURN
  s.label :addr_0x51ce
  s.ins MOV, byte(132), ushort(4096), Parameter.new(0), 200
  s.ins LAYERCTRL, -6, 66, byte(1), Parameter.new(0)
  s.ins RETURN
  s.label :addr_0x51dc
  s.ins MOV, byte(132), ushort(4096), Parameter.new(0), 200
  s.ins LAYERCTRL, -6, 67, byte(1), Parameter.new(0)
  s.ins RETURN
  s.label :addr_0x51ea
  s.ins CALC, ushort(4096), byte(0), Parameter.new(0), byte(0), 1000, byte(3), byte(0), 255, byte(4), byte(255)
  s.ins JUMP_COND, byte(0), Parameter.new(0), 0, :addr_0x5248
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 1000, byte(3), byte(0), 255, byte(4), byte(255)
  s.ins CALC, ushort(4098), byte(0), Parameter.new(2), byte(0), 1000, byte(3), byte(0), 255, byte(4), byte(255)
  s.ins CALC, ushort(4099), byte(0), Parameter.new(3), byte(0), 1000, byte(3), byte(0), 255, byte(4), byte(255)
  s.ins LAYERCTRL, -6, 24, byte(1), 2
  s.ins LAYERCTRL, -6, 31, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -6, 28, byte(1), Parameter.new(1)
  s.ins LAYERCTRL, -6, 29, byte(1), Parameter.new(2)
  s.ins LAYERCTRL, -6, 30, byte(1), Parameter.new(3)
  s.ins JUMP, :addr_0x5264
  s.label :addr_0x5248
  s.ins LAYERCTRL, -6, 24, byte(0)
  s.ins LAYERCTRL, -6, 31, byte(1), 1000
  s.ins LAYERCTRL, -6, 28, byte(1), 1000
  s.ins LAYERCTRL, -6, 29, byte(1), 1000
  s.ins LAYERCTRL, -6, 30, byte(1), 1000
  s.label :addr_0x5264
  s.ins RETURN
  s.label :addr_0x5265
  s.ins WIPEWAIT
  s.ins TRANSWAIT, byte(122)
  s.ins LAYERWAIT, -6, byte(8), 0, 1, 12, 13, 18, 9, 3, 4
  s.ins LAYERWAIT, -6, byte(3), 40, 32, 36
  s.ins RETURN
  s.label :addr_0x527a
  s.ins LAYERCTRL, -6, 0, byte(4), 640
  s.ins LAYERCTRL, -6, 1, byte(4), 640
  s.ins LAYERCTRL, -6, 12, byte(4), 640
  s.ins LAYERCTRL, -6, 13, byte(4), 640
  s.ins LAYERCTRL, -6, 18, byte(4), 640
  s.ins LAYERCTRL, -6, 9, byte(4), 640
  s.ins LAYERCTRL, -6, 3, byte(4), 640
  s.ins LAYERCTRL, -6, 4, byte(4), 640
  s.ins LAYERCTRL, -6, 40, byte(4), 256
  s.ins LAYERCTRL, -6, 32, byte(4), 256
  s.ins LAYERCTRL, -6, 36, byte(4), 256
  s.ins RETURN
  s.ins LAYERSELECT, 0, 255
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :addr_0x541c, []
  s.ins RETURN
  s.label :f_sprite_transform
  s.ins CALC, ushort(4096), byte(0), Parameter.new(0), byte(0), 1, byte(1), byte(0), 2, byte(4), byte(255)
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 1, byte(1), byte(0), 2, byte(4), byte(255)
  s.ins CALC, ushort(4098), byte(0), Parameter.new(2), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins LAYERCTRL, -6, 32, byte(5), 1, 256
  s.ins LAYERCTRL, -6, 34, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -6, 35, byte(0)
  s.ins LAYERCTRL, -6, 33, byte(1), 1
  s.ins LAYERCTRL, -6, 36, byte(5), 1, 256
  s.ins LAYERCTRL, -6, 38, byte(1), Parameter.new(1)
  s.ins LAYERCTRL, -6, 39, byte(0)
  s.ins LAYERCTRL, -6, 37, byte(1), 1
  s.ins LAYERCTRL, -6, 32, byte(6), Parameter.new(2), 4
  s.ins LAYERCTRL, -6, 36, byte(6), Parameter.new(2), 4
  s.ins RETURN
  s.label :addr_0x532a
  s.ins PUSH, byte(1), Register.new(2)
  s.ins CALC, ushort(4096), byte(0), Parameter.new(0), byte(0), 1, byte(1), byte(0), 2, byte(4), byte(255)
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 1, byte(1), byte(0), 2, byte(4), byte(255)
  s.ins CALC, ushort(2), byte(0), 0, byte(0), 4096, byte(0), Parameter.new(2), byte(0), 0, byte(15), byte(24), byte(255)
  s.ins MOV, byte(8), ushort(2), 256
  s.ins LAYERCTRL, -6, 32, byte(5), 1, Register.new(2)
  s.ins LAYERCTRL, -6, 34, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -6, 35, byte(0)
  s.ins LAYERCTRL, -6, 33, byte(1), 1
  s.ins LAYERCTRL, -6, 36, byte(5), 1, Register.new(2)
  s.ins LAYERCTRL, -6, 38, byte(1), Parameter.new(1)
  s.ins LAYERCTRL, -6, 39, byte(0)
  s.ins LAYERCTRL, -6, 37, byte(1), 1
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :addr_0x5388
  s.ins CALC, ushort(4096), byte(0), Parameter.new(0), byte(0), 1, byte(1), byte(0), 2, byte(4), byte(255)
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 1, byte(1), byte(0), 2, byte(4), byte(255)
  s.ins CALC, ushort(4098), byte(0), Parameter.new(2), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins LAYERCTRL, -2, 32, byte(5), 1, 256
  s.ins LAYERCTRL, -2, 34, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -2, 35, byte(0)
  s.ins LAYERCTRL, -2, 33, byte(1), 1
  s.ins LAYERCTRL, -2, 36, byte(5), 1, 256
  s.ins LAYERCTRL, -2, 38, byte(1), Parameter.new(1)
  s.ins LAYERCTRL, -2, 39, byte(0)
  s.ins LAYERCTRL, -2, 37, byte(1), 1
  s.ins LAYERCTRL, -2, 32, byte(6), Parameter.new(2), 4
  s.ins LAYERCTRL, -2, 36, byte(6), Parameter.new(2), 4
  s.ins RETURN
  s.label :addr_0x53e4
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins CALC, ushort(4098), byte(0), Parameter.new(2), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins LAYERCTRL, -2, 32, byte(5), 4, 256
  s.ins LAYERCTRL, -2, 34, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -2, 35, byte(0)
  s.ins LAYERCTRL, -2, 33, byte(1), Parameter.new(1)
  s.ins LAYERCTRL, -2, 32, byte(6), Parameter.new(2), 12292
  s.ins RETURN
  s.label :addr_0x541c
  s.ins LAYERWAIT, -2, byte(6), 0, 1, 12, 13, 18, 9
  s.ins LAYERWAIT, -2, byte(2), 32, 36
  s.ins RETURN
  s.label :addr_0x542b
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins LAYERCTRL, -5, 32, byte(5), 1, 256
  s.ins LAYERCTRL, -5, 34, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -5, 35, byte(0)
  s.ins LAYERCTRL, -5, 33, byte(1), 1
  s.ins LAYERCTRL, -5, 36, byte(5), 1, 256
  s.ins LAYERCTRL, -5, 38, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -5, 39, byte(0)
  s.ins LAYERCTRL, -5, 37, byte(1), 1
  s.ins LAYERCTRL, -5, 32, byte(6), Parameter.new(1), 4
  s.ins LAYERCTRL, -5, 36, byte(6), Parameter.new(1), 4
  s.ins RETURN
  s.label :addr_0x546f
  s.ins PUSH, byte(1), Register.new(2)
  s.ins MOV, byte(0), ushort(2), Register.new(10)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init_current_sprite_values, []
  s.ins CALL, :f_init, []
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins JUMP_COND, byte(1), Register.new(2), 0, :addr_0x54ce
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.label :addr_0x54ce
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :f_bg_related
  s.ins MOV, byte(4), ushort(4097), 10
  s.ins MOV, byte(4), ushort(4098), 10
  s.ins MOV, byte(4), ushort(4099), 10
  s.ins UNARY, 2, 7, 16
  s.ins UNARY, 2, 8, 16
  s.ins UNARY, 2, 9, 16
  s.ins MSGCLOSE, byte(0)
  s.ins LAYERLOAD, 37, byte(2), 0, byte(1), 90
  s.ins MOV, byte(0), ushort(196), 90
  s.ins LAYERCTRL, 37, 25, byte(1), 7
  s.ins LAYERCTRL, 37, 5, byte(1), 1200
  s.ins LAYERCTRL, 37, 23, byte(1), 1
  s.ins LAYERCTRL, 37, 9, byte(0)
  s.ins LAYERCTRL, 37, 9, byte(3), 1000, 48
  s.ins WAIT, byte(0), 48
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [Parameter.new(0), :null]
  s.ins LAYERCTRL, 0, 12, byte(1), Parameter.new(1)
  s.ins LAYERCTRL, 0, 13, byte(1), Parameter.new(1)
  s.ins LAYERCTRL, 0, 0, byte(1), Parameter.new(4)
  s.ins LAYERCTRL, 0, 1, byte(1), Parameter.new(7)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERCTRL, 37, 9, byte(2), 90
  s.ins LAYERCTRL, 0, 12, byte(15), Parameter.new(2), 300, 5, -1
  s.ins LAYERCTRL, 0, 13, byte(15), Parameter.new(2), 300, 5, -1
  s.ins LAYERCTRL, 0, 0, byte(15), Parameter.new(5), 300, 5, -1
  s.ins LAYERCTRL, 0, 1, byte(15), Parameter.new(8), 300, 5, -1
  s.ins LAYERWAIT, 0, byte(1), 1
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERCTRL, 0, 12, byte(3), Parameter.new(3), 48
  s.ins LAYERCTRL, 0, 13, byte(3), Parameter.new(3), 48
  s.ins LAYERCTRL, 0, 0, byte(3), Parameter.new(6), 48
  s.ins LAYERCTRL, 0, 1, byte(3), Parameter.new(9), 48
  s.ins LAYERWAIT, 0, byte(1), 1
  s.ins LAYERUNLOAD, 37, byte(0)
  s.ins MOV, byte(0), ushort(196), -1
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 800
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x55d3
  s.ins LAYERCTRL, 0, 0, byte(5), Parameter.new(0), 256
  s.ins LAYERCTRL, 0, 1, byte(5), 420, 256
  s.ins LAYERCTRL, 0, 66, byte(1), 400
  s.ins LAYERCTRL, 0, 67, byte(1), 400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins LAYERSELECT, 1, 1
  s.ins CALL, :addr_0x6b36, [0, 2, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins CALL, :addr_0x6b36, [1, 2, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins CALL, :addr_0x6b36, [2, 2, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins CALL, :addr_0x6b36, [3, 2, 0]
  s.ins LAYERSELECT, 1, 5
  s.ins LAYERCTRL, -6, 12, byte(1), 700
  s.ins LAYERCTRL, -6, 13, byte(1), 700
  s.ins LAYERCTRL, -6, 1, byte(1), -50
  s.ins CALC, ushort(R_temp), byte(0), 1000, byte(0), 1000, byte(0), 150, byte(0), 10, byte(3), byte(26), byte(2), byte(255)
  s.ins LAYERCTRL, -4, 88, byte(5), Register.new(R_temp), 256
  s.ins LAYERCTRL, -4, 86, byte(5), Parameter.new(1), 256
  s.ins LAYERCTRL, -4, 87, byte(5), -79, 256
  s.ins RETURN
  s.label :addr_0x5670
  s.ins LAYERCTRL, 0, 0, byte(4), 256
  s.ins LAYERCTRL, 0, 1, byte(4), 256
  s.ins LAYERCTRL, 0, 66, byte(0)
  s.ins LAYERCTRL, 0, 67, byte(0)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins LAYERSELECT, 1, 1
  s.ins CALL, :addr_0x6b36, [0, 1, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins CALL, :addr_0x6b36, [1, 1, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins CALL, :addr_0x6b36, [2, 1, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins CALL, :addr_0x6b36, [3, 1, 0]
  s.ins LAYERSELECT, 1, 5
  s.ins LAYERCTRL, -6, 12, byte(1), 1000
  s.ins LAYERCTRL, -6, 13, byte(1), 1000
  s.ins LAYERCTRL, -6, 1, byte(0)
  s.ins LAYERCTRL, -4, 88, byte(4), 256
  s.ins LAYERCTRL, -4, 86, byte(4), 256
  s.ins LAYERCTRL, -4, 87, byte(4), 256
  s.ins RETURN
  s.label :addr_0x56ef
  s.ins LAYERCTRL, 0, 0, byte(4), 256
  s.ins LAYERCTRL, 0, 1, byte(4), 256
  s.ins LAYERCTRL, 0, 12, byte(5), 1400, 256
  s.ins LAYERCTRL, 0, 13, byte(5), 1400, 256
  s.ins LAYERCTRL, 0, 18, byte(4), 256
  s.ins LAYERCTRL, 0, 66, byte(1), 400
  s.ins LAYERCTRL, 0, 67, byte(1), 400
  s.ins RETURN
  s.label :addr_0x5720
  s.ins LAYERCTRL, 0, 0, byte(4), 256
  s.ins LAYERCTRL, 0, 1, byte(4), 256
  s.ins LAYERCTRL, 0, 12, byte(5), 1000, 256
  s.ins LAYERCTRL, 0, 13, byte(5), 1000, 256
  s.ins LAYERCTRL, 0, 18, byte(4), 256
  s.ins LAYERCTRL, 0, 66, byte(0)
  s.ins LAYERCTRL, 0, 67, byte(0)
  s.ins RETURN
  s.label :addr_0x574d
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [Parameter.new(0), Parameter.new(1)]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [Parameter.new(2), 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [Parameter.new(3), 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [Parameter.new(4), 0, 0, 0]
  s.ins JUMP_COND, byte(0), Parameter.new(5), 0, :addr_0x57c8
  s.ins LAYERLOAD, 75, byte(2), 0, byte(1), 515
  s.ins MOV, byte(0), ushort(234), 515
  s.ins LAYERCTRL, 75, 25, byte(1), 7
  s.ins LAYERCTRL, 75, 5, byte(1), 1400
  s.label :addr_0x57c8
  s.ins CALL, :f_lookup_table, []
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x57d6
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(0), 18
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [Parameter.new(0), Parameter.new(1)]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [Parameter.new(2), 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [Parameter.new(3), 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [Parameter.new(4), 0, 0, 0]
  s.ins JUMP_COND, byte(0), Parameter.new(5), 0, :addr_0x5884
  s.ins LAYERLOAD, 75, byte(2), 0, byte(1), 515
  s.ins MOV, byte(0), ushort(234), 515
  s.ins LAYERCTRL, 75, 25, byte(1), 7
  s.ins LAYERCTRL, 75, 5, byte(1), 1400
  s.label :addr_0x5884
  s.ins CALL, :f_lookup_table, []
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x5892
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Parameter.new(0)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [Parameter.new(1), 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [Parameter.new(2), 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [Parameter.new(3), 0, 0, 0]
  s.ins CALL, :f_lookup_table, []
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x58f7
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins RETURN
  s.label :addr_0x5906
  s.ins CALL, :f_lookup_table, []
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x5914
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Parameter.new(0)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [Parameter.new(1), 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [Parameter.new(2), 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [Parameter.new(3), 0, 0, 0]
  s.ins RETURN
  s.label :addr_0x5952
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERLOAD, 85, byte(2), 4, byte(1), 500
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, 85, 25, byte(1), 7
  s.ins LAYERCTRL, 85, 5, byte(1), 1500
  s.ins LAYERLOAD, 80, byte(2), 0, byte(1), 584
  s.ins MOV, byte(0), ushort(239), 584
  s.ins LAYERCTRL, 80, 25, byte(1), 1
  s.ins LAYERCTRL, 80, 5, byte(1), 1400
  s.ins LAYERCTRL, 80, 1, byte(1), 1200
  s.ins LAYERCTRL, 80, 18, byte(1), -90
  s.ins LAYERLOAD, 81, byte(2), 0, byte(1), 584
  s.ins MOV, byte(0), ushort(240), 584
  s.ins LAYERCTRL, 81, 25, byte(1), 1
  s.ins LAYERCTRL, 81, 5, byte(1), 1400
  s.ins LAYERCTRL, 81, 1, byte(1), 1200
  s.ins LAYERCTRL, 81, 18, byte(1), -30
  s.ins LAYERLOAD, 82, byte(2), 0, byte(1), 584
  s.ins MOV, byte(0), ushort(241), 584
  s.ins LAYERCTRL, 82, 25, byte(1), 1
  s.ins LAYERCTRL, 82, 5, byte(1), 1400
  s.ins LAYERCTRL, 82, 1, byte(1), 1200
  s.ins LAYERCTRL, 82, 18, byte(1), 30
  s.ins LAYERLOAD, 83, byte(2), 0, byte(1), 584
  s.ins MOV, byte(0), ushort(242), 584
  s.ins LAYERCTRL, 83, 25, byte(1), 1
  s.ins LAYERCTRL, 83, 5, byte(1), 1400
  s.ins LAYERCTRL, 83, 1, byte(1), 1200
  s.ins LAYERCTRL, 83, 18, byte(1), 91
  s.ins LAYERCTRL, 80, 0, byte(15), -1499, 150, 133, -1
  s.ins LAYERCTRL, 80, 1, byte(15), -2306, 150, 133, -1
  s.ins LAYERCTRL, 81, 0, byte(6), 30, 128
  s.ins LAYERCTRL, 81, 0, byte(15), -499, 150, 133, -1
  s.ins LAYERCTRL, 81, 1, byte(6), 30, 128
  s.ins LAYERCTRL, 81, 1, byte(15), -2576, 150, 133, -1
  s.ins LAYERCTRL, 82, 0, byte(6), 75, 128
  s.ins LAYERCTRL, 82, 0, byte(15), 500, 150, 133, -1
  s.ins LAYERCTRL, 82, 1, byte(6), 75, 128
  s.ins LAYERCTRL, 82, 1, byte(15), -2576, 150, 133, -1
  s.ins LAYERCTRL, 83, 0, byte(6), 120, 128
  s.ins LAYERCTRL, 83, 0, byte(15), 1500, 150, 133, -1
  s.ins LAYERCTRL, 83, 1, byte(6), 120, 128
  s.ins LAYERCTRL, 83, 1, byte(15), -2306, 150, 133, -1
  s.ins RETURN
  s.label :addr_0x5aaa
  s.ins PUSH, byte(1), Register.new(2)
  s.ins MOV, byte(0), ushort(2), Register.new(10)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERLOAD, 40, byte(2), 0, byte(1), 588
  s.ins MOV, byte(0), ushort(199), 588
  s.ins LAYERCTRL, 40, 25, byte(1), 1
  s.ins LAYERCTRL, 40, 5, byte(1), 1700
  s.ins LAYERCTRL, 40, 40, byte(5), 8, 12544
  s.ins LAYERCTRL, 40, 41, byte(1), 780
  s.ins LAYERCTRL, 40, 42, byte(1), 1000
  s.ins LAYERCTRL, 40, 43, byte(0)
  s.ins LAYERCTRL, 40, 44, byte(5), 6, 12544
  s.ins LAYERCTRL, 40, 46, byte(1), 1000
  s.ins LAYERCTRL, 40, 45, byte(1), 16800
  s.ins LAYERCTRL, 40, 47, byte(0)
  s.ins LAYERLOAD, 41, byte(2), 0, byte(1), 589
  s.ins MOV, byte(0), ushort(200), 589
  s.ins LAYERCTRL, 41, 25, byte(1), 1
  s.ins LAYERCTRL, 41, 5, byte(1), 1600
  s.ins LAYERCTRL, 41, 40, byte(5), 9, 12544
  s.ins LAYERCTRL, 41, 41, byte(1), 780
  s.ins LAYERCTRL, 41, 42, byte(1), 1000
  s.ins LAYERCTRL, 41, 43, byte(0)
  s.ins LAYERCTRL, 41, 44, byte(5), 6, 12544
  s.ins LAYERCTRL, 41, 46, byte(1), -999
  s.ins LAYERCTRL, 41, 45, byte(1), 18000
  s.ins LAYERCTRL, 41, 47, byte(0)
  s.ins LAYERLOAD, 42, byte(2), 0, byte(1), 590
  s.ins MOV, byte(0), ushort(201), 590
  s.ins LAYERCTRL, 42, 25, byte(1), 1
  s.ins LAYERCTRL, 42, 5, byte(1), 1500
  s.ins LAYERCTRL, 42, 40, byte(5), 10, 12544
  s.ins LAYERCTRL, 42, 41, byte(1), 780
  s.ins LAYERCTRL, 42, 42, byte(1), 1000
  s.ins LAYERCTRL, 42, 43, byte(0)
  s.ins LAYERCTRL, 42, 44, byte(5), 6, 12544
  s.ins LAYERCTRL, 42, 46, byte(1), 1000
  s.ins LAYERCTRL, 42, 45, byte(1), 12000
  s.ins LAYERCTRL, 42, 47, byte(0)
  s.ins LAYERLOAD, 39, byte(2), 0, byte(1), 587
  s.ins MOV, byte(0), ushort(198), 587
  s.ins LAYERCTRL, 39, 25, byte(1), 1
  s.ins LAYERCTRL, 39, 5, byte(1), 1800
  s.ins LAYERCTRL, 39, 44, byte(5), 6, 12544
  s.ins LAYERCTRL, 39, 46, byte(1), -999
  s.ins LAYERCTRL, 39, 45, byte(1), 14400
  s.ins LAYERCTRL, 39, 47, byte(0)
  s.ins JUMP_COND, byte(1), Register.new(2), 0, :addr_0x5bdd
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.label :addr_0x5bdd
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :addr_0x5be2
  s.ins LAYERUNLOAD, 39, byte(0)
  s.ins MOV, byte(0), ushort(198), -1
  s.ins LAYERUNLOAD, 40, byte(0)
  s.ins MOV, byte(0), ushort(199), -1
  s.ins LAYERUNLOAD, 41, byte(0)
  s.ins MOV, byte(0), ushort(200), -1
  s.ins LAYERUNLOAD, 42, byte(0)
  s.ins MOV, byte(0), ushort(201), -1
  s.ins RETURN
  s.label :addr_0x5c03
  s.ins LAYERLOAD, 32, byte(2), 0, byte(1), 602
  s.ins MOV, byte(0), ushort(191), 602
  s.ins LAYERCTRL, 32, 25, byte(1), 1
  s.ins LAYERCTRL, 32, 5, byte(1), 1400
  s.ins LAYERCTRL, 32, 12, byte(1), 2000
  s.ins LAYERCTRL, 32, 13, byte(1), 2000
  s.ins LAYERCTRL, 32, 66, byte(1), 2000
  s.ins LAYERCTRL, 32, 32, byte(5), 6, 256
  s.ins LAYERCTRL, 32, 33, byte(1), 20
  s.ins JUMP_COND, byte(1), Parameter.new(0), 0, :addr_0x5c54
  s.ins LAYERCTRL, 32, 34, byte(1), 3368
  s.ins LAYERCTRL, 32, 35, byte(1), -1683
  s.ins JUMP, :addr_0x5c61
  s.label :addr_0x5c54
  s.ins LAYERCTRL, 32, 34, byte(1), -3367
  s.ins LAYERCTRL, 32, 35, byte(1), 1684
  s.label :addr_0x5c61
  s.ins RETURN
  s.label :addr_0x5c62
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :f_init, []
  s.ins CALL, :addr_0x4eeb, []
  s.ins CALL, :addr_0x5c03, [Parameter.new(0)]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERCTRL, 32, 12, byte(7), 1000, 12, 3
  s.ins LAYERCTRL, 32, 13, byte(7), 1000, 12, 3
  s.ins WAIT, byte(0), 114
  s.ins LAYERCTRL, 32, 32, byte(0)
  s.ins RETURN
  s.label :addr_0x5cac
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERUNLOAD, 32, byte(0)
  s.ins MOV, byte(0), ushort(191), -1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Parameter.new(1)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [Parameter.new(2), 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [Parameter.new(3), 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [Parameter.new(4), 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x5388, [100, 30, 1000]
  s.ins CALL, :addr_0x541c, []
  s.ins RETURN
  s.label :addr_0x5d1b
  s.ins SEPLAY, 0, 22, 0, 1, 1000, 0, 1000
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :f_init_graphics, []
  s.ins CALL, :addr_0x5670, []
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [4, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Parameter.new(1)]
  s.ins CALL, :addr_0x5c03, [Parameter.new(0)]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERCTRL, 32, 12, byte(7), 1000, 12, 3
  s.ins LAYERCTRL, 32, 13, byte(7), 1000, 12, 3
  s.ins WAIT, byte(0), 114
  s.ins LAYERUNLOAD, 32, byte(0)
  s.ins MOV, byte(0), ushort(191), -1
  s.ins CALL, :addr_0x5388, [100, 30, 1000]
  s.ins CALL, :addr_0x541c, []
  s.ins SESTOP, 0, 30
  s.ins RETURN
  s.label :addr_0x5dba
  s.ins MOV, byte(131), ushort(R_temp), Register.new(R_route_index), 1
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(12), :addr_0x5dfa, :addr_0x5dfa, :addr_0x5dfa, :addr_0x5e08, :addr_0x5e08, :addr_0x5e08, :addr_0x5e16, :addr_0x5e16, :addr_0x5e16, :addr_0x5e24, :addr_0x5e24, :addr_0x5e24
  s.ins JUMP, :addr_0x5e32
  s.label :addr_0x5dfa
  s.ins CALL, :addr_0x5d1b, [1, 133]
  s.ins JUMP, :addr_0x5e32
  s.label :addr_0x5e08
  s.ins CALL, :addr_0x5d1b, [0, 175]
  s.ins JUMP, :addr_0x5e32
  s.label :addr_0x5e16
  s.ins CALL, :addr_0x5d1b, [0, 231]
  s.ins JUMP, :addr_0x5e32
  s.label :addr_0x5e24
  s.ins CALL, :addr_0x5d1b, [1, 264]
  s.ins JUMP, :addr_0x5e32
  s.label :addr_0x5e32
  s.ins RETURN
  s.label :addr_0x5e33
  s.ins MOV, byte(131), ushort(R_temp), Register.new(R_route_index), 1
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(12), :addr_0x5e73, :addr_0x5e73, :addr_0x5e73, :addr_0x5e82, :addr_0x5e73, :addr_0x5e91, :addr_0x5e82, :addr_0x5e91, :addr_0x5e73, :addr_0x5e9e, :addr_0x5e9e, :addr_0x5e73
  s.ins JUMP, :addr_0x5ead
  s.label :addr_0x5e73
  s.ins CALL, :f_bgm_play, [9, 0, 0, 1]
  s.ins JUMP, :addr_0x5ead
  s.label :addr_0x5e82
  s.ins CALL, :f_bgm_play, [10, 0, 0, 1]
  s.ins JUMP, :addr_0x5ead
  s.label :addr_0x5e91
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins JUMP, :addr_0x5ead
  s.label :addr_0x5e9e
  s.ins CALL, :f_bgm_play, [3, 0, 0, 1]
  s.ins JUMP, :addr_0x5ead
  s.label :addr_0x5ead
  s.ins RETURN
  s.label :addr_0x5eae
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init_graphics, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [Parameter.new(0), Parameter.new(1)]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [Parameter.new(2), 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [Parameter.new(3), 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [Parameter.new(4), 0, 0, 0]
  s.ins LAYERCTRL, 0, 22, byte(0)
  s.ins LAYERLOAD, 32, byte(2), 0, byte(1), 603
  s.ins MOV, byte(0), ushort(191), 603
  s.ins LAYERCTRL, 32, 25, byte(1), 1
  s.ins LAYERCTRL, 32, 12, byte(1), 500
  s.ins LAYERCTRL, 32, 13, byte(1), 500
  s.ins LAYERCTRL, 32, 1, byte(1), 100
  s.ins LAYERCTRL, 32, 5, byte(1), 1400
  s.ins LAYERCTRL, 32, 18, byte(1), 27
  s.ins LAYERCTRL, 32, 66, byte(1), 2000
  s.ins LAYERLOAD, 33, byte(2), 0, byte(1), 603
  s.ins MOV, byte(0), ushort(192), 603
  s.ins LAYERCTRL, 33, 25, byte(1), 1
  s.ins LAYERCTRL, 33, 12, byte(1), 450
  s.ins LAYERCTRL, 33, 13, byte(1), 450
  s.ins LAYERCTRL, 33, 0, byte(1), -99
  s.ins LAYERCTRL, 33, 1, byte(1), 30
  s.ins LAYERCTRL, 33, 5, byte(1), 1200
  s.ins LAYERCTRL, 33, 18, byte(1), 22
  s.ins LAYERCTRL, 33, 66, byte(1), 2000
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERCTRL, 32, 12, byte(15), 2000, 12, 5, 1
  s.ins LAYERCTRL, 32, 13, byte(15), 2000, 12, 5, 1
  s.ins LAYERCTRL, 32, 0, byte(15), 3500, 12, 133, 1
  s.ins LAYERCTRL, 32, 1, byte(15), 550, 12, 5, 1
  s.ins LAYERCTRL, 32, 66, byte(1), 4000
  s.ins LAYERCTRL, 32, 67, byte(1), 1000
  s.ins LAYERCTRL, 33, 12, byte(15), 1800, 12, 5, 1
  s.ins LAYERCTRL, 33, 13, byte(15), 1800, 12, 5, 1
  s.ins LAYERCTRL, 33, 0, byte(15), 3500, 12, 133, 1
  s.ins LAYERCTRL, 33, 1, byte(15), 750, 12, 5, 1
  s.ins LAYERCTRL, 33, 66, byte(1), 4000
  s.ins LAYERCTRL, 33, 67, byte(1), 1000
  s.ins LAYERWAIT, 33, byte(2), 0, 1
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERCTRL, 0, 22, byte(1), 1
  s.ins LAYERUNLOAD, 32, byte(0)
  s.ins MOV, byte(0), ushort(191), -1
  s.ins LAYERUNLOAD, 33, byte(0)
  s.ins MOV, byte(0), ushort(192), -1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins RETURN
  s.label :addr_0x604e
  s.ins LAYERLOAD, 45, byte(1), 0, 31, 61440, -959, -60, 1920, 120
  s.ins MOV, byte(0), ushort(204), 1000
  s.ins LAYERCTRL, 45, 25, byte(1), 1
  s.ins LAYERCTRL, 45, 1, byte(1), -599
  s.ins LAYERCTRL, 45, 5, byte(1), Parameter.new(0)
  s.ins LAYERLOAD, 46, byte(1), 0, 31, 61440, -959, -60, 1920, 120
  s.ins MOV, byte(0), ushort(205), 1000
  s.ins LAYERCTRL, 46, 25, byte(1), 1
  s.ins LAYERCTRL, 46, 1, byte(1), 600
  s.ins LAYERCTRL, 46, 5, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, 45, 1, byte(3), -479, 18
  s.ins LAYERCTRL, 46, 1, byte(3), 480, 18
  s.ins RETURN
  s.label :addr_0x60a7
  s.ins LAYERCTRL, 45, 1, byte(3), -599, 18
  s.ins LAYERCTRL, 46, 1, byte(3), 600, 18
  s.ins LAYERUNLOAD, 45, byte(18)
  s.ins MOV, byte(0), ushort(204), -1
  s.ins LAYERUNLOAD, 46, byte(18)
  s.ins MOV, byte(0), ushort(205), -1
  s.ins RETURN
  s.label :addr_0x60c6
  s.ins PUSH, byte(1), Register.new(2)
  s.ins MOV, byte(0), ushort(2), Register.new(10)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERLOAD, 47, byte(2), 0, byte(1), 509
  s.ins MOV, byte(0), ushort(206), 509
  s.ins LAYERCTRL, 47, 25, byte(1), 7
  s.ins LAYERCTRL, 47, 5, byte(1), 1900
  s.ins LAYERCTRL, 47, 66, byte(1), 200
  s.ins LAYERCTRL, 47, 67, byte(1), 200
  s.ins LAYERLOAD, 48, byte(2), 0, byte(1), 510
  s.ins MOV, byte(0), ushort(207), 510
  s.ins LAYERCTRL, 48, 25, byte(1), 7
  s.ins LAYERCTRL, 48, 5, byte(1), 1300
  s.ins LAYERCTRL, 48, 40, byte(5), 4, 12544
  s.ins LAYERCTRL, 48, 42, byte(1), 500
  s.ins LAYERCTRL, 48, 41, byte(1), 96
  s.ins LAYERCTRL, 48, 43, byte(1), 500
  s.ins LAYERLOAD, 49, byte(2), 0, byte(1), 511
  s.ins MOV, byte(0), ushort(208), 511
  s.ins LAYERCTRL, 49, 25, byte(1), 7
  s.ins LAYERCTRL, 49, 5, byte(1), 1200
  s.ins LAYERCTRL, 49, 40, byte(5), 4, 12544
  s.ins LAYERCTRL, 49, 42, byte(1), 500
  s.ins LAYERCTRL, 49, 41, byte(1), 120
  s.ins LAYERCTRL, 49, 43, byte(1), 500
  s.ins JUMP_COND, byte(1), Register.new(2), 0, :addr_0x6178
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.label :addr_0x6178
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :addr_0x617d
  s.ins PUSH, byte(1), Register.new(2)
  s.ins MOV, byte(0), ushort(2), Register.new(10)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERUNLOAD, 47, byte(0)
  s.ins MOV, byte(0), ushort(206), -1
  s.ins LAYERUNLOAD, 48, byte(0)
  s.ins MOV, byte(0), ushort(207), -1
  s.ins LAYERUNLOAD, 49, byte(0)
  s.ins MOV, byte(0), ushort(208), -1
  s.ins JUMP_COND, byte(1), Register.new(2), 0, :addr_0x61bc
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x4e83, []
  s.label :addr_0x61bc
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :addr_0x61c1
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERLOAD, 72, byte(2), 0, byte(1), 90
  s.ins MOV, byte(0), ushort(231), 90
  s.ins LAYERCTRL, 72, 25, byte(1), 7
  s.ins LAYERCTRL, 72, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(0), 18
  s.ins CALL, :addr_0x4e7c, []
  s.ins RETURN
  s.label :addr_0x6211
  s.ins LAYERUNLOAD, 72, byte(0)
  s.ins MOV, byte(0), ushort(231), -1
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x6238
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERLOAD, 72, byte(2), 0, byte(1), 90
  s.ins MOV, byte(0), ushort(231), 90
  s.ins LAYERCTRL, 72, 25, byte(1), 7
  s.ins LAYERCTRL, 72, 5, byte(1), 1400
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(0), 18
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERUNLOAD, 72, byte(0)
  s.ins MOV, byte(0), ushort(231), -1
  s.ins LAYERLOAD, 75, byte(2), 0, byte(1), 515
  s.ins MOV, byte(0), ushort(234), 515
  s.ins LAYERCTRL, 75, 25, byte(1), 7
  s.ins LAYERCTRL, 75, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [Parameter.new(0), Parameter.new(1)]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x62ed
  s.ins PUSH, byte(1), Register.new(2)
  s.ins MOV, byte(0), ushort(2), Register.new(10)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERCTRL, -4, 24, byte(1), 1
  s.ins LAYERCTRL, -4, 28, byte(1), 1200
  s.ins LAYERCTRL, -4, 29, byte(1), 1000
  s.ins LAYERCTRL, -4, 30, byte(1), 700
  s.ins LAYERCTRL, -4, 31, byte(1), 1000
  s.ins JUMP_COND, byte(1), Register.new(2), 0, :addr_0x6331
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x4e83, []
  s.label :addr_0x6331
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :addr_0x6336
  s.ins PUSH, byte(1), Register.new(2)
  s.ins MOV, byte(0), ushort(2), Register.new(10)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERCTRL, -4, 24, byte(0)
  s.ins LAYERCTRL, -4, 28, byte(1), 1000
  s.ins LAYERCTRL, -4, 29, byte(1), 1000
  s.ins LAYERCTRL, -4, 30, byte(1), 1000
  s.ins LAYERCTRL, -4, 31, byte(1), 1000
  s.ins JUMP_COND, byte(1), Register.new(2), 0, :addr_0x6379
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins CALL, :addr_0x4e83, []
  s.label :addr_0x6379
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :addr_0x637e
  s.ins CALL, :f_perform_transition, [0]
  s.ins LAYERLOAD, 64, byte(4), 0, 0
  s.ins MOV, byte(0), ushort(223), 2000
  s.ins LAYERCTRL, 64, 25, byte(1), 7
  s.ins LAYERCTRL, 64, 5, byte(1), 1400
  s.ins RETURN
  s.label :addr_0x639f
  s.ins CALL, :f_perform_transition, [1]
  s.ins LAYERLOAD, 64, byte(0), 6, 0
  s.ins LAYERUNLOAD, 64, byte(1)
  s.ins MOV, byte(0), ushort(223), -1
  s.ins RETURN
  s.label :addr_0x63b6
  s.ins MSGCLOSE, byte(0)
  s.ins WAIT, byte(0), 30
  s.ins CALL, :addr_0x4eba, [1000, 0]
  s.ins BGMSTOP, 60
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(11), 7
  s.ins MOV, byte(0), ushort(12), 1500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x6405
  s.ins MSGCLOSE, byte(0)
  s.ins WAIT, byte(0), 30
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins BGMSTOP, 60
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(11), 7
  s.ins MOV, byte(0), ushort(12), 1500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(0), 180
  s.ins RETURN
  s.label :addr_0x6459
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERLOAD, 50, byte(2), 0, byte(1), 573
  s.ins MOV, byte(0), ushort(209), 573
  s.ins LAYERCTRL, 50, 25, byte(1), 7
  s.ins LAYERCTRL, 50, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(1), 300
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERUNLOAD, 50, byte(0)
  s.ins MOV, byte(0), ushort(209), -1
  s.ins RETURN
  s.label :addr_0x64cf
  s.ins CALL, :addr_0x6459, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [Parameter.new(0), Parameter.new(1)]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x650b
  s.ins CALL, :addr_0x6459, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Parameter.new(0)]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 0
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x6592
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5e33, []
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERLOAD, 51, byte(2), 0, byte(1), 574
  s.ins MOV, byte(0), ushort(210), 574
  s.ins LAYERCTRL, 51, 25, byte(1), 7
  s.ins LAYERCTRL, 51, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(1), 300
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERUNLOAD, 51, byte(0)
  s.ins MOV, byte(0), ushort(210), -1
  s.ins RETURN
  s.label :addr_0x660e
  s.ins CALL, :addr_0x6592, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [Parameter.new(0), Parameter.new(1)]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x664a
  s.ins CALL, :addr_0x6592, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [Parameter.new(0)]
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [Parameter.new(1), 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [Parameter.new(2), 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [Parameter.new(3), 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins RETURN
  s.label :addr_0x66ab
  s.ins CALC, ushort(4096), byte(0), Parameter.new(0), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins MSGCLOSE, byte(0)
  s.ins WAIT, byte(0), 30
  s.ins CALL, :addr_0x4eba, [1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERLOAD, 50, byte(2), 0, byte(1), Parameter.new(1)
  s.ins LAYERCTRL, 50, 25, byte(1), 7
  s.ins LAYERCTRL, 50, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(1), 300
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERUNLOAD, 50, byte(0)
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(0), 60
  s.ins RETURN
  s.label :f_show_bustup_sprite
  s.ins PUSH, byte(2), Register.new(2), Register.new(3)
  s.ins JUMP_COND, byte(1), Parameter.new(2), :null, :l_f_show_bustup_sprite_px2_not_null
  s.ins TABLEGET, ushort(4098), Parameter.new(0), ushort(5), Register.new(R_fuuka_current_sprite_px2), byte(0), byte(0), Register.new(R_karin_current_sprite_px2), byte(0), byte(0), Register.new(R_mina_current_sprite_px2), byte(0), byte(0), Register.new(R_doremi_current_sprite_px2), byte(0), byte(0), Register.new(R_char4_current_sprite_px2), byte(0), byte(0)
  s.label :l_f_show_bustup_sprite_px2_not_null
  s.ins JUMP_COND, byte(1), Parameter.new(3), :null, :l_f_show_bustup_sprite_px3_not_null
  s.ins TABLEGET, ushort(4099), Parameter.new(0), ushort(5), Register.new(R_fuuka_current_face), byte(0), byte(0), Register.new(R_karin_current_face), byte(0), byte(0), Register.new(R_mina_current_face), byte(0), byte(0), Register.new(R_doremi_current_face), byte(0), byte(0), Register.new(R_char4_current_face), byte(0), byte(0)
  s.label :l_f_show_bustup_sprite_px3_not_null
  s.ins JUMP_COND, byte(1), Parameter.new(4), :null, :l_f_show_bustup_sprite_px4_not_null
  s.ins TABLEGET, ushort(4100), Parameter.new(0), ushort(5), Register.new(R_fuuka_current_sprite_px4), byte(0), byte(0), Register.new(R_karin_current_sprite_px4), byte(0), byte(0), Register.new(R_mina_current_sprite_px4), byte(0), byte(0), Register.new(R_doremi_current_sprite_px4), byte(0), byte(0), Register.new(R_char4_current_sprite_px4), byte(0), byte(0)
  s.label :l_f_show_bustup_sprite_px4_not_null
  s.ins JUMP_COND, byte(1), Parameter.new(5), :null, :l_f_show_bustup_sprite_px5_not_null
  s.ins TABLEGET, ushort(4101), Parameter.new(0), ushort(5), Register.new(R_fuuka_current_sprite_px5), byte(0), byte(0), Register.new(R_karin_current_sprite_px5), byte(0), byte(0), Register.new(R_mina_current_sprite_px5), byte(0), byte(0), Register.new(R_doremi_current_sprite_px5), byte(0), byte(0), Register.new(R_char4_current_sprite_px5), byte(0), byte(0)
  s.label :l_f_show_bustup_sprite_px5_not_null
  s.ins CALL, :f_lookup_2, [Parameter.new(0), Parameter.new(2), Parameter.new(3)]
  s.ins JUMP_COND, byte(5), Register.new(30), 0, :l_f_show_bustup_sprite_no_save_return
  s.ins CALL, :f_lookup_first_sprite_index_by_char, [Parameter.new(0), Register.new(30)]
  s.ins JUMP_COND, byte(0), Register.new(R_index_of_first_sprite_in_group), 0, :l_f_show_bustup_sprite_no_save_return
  s.ins TABLEGET, ushort(R_current_sprite_mode), Parameter.new(0), ushort(5), Register.new(R_fuuka_current_sprite_rx20), byte(0), byte(0), Register.new(R_karin_current_sprite_rx20), byte(0), byte(0), Register.new(R_mina_current_sprite_rx20), byte(0), byte(0), Register.new(R_doremi_current_sprite_rx20), byte(0), byte(0), Register.new(R_char4_current_sprite_rx20), byte(0), byte(0)
  s.ins JUMP_COND, byte(6), Parameter.new(1), 64, :l_f_show_bustup_sprite_start_bupclear
  s.ins JUMP_COND, byte(1), Parameter.new(6), 0, :l_f_show_bustup_sprite_start_bupload
  s.ins JUMP_COND, byte(0), Parameter.new(1), 0, :l_f_show_bustup_sprite_save
  s.ins JUMP_COND, byte(6), Parameter.new(1), 1, :l_f_show_bustup_sprite_start_bupload
  s.ins JUMP_COND, byte(6), Parameter.new(1), 2, :l_f_show_bustup_sprite_start_faceload
  s.ins JUMP, :l_f_show_bustup_sprite_save_return
  s.label :l_f_show_bustup_sprite_save
  s.ins JUMP_COND, byte(6), Register.new(R_current_sprite_mode), 1, :l_f_show_bustup_sprite_start_bupload
  s.ins JUMP_COND, byte(6), Register.new(R_current_sprite_mode), 2, :l_f_show_bustup_sprite_start_faceload
  s.ins JUMP, :l_f_show_bustup_sprite_save_return
  s.label :l_f_show_bustup_sprite_start_bupload
  s.ins DEBUG, 'char:%d bupload(%d,%d)', byte(3), Parameter.new(0), Parameter.new(2), Parameter.new(3)
  s.ins CALL, :f_perform_transition, [0]
  s.ins MOV, byte(0), ushort(3), 6
  s.ins JUMP_COND, byte(0), Parameter.new(6), 0, :l_f_show_bustup_sprite_px6_is_0
  s.ins MOV, byte(8), ushort(3), 16
  s.label :l_f_show_bustup_sprite_px6_is_0
  s.ins JUMP_COND, byte(134), Parameter.new(1), 1, :addr_0x68a1
  s.ins MOV, byte(0), ushort(4101), 1
  s.label :addr_0x68a1
  s.ins MOV, byte(130), ushort(2), Register.new(R_index_of_first_sprite_in_group), Parameter.new(5)
  s.ins LAYERLOAD, -6, byte(3), Register.new(3), byte(15), Register.new(2), Parameter.new(2), Parameter.new(3), Parameter.new(4)
  s.ins JUMP_TABLE, Parameter.new(6), ushort(3), :l_f_show_bustup_sprite_start_z, :addr_0x68c1, :addr_0x6907
  s.label :addr_0x68c1
  s.ins LAYERCTRL, -6, 12, byte(1), 1000
  s.ins LAYERCTRL, -6, 13, byte(1), 1000
  s.ins LAYERCTRL, -6, 18, byte(0)
  s.ins LAYERCTRL, -6, 9, byte(1), 1000
  s.ins LAYERCTRL, -6, 24, byte(0)
  s.ins LAYERCTRL, -6, 31, byte(1), 1000
  s.ins LAYERCTRL, -6, 28, byte(1), 1000
  s.ins LAYERCTRL, -6, 29, byte(1), 1000
  s.ins LAYERCTRL, -6, 30, byte(1), 1000
  s.ins 0x45, 0, Parameter.new(0), ushort(5), ushort(58), ushort(59), ushort(60), ushort(61), ushort(62)
  s.ins JUMP, :l_f_show_bustup_sprite_start_z
  s.label :addr_0x6907
  s.ins LAYERINIT, -6
  s.ins JUMP, :l_f_show_bustup_sprite_start_z
  s.label :l_f_show_bustup_sprite_start_z
  s.ins TABLEGET, ushort(2), Parameter.new(5), ushort(4), 1900, byte(0), byte(0), 1800, byte(0), byte(0), 1700, byte(0), byte(0), 1600, byte(0), byte(0)
  s.ins TABLEGET, ushort(R_temp), Parameter.new(0), ushort(5), Register.new(R_fuuka_relative_z), byte(0), byte(0), Register.new(R_karin_relative_z), byte(0), byte(0), Register.new(R_mina_relative_z), byte(0), byte(0), Register.new(R_doremi_relative_z), byte(0), byte(0), Register.new(R_char4_relative_z), byte(0), byte(0)
  s.ins MOV, byte(2), ushort(2), Register.new(R_temp)
  s.ins DEBUG, 'char%d z:%d', byte(2), Parameter.new(0), Register.new(2)
  s.ins LAYERCTRL, -6, 5, byte(1), Register.new(2)
  s.ins TABLEGET, ushort(2), Parameter.new(5), ushort(4), 1538, byte(0), byte(0), 1000, byte(0), byte(0), 666, byte(0), byte(0), 500, byte(0), byte(0)
  s.ins LAYERCTRL, -6, 2, byte(1), Register.new(2)
  s.ins LAYERCTRL, -6, 6, byte(1), Register.new(27)
  s.ins LAYERCTRL, -6, 7, byte(1), Register.new(28)
  s.ins LAYERCTRL, -6, 8, byte(1), Register.new(29)
  s.ins JUMP_COND, byte(134), Parameter.new(1), 128, :addr_0x699d
  s.ins JUMP_COND, byte(1), Parameter.new(6), 0, :addr_0x699d
  s.ins JUMP, :addr_0x69af
  s.label :addr_0x699d
  s.ins CALL, :addr_0x527a, []
  s.ins LAYERCTRL, -6, 32, byte(4), 256
  s.ins LAYERCTRL, -6, 36, byte(4), 256
  s.label :addr_0x69af
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_current_sprite_mode), byte(0), 129, byte(8), byte(0), 129, byte(14), byte(255)
  s.ins JUMP_COND, byte(1), Register.new(R_temp), 0, :addr_0x69cd
  s.ins CALL, :addr_0x6e44, [-6]
  s.label :addr_0x69cd
  s.ins 0x8b, byte(208), byte(127), byte(0), byte(0), byte(0)
  s.ins MOV, byte(0), ushort(R_current_sprite_mode), 129
  s.ins JUMP, :l_f_show_bustup_sprite_save_return
  s.label :l_f_show_bustup_sprite_start_faceload
  s.ins DEBUG, 'char:%d faceload(%d,%d)', byte(3), Parameter.new(0), Parameter.new(2), Parameter.new(3)
  s.ins MOV, byte(130), ushort(2), Register.new(R_index_of_first_sprite_in_group), 4
  s.ins 0x8b, byte(208), byte(178), byte(210), byte(211), byte(212)
  s.ins CALC, ushort(R_temp), byte(0), Register.new(R_current_sprite_mode), byte(0), 129, byte(8), byte(0), 129, byte(14), byte(255)
  s.ins JUMP_COND, byte(0), Register.new(R_temp), 0, :addr_0x6a4d
  s.ins DEBUG, 'char:%d bupclear', byte(1), Parameter.new(0)
  s.ins CALL, :f_perform_transition, [1]
  s.ins CALL, :f_set_sprite_properties, [-6]
  s.ins LAYERLOAD, -6, byte(0), 6, 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.label :addr_0x6a4d
  s.ins MOV, byte(0), ushort(R_current_sprite_mode), 130
  s.ins JUMP, :l_f_show_bustup_sprite_save_return
  s.label :l_f_show_bustup_sprite_start_bupclear
  s.ins JUMP_COND, byte(134), Register.new(R_current_sprite_mode), 128, :addr_0x6aba
  s.ins JUMP_COND, byte(134), Register.new(R_current_sprite_mode), 1, :l_f_show_bustup_sprite_start_faceclear
  s.ins DEBUG, 'char:%d bupclear', byte(1), Parameter.new(0)
  s.ins CALL, :f_perform_transition, [1]
  s.ins CALL, :f_set_sprite_properties, [-6]
  s.ins LAYERLOAD, -6, byte(0), 6, 0
  s.label :l_f_show_bustup_sprite_start_faceclear
  s.ins JUMP_COND, byte(134), Register.new(R_current_sprite_mode), 2, :addr_0x6aba
  s.ins DEBUG, 'char:%d faceclear', byte(1), Parameter.new(0)
  s.ins 0x8b, byte(208), byte(127), byte(0), byte(0), byte(0)
  s.label :addr_0x6aba
  s.ins JUMP_COND, byte(134), Parameter.new(1), 3, :addr_0x6ad8
  s.ins MOV, byte(135), ushort(R_current_sprite_mode), Parameter.new(1), 3
  s.ins JUMP_COND, byte(134), Parameter.new(1), 1, :addr_0x6ad8
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(4101), 1
  s.label :addr_0x6ad8
  s.ins MOV, byte(7), ushort(R_current_sprite_mode), 127
  s.ins JUMP, :l_f_show_bustup_sprite_save_return
  s.label :l_f_show_bustup_sprite_save_return
  s.ins 0x45, Register.new(R_current_sprite_mode), Parameter.new(0), ushort(5), ushort(33), ushort(34), ushort(35), ushort(36), ushort(37)
  s.ins 0x45, Parameter.new(2), Parameter.new(0), ushort(5), ushort(38), ushort(39), ushort(40), ushort(41), ushort(42)
  s.ins 0x45, Parameter.new(3), Parameter.new(0), ushort(5), ushort(43), ushort(44), ushort(45), ushort(46), ushort(47)
  s.ins 0x45, Parameter.new(4), Parameter.new(0), ushort(5), ushort(48), ushort(49), ushort(50), ushort(51), ushort(52)
  s.ins 0x45, Parameter.new(5), Parameter.new(0), ushort(5), ushort(53), ushort(54), ushort(55), ushort(56), ushort(57)
  s.label :l_f_show_bustup_sprite_no_save_return
  s.ins POP, byte(2), ushort(3), ushort(2)
  s.ins RETURN
  s.label :addr_0x6b36
  s.ins PUSH, byte(1), Register.new(2)
  s.ins TABLEGET, ushort(2), Parameter.new(0), ushort(5), Register.new(R_fuuka_current_sprite_rx20), byte(0), byte(0), Register.new(R_karin_current_sprite_rx20), byte(0), byte(0), Register.new(R_mina_current_sprite_rx20), byte(0), byte(0), Register.new(R_doremi_current_sprite_rx20), byte(0), byte(0), Register.new(R_char4_current_sprite_rx20), byte(0), byte(0)
  s.ins CALC, ushort(2), byte(0), Register.new(2), byte(0), 129, byte(8), byte(0), 129, byte(14), byte(255)
  s.ins JUMP_COND, byte(0), Register.new(2), 0, :addr_0x6b9d
  s.ins TABLEGET, ushort(2), Parameter.new(0), ushort(5), Register.new(R_fuuka_current_sprite_px5), byte(0), byte(0), Register.new(R_karin_current_sprite_px5), byte(0), byte(0), Register.new(R_mina_current_sprite_px5), byte(0), byte(0), Register.new(R_doremi_current_sprite_px5), byte(0), byte(0), Register.new(R_char4_current_sprite_px5), byte(0), byte(0)
  s.ins JUMP_COND, byte(0), Register.new(2), Parameter.new(1), :addr_0x6bac
  s.ins CALL, :f_show_bustup_sprite, [Parameter.new(0), 0, :null, :null, :null, Parameter.new(1), Parameter.new(2)]
  s.ins JUMP, :addr_0x6bac
  s.label :addr_0x6b9d
  s.ins 0x45, Parameter.new(1), Parameter.new(0), ushort(5), ushort(53), ushort(54), ushort(55), ushort(56), ushort(57)
  s.label :addr_0x6bac
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :addr_0x6bb1
  s.ins PUSH, byte(2), Register.new(2), Register.new(3)
  s.ins TABLEGET, ushort(2), Parameter.new(0), ushort(5), Register.new(R_fuuka_current_sprite_px5), byte(0), byte(0), Register.new(R_karin_current_sprite_px5), byte(0), byte(0), Register.new(R_mina_current_sprite_px5), byte(0), byte(0), Register.new(R_doremi_current_sprite_px5), byte(0), byte(0), Register.new(R_char4_current_sprite_px5), byte(0), byte(0)
  s.ins TABLEGET, ushort(2), Register.new(2), ushort(4), 1900, byte(0), byte(0), 1800, byte(0), byte(0), 1700, byte(0), byte(0), 1600, byte(0), byte(0)
  s.ins JUMP_COND, byte(1), Parameter.new(1), 0, :addr_0x6c0c
  s.ins MOV, byte(0), ushort(3), Register.new(22)
  s.ins DEBUG, 'r_BACK=%d', byte(1), Register.new(3)
  s.ins MOV, byte(2), ushort(22), 1
  s.ins JUMP, :addr_0x6c27
  s.label :addr_0x6c0c
  s.ins MOV, byte(0), ushort(3), Register.new(21)
  s.ins DEBUG, 'r_FRONT=%d', byte(1), Register.new(3)
  s.ins MOV, byte(3), ushort(21), 1
  s.label :addr_0x6c27
  s.ins MOV, byte(2), ushort(2), Register.new(3)
  s.ins LAYERCTRL, -6, 5, byte(1), Register.new(2)
  s.ins 0x45, Register.new(3), Parameter.new(0), ushort(5), ushort(58), ushort(59), ushort(60), ushort(61), ushort(62)
  s.ins POP, byte(2), ushort(3), ushort(2)
  s.ins RETURN
  s.label :f_hide_all_sprites
  s.ins PUSH, byte(2), Register.new(2), Register.new(3)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins MOV, byte(0), ushort(2), 0
  s.label :f_hide_all_sprites_loop
  s.ins JUMP_COND, byte(2), Register.new(2), 4, :f_hide_all_sprites_done
  s.ins MOV, byte(130), ushort(3), Register.new(2), 1
  s.ins LAYERSELECT, Register.new(3), Register.new(3)
  s.ins CALL, :f_show_bustup_sprite, [Register.new(2), 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(2), ushort(2), 1
  s.ins JUMP, :f_hide_all_sprites_loop
  s.label :f_hide_all_sprites_done
  s.ins POP, byte(2), ushort(3), ushort(2)
  s.ins RETURN
  s.label :addr_0x6c90
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins CALC, ushort(4099), byte(0), Parameter.new(3), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins UNARY, 2, 0, 16
  s.ins MOV, byte(4), ushort(4099), Parameter.new(2)
  s.ins LAYERCTRL, -6, 36, byte(5), 5, 256
  s.ins LAYERCTRL, -6, 38, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -6, 39, byte(0)
  s.ins LAYERCTRL, -6, 37, byte(1), Parameter.new(1)
  s.ins LAYERCTRL, -6, 36, byte(6), Parameter.new(3), 4
  s.ins RETURN
  s.label :addr_0x6ccf
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins CALC, ushort(4098), byte(0), Parameter.new(2), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins UNARY, 2, 0, 16
  s.ins MOV, byte(4), ushort(4098), 2
  s.ins LAYERCTRL, -6, 36, byte(5), 5, 256
  s.ins LAYERCTRL, -6, 38, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -6, 39, byte(0)
  s.ins LAYERCTRL, -6, 37, byte(1), Parameter.new(1)
  s.ins LAYERCTRL, -6, 36, byte(6), Parameter.new(2), 4
  s.ins RETURN
  s.label :addr_0x6d0e
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins LAYERCTRL, -6, 32, byte(5), 4, 12544
  s.ins LAYERCTRL, -6, 34, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, -6, 35, byte(0)
  s.ins LAYERCTRL, -6, 33, byte(1), Parameter.new(1)
  s.ins RETURN
  s.label :addr_0x6d32
  s.ins PUSH, byte(1), Register.new(2)
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins CALC, ushort(4098), byte(0), Parameter.new(2), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins LAYERCTRL, -6, 3, byte(7), Parameter.new(0), Parameter.new(1), 256
  s.ins UNARY, 514, 0, Parameter.new(0)
  s.ins LAYERCTRL, -6, 3, byte(3), Register.new(2), Parameter.new(2)
  s.ins LAYERCTRL, -6, 3, byte(3), Parameter.new(0), Parameter.new(2)
  s.ins LAYERCTRL, -6, 3, byte(3), Register.new(2), Parameter.new(2)
  s.ins LAYERCTRL, -6, 3, byte(2), Parameter.new(1)
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :addr_0x6d78
  s.ins LAYERCTRL, -6, 3, byte(7), -12, 1, 256
  s.ins LAYERCTRL, -6, 3, byte(3), 12, 2
  s.ins LAYERCTRL, -6, 3, byte(3), -12, 2
  s.ins LAYERCTRL, -6, 3, byte(3), 12, 2
  s.ins LAYERCTRL, -6, 3, byte(3), -12, 2
  s.ins LAYERCTRL, -6, 3, byte(3), 12, 2
  s.ins LAYERCTRL, -6, 3, byte(3), -12, 2
  s.ins LAYERCTRL, -6, 3, byte(3), 12, 2
  s.ins LAYERCTRL, -6, 3, byte(2), 1
  s.ins RETURN
  s.label :addr_0x6db0
  s.ins LAYERCTRL, -6, 36, byte(5), 4, 256
  s.ins LAYERCTRL, -6, 38, byte(1), -10
  s.ins LAYERCTRL, -6, 39, byte(0)
  s.ins LAYERCTRL, -6, 37, byte(1), 36
  s.ins LAYERCTRL, -6, 36, byte(6), 18, 4
  s.ins LAYERCTRL, -6, 12, byte(7), 100, 18, 640
  s.ins LAYERCTRL, -6, 13, byte(7), 100, 18, 640
  s.ins RETURN
  s.label :addr_0x6dde
  s.ins LAYERCTRL, -6, 4, byte(5), 300, 256
  s.ins LAYERCTRL, -6, 4, byte(7), -15, 24, 3
  s.ins LAYERCTRL, -6, 4, byte(6), 6, 3
  s.ins RETURN
  s.label :addr_0x6df4
  s.ins MOV, byte(4), ushort(4096), 2
  s.ins LAYERCTRL, -6, 0, byte(7), Parameter.new(0), 24, 128
  s.ins LAYERCTRL, -6, 36, byte(5), 5, 256
  s.ins LAYERCTRL, -6, 38, byte(1), -50
  s.ins LAYERCTRL, -6, 39, byte(0)
  s.ins LAYERCTRL, -6, 37, byte(1), 24
  s.ins LAYERCTRL, -6, 36, byte(6), 24, 4
  s.ins RETURN
  s.label :addr_0x6e1d
  s.ins CALC, ushort(4096), byte(0), Parameter.new(0), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins LAYERCTRL, -6, 9, byte(6), Parameter.new(0), 384
  s.ins LAYERCTRL, -6, 9, byte(2), Parameter.new(1)
  s.ins RETURN
  s.label :addr_0x6e44
  s.ins PUSH, byte(2), Register.new(2), Register.new(3)
  s.ins JUMP_COND, byte(1), Register.new(R_rx3f_transition_related), 0, :addr_0x6e80
  s.ins CALC, ushort(2), byte(0), Register.new(R_default_transition_duration), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins UNARY, 515, 0, Register.new(66)
  s.ins LAYERCTRL, Parameter.new(0), 3, byte(5), Register.new(65), 512
  s.ins LAYERCTRL, Parameter.new(0), 3, byte(6), Register.new(2), 2
  s.ins LAYERCTRL, Parameter.new(0), 4, byte(5), Register.new(3), 512
  s.ins LAYERCTRL, Parameter.new(0), 4, byte(6), Register.new(2), 2
  s.label :addr_0x6e80
  s.ins POP, byte(2), ushort(3), ushort(2)
  s.ins RETURN
  s.label :f_set_sprite_properties
  s.ins PUSH, byte(2), Register.new(2), Register.new(3)
  s.ins JUMP_COND, byte(1), Register.new(R_rx3f_transition_related), 1, :addr_0x6ec3
  s.ins CALC, ushort(2), byte(0), Register.new(R_default_transition_duration), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins UNARY, 515, 0, Register.new(66)
  s.ins LAYERCTRL, Parameter.new(0), 3, byte(4), 512
  s.ins LAYERCTRL, Parameter.new(0), 3, byte(7), Register.new(65), Register.new(2), 1
  s.ins LAYERCTRL, Parameter.new(0), 4, byte(4), 512
  s.ins LAYERCTRL, Parameter.new(0), 4, byte(7), Register.new(3), Register.new(2), 1
  s.label :addr_0x6ec3
  s.ins POP, byte(2), ushort(3), ushort(2)
  s.ins RETURN
  s.label :f_se_play
  s.ins PUSH, byte(1), Register.new(2)
  s.ins MOV, byte(4), ushort(4098), 10
  s.ins CALC, ushort(4099), byte(0), Parameter.new(3), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins CALC, ushort(2), byte(0), 1, byte(0), 0, byte(0), Parameter.new(0), byte(0), 3, byte(16), byte(24), byte(255)
  s.ins SEPLAY, Parameter.new(0), Parameter.new(1), Parameter.new(3), Register.new(2), Parameter.new(2), 0, 1000
  s.ins JUMP_COND, byte(0), Parameter.new(4), 0, :addr_0x6f01
  s.ins SEWAIT, Parameter.new(0), 2
  s.label :addr_0x6f01
  s.ins POP, byte(1), ushort(2)
  s.ins RETURN
  s.label :f_se_fadeout
  s.ins CALC, ushort(4097), byte(0), Parameter.new(1), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins SESTOP, Parameter.new(0), Parameter.new(1)
  s.ins RETURN
  s.label :addr_0x6f17
  s.ins MOV, byte(4), ushort(4097), 10
  s.ins CALC, ushort(4098), byte(0), Parameter.new(2), byte(0), 6, byte(3), byte(0), 100, byte(4), byte(255)
  s.ins SEVOL, Parameter.new(0), Parameter.new(1), Parameter.new(2)
  s.ins RETURN
  s.ins MSGCLOSE, byte(0)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, 0]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERLOAD, 50, byte(2), 0, byte(1), Parameter.new(0)
  s.ins LAYERCTRL, 50, 25, byte(1), 7
  s.ins LAYERCTRL, 50, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins WAIT, byte(1), 300
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERUNLOAD, 50, byte(0)
  s.ins RETURN
  s.label :f_lookup_1
  s.ins FIND_IN_LIST, ushort(R_temp), Parameter.new(0), 0, PaddedVarlens.new([3, 5, 30, 31, 32, 33, 34, 35, 36])
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(9), :l_f_lookup_1_p0_3, :l_f_lookup_1_p0_5, :l_f_lookup_1_p0_30, :l_f_lookup_1_p0_31_32_33, :l_f_lookup_1_p0_31_32_33, :l_f_lookup_1_p0_31_32_33, :l_f_lookup_1_p0_34, :l_f_lookup_1_p0_35, :l_f_lookup_1_p0_36
  s.ins MOV, byte(0), ushort(25), 0
  s.ins JUMP, :l_f_lookup_1_return
  s.label :l_f_lookup_1_p0_3
  s.ins FIND_IN_LIST, ushort(R_temp), Parameter.new(1), 0, PaddedVarlens.new([0, 3, 4])
  s.ins JUMP_COND, byte(2), Register.new(R_temp), 0, :addr_0x70af
  s.ins MOV, byte(0), ushort(25), 0
  s.ins JUMP, :l_f_lookup_1_return
  s.label :l_f_lookup_1_p0_5
  s.ins MOV, byte(0), ushort(25), 0
  s.ins JUMP, :l_f_lookup_1_return
  s.label :l_f_lookup_1_p0_30
  s.ins FIND_IN_LIST, ushort(R_temp), Parameter.new(1), 0, PaddedVarlens.new([0, 1])
  s.ins JUMP_COND, byte(2), Register.new(R_temp), 0, :addr_0x70af
  s.ins MOV, byte(0), ushort(25), 0
  s.ins JUMP, :l_f_lookup_1_return
  s.label :l_f_lookup_1_p0_31_32_33
  s.ins FIND_IN_LIST, ushort(R_temp), Parameter.new(1), 0, PaddedVarlens.new([0, 1, 6, 7])
  s.ins JUMP_COND, byte(2), Register.new(R_temp), 0, :addr_0x70af
  s.ins MOV, byte(0), ushort(25), 0
  s.ins JUMP, :l_f_lookup_1_return
  s.label :l_f_lookup_1_p0_34
  s.ins MOV, byte(0), ushort(25), 0
  s.ins JUMP, :l_f_lookup_1_return
  s.label :l_f_lookup_1_p0_35
  s.ins MOV, byte(0), ushort(25), 1
  s.ins JUMP, :l_f_lookup_1_return
  s.label :l_f_lookup_1_p0_36
  s.ins FIND_IN_LIST, ushort(R_temp), Parameter.new(1), 0, PaddedVarlens.new([0, 1, 2, 5, 6])
  s.ins JUMP_COND, byte(2), Register.new(R_temp), 0, :addr_0x70af
  s.ins MOV, byte(0), ushort(25), 0
  s.ins JUMP, :l_f_lookup_1_return
  s.label :addr_0x70af
  s.ins MOV, byte(0), ushort(25), Parameter.new(1)
  s.label :l_f_lookup_1_return
  s.ins RETURN
  s.label :f_lookup_2
  s.ins MOV, byte(5), ushort(4098), 100
  s.ins JUMP_TABLE, Parameter.new(0), ushort(5), :l_f_lookup_2_fuuka_char4, :l_f_lookup_2_karin_doremi, :l_f_lookup_2_mina, :l_f_lookup_2_karin_doremi, :l_f_lookup_2_fuuka_char4
  s.ins MOV, byte(0), ushort(30), 0
  s.ins JUMP, :l_f_lookup_2_return
  s.label :l_f_lookup_2_karin_doremi
  s.ins JUMP_TABLE, Parameter.new(2), ushort(4), :addr_0x70fb, :addr_0x70fb, :addr_0x7112, :addr_0x711c
  s.ins MOV, byte(0), ushort(30), 0
  s.ins JUMP, :l_f_lookup_2_return
  s.label :addr_0x70fb
  s.ins TABLEGET, ushort(30), Parameter.new(1), ushort(3), 1, byte(0), byte(0), byte(0), 1, byte(0), byte(0), byte(0), 0, byte(0), byte(0), byte(0)
  s.ins JUMP, :l_f_lookup_2_return
  s.label :addr_0x7112
  s.ins MOV, byte(0), ushort(30), 0
  s.ins JUMP, :l_f_lookup_2_return
  s.label :addr_0x711c
  s.ins TABLEGET, ushort(30), Parameter.new(1), ushort(3), 0, byte(0), byte(0), byte(0), 0, byte(0), byte(0), byte(0), 1, byte(0), byte(0), byte(0)
  s.ins JUMP, :l_f_lookup_2_return
  s.label :l_f_lookup_2_mina
  s.ins JUMP_TABLE, Parameter.new(2), ushort(4), :addr_0x7151, :addr_0x7151, :addr_0x7151, :addr_0x7168
  s.ins MOV, byte(0), ushort(30), 0
  s.ins JUMP, :l_f_lookup_2_return
  s.label :addr_0x7151
  s.ins TABLEGET, ushort(30), Parameter.new(1), ushort(3), 1, byte(0), byte(0), byte(0), 1, byte(0), byte(0), byte(0), 0, byte(0), byte(0), byte(0)
  s.ins JUMP, :l_f_lookup_2_return
  s.label :addr_0x7168
  s.ins TABLEGET, ushort(30), Parameter.new(1), ushort(3), 0, byte(0), byte(0), byte(0), 0, byte(0), byte(0), byte(0), 1, byte(0), byte(0), byte(0)
  s.ins JUMP, :l_f_lookup_2_return
  s.label :l_f_lookup_2_fuuka_char4
  s.ins JUMP_TABLE, Parameter.new(2), ushort(4), :addr_0x719d, :addr_0x719d, :addr_0x71b8, :addr_0x71c2
  s.ins MOV, byte(0), ushort(30), 0
  s.ins JUMP, :l_f_lookup_2_return
  s.label :addr_0x719d
  s.ins TABLEGET, ushort(30), Parameter.new(1), ushort(4), 1, byte(0), byte(0), byte(0), 1, byte(0), byte(0), byte(0), 0, byte(0), byte(0), byte(0), 1, byte(0), byte(0), byte(0)
  s.ins JUMP, :l_f_lookup_2_return
  s.label :addr_0x71b8
  s.ins MOV, byte(0), ushort(30), 0
  s.ins JUMP, :l_f_lookup_2_return
  s.label :addr_0x71c2
  s.ins TABLEGET, ushort(30), Parameter.new(1), ushort(4), 0, byte(0), byte(0), byte(0), 0, byte(0), byte(0), byte(0), 1, byte(0), byte(0), byte(0), 0, byte(0), byte(0), byte(0)
  s.ins JUMP, :l_f_lookup_2_return
  s.label :l_f_lookup_2_return
  s.ins CALC, ushort(30), byte(0), -1, byte(0), Parameter.new(2), byte(0), Register.new(30), byte(24), byte(255)
  s.ins RETURN
  s.label :f_lookup_first_sprite_index_by_char
  s.ins JUMP_TABLE, Parameter.new(0), ushort(5), :l_f_lookup_first_sprite_index_fuuka_char4, :l_f_lookup_first_sprite_index_karin, :l_f_lookup_first_sprite_index_mina, :l_f_lookup_first_sprite_index_doremi, :l_f_lookup_first_sprite_index_fuuka_char4
  s.ins MOV, byte(0), ushort(R_index_of_first_sprite_in_group), 0
  s.ins JUMP, :l_f_lookup_first_sprite_index_return
  s.label :l_f_lookup_first_sprite_index_karin
  s.ins TABLEGET, ushort(R_index_of_first_sprite_in_group), Parameter.new(1), ushort(4), 16, byte(0), byte(0), byte(0), 21, byte(0), byte(0), byte(0), 0, byte(0), byte(0), byte(0), 26, byte(0), byte(0), byte(0)
  s.ins JUMP, :l_f_lookup_first_sprite_index_return
  s.label :l_f_lookup_first_sprite_index_mina
  s.ins TABLEGET, ushort(R_index_of_first_sprite_in_group), Parameter.new(1), ushort(4), 31, byte(0), byte(0), byte(0), 36, byte(0), byte(0), byte(0), 41, byte(0), byte(0), byte(0), 46, byte(0), byte(0), byte(0)
  s.ins JUMP, :l_f_lookup_first_sprite_index_return
  s.label :l_f_lookup_first_sprite_index_doremi
  s.ins TABLEGET, ushort(R_index_of_first_sprite_in_group), Parameter.new(1), ushort(4), 51, byte(0), byte(0), byte(0), 56, byte(0), byte(0), byte(0), 0, byte(0), byte(0), byte(0), 61, byte(0), byte(0), byte(0)
  s.ins JUMP, :l_f_lookup_first_sprite_index_return
  s.label :l_f_lookup_first_sprite_index_fuuka_char4
  s.ins TABLEGET, ushort(R_index_of_first_sprite_in_group), Parameter.new(1), ushort(4), 1, byte(0), byte(0), byte(0), 6, byte(0), byte(0), byte(0), 0, byte(0), byte(0), byte(0), 11, byte(0), byte(0), byte(0)
  s.ins JUMP, :l_f_lookup_first_sprite_index_return
  s.label :l_f_lookup_first_sprite_index_return
  s.ins RETURN
  s.label :f_lookup_table
  s.ins RANDOM, byte(1), byte(0), byte(0), byte(50)
  s.ins JUMP_TABLE, Register.new(R_temp), ushort(51), :addr_0x734e, :addr_0x7369, :addr_0x7384, :addr_0x739f, :addr_0x73bb, :addr_0x73d6, :addr_0x73f1, :addr_0x740c, :addr_0x7427, :addr_0x7442, :addr_0x745d, :addr_0x7478, :addr_0x7493, :addr_0x74ae, :addr_0x74c9, :addr_0x74e4, :addr_0x74ff, :addr_0x751a, :addr_0x7535, :addr_0x7550, :addr_0x756b, :addr_0x7586, :addr_0x75a1, :addr_0x75bc, :addr_0x75d7, :addr_0x75f2, :addr_0x760d, :addr_0x7628, :addr_0x7643, :addr_0x765e, :addr_0x7679, :addr_0x7694, :addr_0x76af, :addr_0x76ca, :addr_0x76e5, :addr_0x7700, :addr_0x771b, :addr_0x7736, :addr_0x7751, :addr_0x776c, :addr_0x7786, :addr_0x77a1, :addr_0x77bc, :addr_0x77d7, :addr_0x77f2, :addr_0x780c, :addr_0x7827, :addr_0x7842, :addr_0x785d, :addr_0x7878, :addr_0x7893
  s.label :addr_0x734e
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 1160
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 0
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7369
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 63
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7384
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 9
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x739f
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 64
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x73bb
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 55
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x73d6
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 57
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x73f1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 501
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 58
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x740c
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 59
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7427
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 60
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7442
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 62
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x745d
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 56
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7478
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 1
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7493
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 3
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x74ae
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 501
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 4
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x74c9
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 5
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x74e4
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 6
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x74ff
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 8
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x751a
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 2
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7535
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 36
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7550
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 39
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x756b
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 40
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7586
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 94
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 41
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x75a1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 94
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 42
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x75bc
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 44
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x75d7
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 45
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x75f2
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 46
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x760d
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 37
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7628
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 38
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7643
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 18
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x765e
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 21
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7679
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 22
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7694
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 94
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 23
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x76af
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 94
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 24
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x76ca
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 26
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x76e5
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 27
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7700
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 28
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x771b
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 19
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7736
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 20
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7751
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 48
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x776c
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 47
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 49
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7786
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 188
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 50
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x77a1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 51
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x77bc
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 47
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x77d7
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 30
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x77f2
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 47
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 31
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x780c
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 188
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 32
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7827
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 33
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7842
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 29
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x785d
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 768
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 10
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7878
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 2007
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 13
  s.ins JUMP, :l_f_lookup_table_return
  s.label :addr_0x7893
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(13), 15
  s.ins JUMP, :l_f_lookup_table_return
  s.label :l_f_lookup_table_return
  s.ins RETURN
end
