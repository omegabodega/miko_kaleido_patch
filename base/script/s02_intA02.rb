require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_s02_intA02(s)
  s.label :addr_0x1bd58
  s.ins MOV, byte(0), ushort(77), 1
  s.ins JUMP, :addr_0x8394
  s.ins DEBUG, 's02_intA02_01 start.', byte(0)
  s.label :game_02_intA02_01_初めての、ゲーム開始
  s.ins CALL, :addr_0x63b6, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(659), byte(127), line(s, 659)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(660), byte(127), line(s, 660)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(661), byte(127), line(s, 661)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(662), byte(127), line(s, 662)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MSGSET, uint(663), byte(4), line(s, 663)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(664), byte(4), line(s, 664)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(665), byte(4), line(s, 665)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(666), byte(127), line(s, 666)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(667), byte(127), line(s, 667)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(668), byte(127), line(s, 668)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(669), byte(127), line(s, 669)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(670), byte(127), line(s, 670)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 1, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(671), byte(2), line(s, 671)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(672), byte(127), line(s, 672)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(673), byte(127), line(s, 673)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, 1, 106, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(674), byte(3), line(s, 674)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, 1, 15, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(675), byte(1), line(s, 675)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 7, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(676), byte(0), line(s, 676)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(677), byte(127), line(s, 677)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(678), byte(127), line(s, 678)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(679), byte(4), line(s, 679)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(680), byte(4), line(s, 680)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(681), byte(4), line(s, 681)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(682), byte(4), line(s, 682)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x1c745
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x1c745
  s.ins MOV, byte(0), ushort(78), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_02 start.', byte(0)
  s.label :game_02_intA02_02_初めての、ゲーム開始２
  s.ins CALL, :addr_0x63b6, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins CALL, :addr_0x5dba, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(683), byte(127), line(s, 683)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(684), byte(127), line(s, 684)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MSGSET, uint(685), byte(4), line(s, 685)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(686), byte(4), line(s, 686)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [1800, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(687), byte(127), line(s, 687)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(688), byte(127), line(s, 688)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, 1, 15, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(689), byte(1), line(s, 689)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(690), byte(127), line(s, 690)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x1cb24
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x1cb24
  s.ins MOV, byte(0), ushort(79), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_03 start.', byte(0)
  s.label :game_02_intA02_03_初めての、ゲーム開始３
  s.ins CALL, :addr_0x63b6, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(691), byte(127), line(s, 691)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(692), byte(127), line(s, 692)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MSGSET, uint(693), byte(4), line(s, 693)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(694), byte(4), line(s, 694)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(695), byte(127), line(s, 695)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(696), byte(127), line(s, 696)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [1, 500]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x1ce19
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x1ce19
  s.ins MOV, byte(0), ushort(80), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_04 start.', byte(0)
  s.label :game_02_intA02_04_初めての、ゲーム開始４
  s.ins CALL, :addr_0x63b6, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(697), byte(127), line(s, 697)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(698), byte(127), line(s, 698)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MSGSET, uint(699), byte(4), line(s, 699)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(700), byte(4), line(s, 700)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(701), byte(127), line(s, 701)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(702), byte(127), line(s, 702)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(703), byte(127), line(s, 703)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [1, 500]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x1d118
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x1d118
  s.ins MOV, byte(0), ushort(81), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_05 start.', byte(0)
  s.label :game_02_intA02_05_初めての、ゲーム開始５
  s.ins CALL, :addr_0x6405, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins LAYERSELECT, 56, 56
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [604, Register.new(215)]
  s.ins MOV, byte(0), ushort(215), 604
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(704), byte(127), line(s, 704)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(705), byte(127), line(s, 705)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(706), byte(127), line(s, 706)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(707), byte(127), line(s, 707)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(708), byte(127), line(s, 708)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(709), byte(127), line(s, 709)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(710), byte(127), line(s, 710)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(711), byte(127), line(s, 711)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 56, 56
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(215)]
  s.ins MOV, byte(0), ushort(215), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 2000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(712), byte(4), line(s, 712)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(713), byte(4), line(s, 713)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :f_se_fadeout, [1, 500]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x1d53c
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x1d53c
  s.ins MOV, byte(0), ushort(82), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_06 start.', byte(0)
  s.label :game_02_intA02_06_初めての、ゲーム開始６
  s.ins CALL, :addr_0x6405, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(714), byte(127), line(s, 714)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(715), byte(127), line(s, 715)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(716), byte(127), line(s, 716)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(717), byte(127), line(s, 717)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(718), byte(127), line(s, 718)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(719), byte(127), line(s, 719)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(720), byte(127), line(s, 720)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(721), byte(127), line(s, 721)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(722), byte(127), line(s, 722)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(723), byte(127), line(s, 723)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(724), byte(127), line(s, 724)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(725), byte(127), line(s, 725)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(726), byte(127), line(s, 726)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4eba, [3000, 1]
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(727), byte(4), line(s, 727)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(728), byte(4), line(s, 728)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :f_se_fadeout, [1, 500]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x1da60
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x1da60
  s.ins MOV, byte(0), ushort(83), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_07 start.', byte(0)
  s.label :game_02_intA02_07_初めての、ゲーム開始７
  s.ins CALL, :addr_0x6405, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(729), byte(127), line(s, 729)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(730), byte(127), line(s, 730)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(731), byte(127), line(s, 731)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(732), byte(127), line(s, 732)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(733), byte(127), line(s, 733)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(734), byte(127), line(s, 734)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(735), byte(127), line(s, 735)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(736), byte(127), line(s, 736)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(737), byte(127), line(s, 737)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(738), byte(127), line(s, 738)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(739), byte(127), line(s, 739)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(740), byte(127), line(s, 740)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(741), byte(127), line(s, 741)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(742), byte(127), line(s, 742)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(743), byte(127), line(s, 743)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(744), byte(127), line(s, 744)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(745), byte(127), line(s, 745)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(746), byte(127), line(s, 746)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4eba, [3000, 1]
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(747), byte(4), line(s, 747)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(748), byte(4), line(s, 748)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :f_se_fadeout, [1, 500]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x1e0ad
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x1e0ad
  s.ins MOV, byte(0), ushort(84), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_08 start.', byte(0)
  s.label :game_02_intA02_08_初めての、ゲーム開始８
  s.ins CALL, :addr_0x6405, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(749), byte(127), line(s, 749)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(750), byte(127), line(s, 750)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(751), byte(127), line(s, 751)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(752), byte(127), line(s, 752)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(753), byte(127), line(s, 753)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(754), byte(127), line(s, 754)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(755), byte(127), line(s, 755)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(756), byte(127), line(s, 756)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(757), byte(127), line(s, 757)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(758), byte(127), line(s, 758)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(759), byte(127), line(s, 759)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(760), byte(127), line(s, 760)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(761), byte(127), line(s, 761)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(762), byte(127), line(s, 762)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(763), byte(127), line(s, 763)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(764), byte(127), line(s, 764)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(765), byte(127), line(s, 765)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(766), byte(127), line(s, 766)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(767), byte(127), line(s, 767)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(768), byte(127), line(s, 768)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(769), byte(127), line(s, 769)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(770), byte(127), line(s, 770)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(771), byte(127), line(s, 771)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(772), byte(127), line(s, 772)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(773), byte(127), line(s, 773)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(774), byte(127), line(s, 774)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [587, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 587
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERCTRL, 20, 44, byte(5), 6, 12544
  s.ins LAYERCTRL, 20, 46, byte(1), -999
  s.ins LAYERCTRL, 20, 45, byte(1), 14400
  s.ins LAYERCTRL, 20, 47, byte(0)
  s.ins MSGSET, uint(775), byte(127), line(s, 775)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(776), byte(127), line(s, 776)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(777), byte(127), line(s, 777)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(778), byte(127), line(s, 778)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 3000
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :addr_0x4eba, [3000, 1]
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(779), byte(4), line(s, 779)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(780), byte(4), line(s, 780)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :f_se_fadeout, [1, 500]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x1eb02
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x1eb02
  s.ins MOV, byte(0), ushort(85), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_09 start.', byte(0)
  s.label :game_02_intA02_09_初めての、ゲーム開始９
  s.ins CALL, :addr_0x6405, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :f_bgm_play, [6, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [515, Register.new(234), 1400]
  s.ins MOV, byte(0), ushort(234), 515
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [36, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :f_se_play, [3, 65, 100, 1000, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 2, 321, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(781), byte(0), line(s, 781)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, 2, 321, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(782), byte(2), line(s, 782)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x574d, [34, :null, 800, 0, 100, 1]
  s.ins MSGSET, uint(783), byte(127), line(s, 783)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(784), byte(127), line(s, 784)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins MSGSET, uint(785), byte(127), line(s, 785)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(786), byte(127), line(s, 786)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(787), byte(127), line(s, 787)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [80, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 2, 319, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-249, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 2, 314, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-699, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 317, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 317, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [700, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(788), byte(1), line(s, 788)
  s.ins MSGSYNC, byte(0), 2581
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 321, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 315, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(789), byte(3), line(s, 789)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(790), byte(4), line(s, 790)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(791), byte(4), line(s, 791)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 300, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 300, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 300, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 303, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(792), byte(0), line(s, 792)
  s.ins MSGSYNC, byte(0), 1792
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 300, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(793), byte(4), line(s, 793)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(794), byte(4), line(s, 794)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 301, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(795), byte(2), line(s, 795)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 308, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(796), byte(1), line(s, 796)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 315, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(797), byte(3), line(s, 797)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 309, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(798), byte(0), line(s, 798)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(799), byte(4), line(s, 799)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 314, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 312, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(800), byte(127), line(s, 800)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(801), byte(4), line(s, 801)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 300, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 1600, 1, 800]
  s.ins MSGSET, uint(802), byte(2), line(s, 802)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 303, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(803), byte(2), line(s, 803)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 317, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(804), byte(1), line(s, 804)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(805), byte(4), line(s, 805)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(806), byte(4), line(s, 806)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [36, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(807), byte(127), line(s, 807)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(808), byte(127), line(s, 808)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(809), byte(127), line(s, 809)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(810), byte(127), line(s, 810)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(811), byte(127), line(s, 811)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(812), byte(127), line(s, 812)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(813), byte(127), line(s, 813)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(814), byte(127), line(s, 814)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [34, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [80, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 300, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 312, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 308, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 301, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(815), byte(0), line(s, 815)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(816), byte(4), line(s, 816)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(817), byte(4), line(s, 817)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 317, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(818), byte(1), line(s, 818)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(819), byte(4), line(s, 819)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(820), byte(4), line(s, 820)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 308, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 317, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 317, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(821), byte(0), line(s, 821)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 315, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(822), byte(3), line(s, 822)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 314, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(823), byte(3), line(s, 823)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(824), byte(4), line(s, 824)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 318, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(825), byte(2), line(s, 825)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(826), byte(4), line(s, 826)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(827), byte(4), line(s, 827)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(828), byte(4), line(s, 828)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(829), byte(4), line(s, 829)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 300, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins MSGSET, uint(830), byte(0), line(s, 830)
  s.ins MSGSYNC, byte(0), 1514
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 313, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(831), byte(4), line(s, 831)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(234)]
  s.ins MOV, byte(0), ushort(234), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 2000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(832), byte(4), line(s, 832)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(833), byte(4), line(s, 833)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :f_se_fadeout, [1, 500]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x202cf
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x202cf
  s.ins MOV, byte(0), ushort(86), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_10 start.', byte(0)
  s.label :game_02_intA02_10_初めての、ゲーム開始１０
  s.ins CALL, :addr_0x6405, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [7, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 16, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(834), byte(1), line(s, 834)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 223, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [2, 1]
  s.ins MSGSET, uint(835), byte(2), line(s, 835)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(836), byte(1), line(s, 836)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 117, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [3, 1]
  s.ins MSGSET, uint(837), byte(3), line(s, 837)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(838), byte(2), line(s, 838)
  s.ins MSGSYNC, byte(0), 1184
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(839), byte(2), line(s, 839)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 10, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(840), byte(0), line(s, 840)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(841), byte(0), line(s, 841)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(842), byte(0), line(s, 842)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(843), byte(0), line(s, 843)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(844), byte(0), line(s, 844)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(845), byte(1), line(s, 845)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(846), byte(0), line(s, 846)
  s.ins MSGSYNC, byte(0), 3712
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins MSGSET, uint(847), byte(127), line(s, 847)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(848), byte(127), line(s, 848)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(849), byte(127), line(s, 849)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 120, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(850), byte(1), line(s, 850)
  s.ins MSGSYNC, byte(0), 3061
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(851), byte(0), line(s, 851)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(852), byte(3), line(s, 852)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(853), byte(2), line(s, 853)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(854), byte(2), line(s, 854)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(855), byte(1), line(s, 855)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(856), byte(3), line(s, 856)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 2, 400]
  s.ins MSGSET, uint(857), byte(1), line(s, 857)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [451]
  s.ins CALL, :addr_0x5906, []
  s.ins MSGSET, uint(858), byte(2), line(s, 858)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(859), byte(1), line(s, 859)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(860), byte(0), line(s, 860)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(861), byte(0), line(s, 861)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(862), byte(0), line(s, 862)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 31, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(863), byte(2), line(s, 863)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(864), byte(3), line(s, 864)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(865), byte(1), line(s, 865)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(866), byte(0), line(s, 866)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(867), byte(0), line(s, 867)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(868), byte(0), line(s, 868)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins MSGSET, uint(869), byte(0), line(s, 869)
  s.ins MSGSYNC, byte(0), 309
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [568, Register.new(211)]
  s.ins MOV, byte(0), ushort(211), 568
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins CALL, :addr_0x5265, []
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 52, 52
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 800
  s.ins MOV, byte(0), ushort(13), 18
  s.ins MOV, byte(0), ushort(14), 376
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(211)]
  s.ins MOV, byte(0), ushort(211), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 1, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_flag), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(870), byte(1), line(s, 870)
  s.ins MSGSYNC, byte(0), 2122
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [15, 15, 200]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(871), byte(2), line(s, 871)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(872), byte(0), line(s, 872)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 9, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(873), byte(3), line(s, 873)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x61c1, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [515, Register.new(234), 1400]
  s.ins MOV, byte(0), ushort(234), 515
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [36, 0]
  s.ins CALL, :addr_0x6211, []
  s.ins MSGSET, uint(874), byte(127), line(s, 874)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(875), byte(127), line(s, 875)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(876), byte(127), line(s, 876)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(877), byte(127), line(s, 877)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(878), byte(127), line(s, 878)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(879), byte(127), line(s, 879)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(880), byte(127), line(s, 880)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x61c1, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(234)]
  s.ins MOV, byte(0), ushort(234), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [7, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 16, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x6211, []
  s.ins MSGSET, uint(881), byte(1), line(s, 881)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(882), byte(3), line(s, 882)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(883), byte(2), line(s, 883)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(884), byte(3), line(s, 884)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 209, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(885), byte(2), line(s, 885)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(886), byte(1), line(s, 886)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(887), byte(0), line(s, 887)
  s.ins MSGSYNC, byte(0), 2730
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins CALL, :addr_0x61c1, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [515, Register.new(234), 1400]
  s.ins MOV, byte(0), ushort(234), 515
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [36, :null]
  s.ins CALL, :addr_0x6211, []
  s.ins MSGSET, uint(888), byte(127), line(s, 888)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(889), byte(127), line(s, 889)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(890), byte(127), line(s, 890)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(891), byte(127), line(s, 891)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x61c1, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(234)]
  s.ins MOV, byte(0), ushort(234), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [7, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 106, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x6211, []
  s.ins MSGSET, uint(892), byte(2), line(s, 892)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(893), byte(3), line(s, 893)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(894), byte(1), line(s, 894)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 18, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(895), byte(0), line(s, 895)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(896), byte(0), line(s, 896)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 30, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(897), byte(2), line(s, 897)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(898), byte(0), line(s, 898)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x61c1, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [515, Register.new(234), 1400]
  s.ins MOV, byte(0), ushort(234), 515
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [36, :null]
  s.ins CALL, :addr_0x6211, []
  s.ins MSGSET, uint(899), byte(127), line(s, 899)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(900), byte(127), line(s, 900)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(901), byte(127), line(s, 901)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(902), byte(127), line(s, 902)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x61c1, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(234)]
  s.ins MOV, byte(0), ushort(234), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [7, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins CALL, :addr_0x6211, []
  s.ins MSGSET, uint(903), byte(0), line(s, 903)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 127, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(904), byte(1), line(s, 904)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 111, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(905), byte(3), line(s, 905)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(906), byte(0), line(s, 906)
  s.ins MSGSYNC, byte(0), 2794
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(907), byte(0), line(s, 907)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(908), byte(0), line(s, 908)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x61c1, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [515, Register.new(234), 1400]
  s.ins MOV, byte(0), ushort(234), 515
  s.ins CALL, :addr_0x5670, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [36, 2]
  s.ins CALL, :addr_0x6211, []
  s.ins MSGSET, uint(909), byte(127), line(s, 909)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(910), byte(127), line(s, 910)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x61c1, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(234)]
  s.ins MOV, byte(0), ushort(234), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [7, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 108, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins CALL, :addr_0x6211, []
  s.ins MSGSET, uint(911), byte(0), line(s, 911)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 33, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(912), byte(2), line(s, 912)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(913), byte(1), line(s, 913)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(914), byte(0), line(s, 914)
  s.ins MSGSYNC, byte(0), 2965
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 30, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(915), byte(1), line(s, 915)
  s.ins MSGSYNC, byte(0), 2624
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(916), byte(3), line(s, 916)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 0, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(917), byte(3), line(s, 917)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(918), byte(0), line(s, 918)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(919), byte(0), line(s, 919)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 113, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(920), byte(2), line(s, 920)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(921), byte(3), line(s, 921)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(922), byte(0), line(s, 922)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 19, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [710, -449]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(923), byte(1), line(s, 923)
  s.ins MSGSYNC, byte(0), 3018
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(924), byte(1), line(s, 924)
  s.ins MSGSYNC, byte(0), 1962
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(925), byte(0), line(s, 925)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 10, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(926), byte(0), line(s, 926)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(927), byte(1), line(s, 927)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(928), byte(0), line(s, 928)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 128, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(929), byte(1), line(s, 929)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 212, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(930), byte(2), line(s, 930)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(931), byte(1), line(s, 931)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 19, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(932), byte(1), line(s, 932)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(933), byte(2), line(s, 933)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(934), byte(1), line(s, 934)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(935), byte(1), line(s, 935)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(936), byte(3), line(s, 936)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(937), byte(1), line(s, 937)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(938), byte(1), line(s, 938)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(939), byte(1), line(s, 939)
  s.ins MSGSYNC, byte(0), 9237
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 213, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(940), byte(2), line(s, 940)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(941), byte(2), line(s, 941)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(942), byte(3), line(s, 942)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(943), byte(1), line(s, 943)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 17, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(944), byte(0), line(s, 944)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(945), byte(2), line(s, 945)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [451]
  s.ins CALL, :addr_0x5670, []
  s.ins CALL, :addr_0x5906, []
  s.ins MSGSET, uint(946), byte(4), line(s, 946)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(947), byte(3), line(s, 947)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(948), byte(2), line(s, 948)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(949), byte(4), line(s, 949)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [3, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 117, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 100, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-699, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [3, 1]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [700, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [2, 1]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-249, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [90, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [0, 1]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(950), byte(0), line(s, 950)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(951), byte(3), line(s, 951)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 210, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(952), byte(2), line(s, 952)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(953), byte(1), line(s, 953)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [451]
  s.ins CALL, :addr_0x5906, []
  s.ins MSGSET, uint(954), byte(1), line(s, 954)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(955), byte(4), line(s, 955)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 65, :null, :null, :null, :null, 2]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 2000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(956), byte(4), line(s, 956)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(957), byte(4), line(s, 957)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(958), byte(4), line(s, 958)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(959), byte(4), line(s, 959)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(960), byte(4), line(s, 960)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 1, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(961), byte(0), line(s, 961)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, 1, 29, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(962), byte(3), line(s, 962)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [1, 500]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x244c2
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x244c2
  s.ins MOV, byte(0), ushort(87), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_11 start.', byte(0)
  s.label :game_02_intA02_11_初めての、ゲーム開始１１
  s.ins CALL, :addr_0x6405, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(963), byte(127), line(s, 963)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(964), byte(127), line(s, 964)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(965), byte(127), line(s, 965)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(966), byte(127), line(s, 966)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(967), byte(127), line(s, 967)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(968), byte(127), line(s, 968)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(969), byte(127), line(s, 969)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(970), byte(127), line(s, 970)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(971), byte(127), line(s, 971)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(972), byte(127), line(s, 972)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(973), byte(127), line(s, 973)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(974), byte(127), line(s, 974)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x58f7, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [532, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 532
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 11
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MSGSET, uint(975), byte(4), line(s, 975)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(976), byte(4), line(s, 976)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(977), byte(4), line(s, 977)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(978), byte(4), line(s, 978)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(979), byte(4), line(s, 979)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(980), byte(4), line(s, 980)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(981), byte(4), line(s, 981)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(982), byte(4), line(s, 982)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(983), byte(4), line(s, 983)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(984), byte(4), line(s, 984)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(985), byte(127), line(s, 985)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(986), byte(127), line(s, 986)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(987), byte(127), line(s, 987)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [6, 100, 1000, 1]
  s.ins MSGSET, uint(988), byte(4), line(s, 988)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(989), byte(4), line(s, 989)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(990), byte(4), line(s, 990)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(991), byte(4), line(s, 991)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(992), byte(4), line(s, 992)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(993), byte(4), line(s, 993)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(994), byte(4), line(s, 994)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(995), byte(4), line(s, 995)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(996), byte(4), line(s, 996)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(997), byte(4), line(s, 997)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(998), byte(4), line(s, 998)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [538, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 538
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(999), byte(127), line(s, 999)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1000), byte(4), line(s, 1000)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1001), byte(4), line(s, 1001)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1002), byte(4), line(s, 1002)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1003), byte(4), line(s, 1003)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1004), byte(4), line(s, 1004)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1005), byte(4), line(s, 1005)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1006), byte(4), line(s, 1006)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1007), byte(4), line(s, 1007)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1008), byte(4), line(s, 1008)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1009), byte(4), line(s, 1009)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1010), byte(4), line(s, 1010)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1011), byte(4), line(s, 1011)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [Register.new(179), Register.new(179)]
  s.ins MOV, byte(0), ushort(179), Register.new(179)
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MSGSET, uint(1012), byte(4), line(s, 1012)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1013), byte(4), line(s, 1013)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 70, 100, 2000, 0]
  s.ins MSGSET, uint(1014), byte(127), line(s, 1014)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1015), byte(127), line(s, 1015)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1016), byte(4), line(s, 1016)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1017), byte(4), line(s, 1017)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 0]
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [Register.new(179), Register.new(179)]
  s.ins MOV, byte(0), ushort(179), Register.new(179)
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MSGSET, uint(1018), byte(4), line(s, 1018)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1019), byte(4), line(s, 1019)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1020), byte(4), line(s, 1020)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1021), byte(4), line(s, 1021)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1022), byte(4), line(s, 1022)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1023), byte(4), line(s, 1023)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1024), byte(4), line(s, 1024)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1025), byte(4), line(s, 1025)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1026), byte(4), line(s, 1026)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [Register.new(179), Register.new(179)]
  s.ins MOV, byte(0), ushort(179), Register.new(179)
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MSGSET, uint(1027), byte(4), line(s, 1027)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1028), byte(4), line(s, 1028)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1029), byte(4), line(s, 1029)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1030), byte(4), line(s, 1030)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1031), byte(4), line(s, 1031)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1032), byte(4), line(s, 1032)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1033), byte(4), line(s, 1033)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1034), byte(4), line(s, 1034)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1035), byte(4), line(s, 1035)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1036), byte(4), line(s, 1036)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1037), byte(4), line(s, 1037)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1038), byte(4), line(s, 1038)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1039), byte(4), line(s, 1039)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [10, 100, 1000, 1]
  s.ins LAYERSELECT, 84, 84
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [499, Register.new(243), 1400]
  s.ins MOV, byte(0), ushort(243), 499
  s.ins CALL, :addr_0x604e, [1300]
  s.ins MSGSET, uint(1040), byte(127), line(s, 1040)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1041), byte(127), line(s, 1041)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1042), byte(127), line(s, 1042)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1043), byte(127), line(s, 1043)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1044), byte(127), line(s, 1044)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1045), byte(127), line(s, 1045)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1046), byte(127), line(s, 1046)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1047), byte(127), line(s, 1047)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins LAYERSELECT, 84, 84
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(243)]
  s.ins MOV, byte(0), ushort(243), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :addr_0x60a7, []
  s.ins MSGSET, uint(1048), byte(4), line(s, 1048)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1049), byte(4), line(s, 1049)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1050), byte(4), line(s, 1050)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1051), byte(4), line(s, 1051)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [Register.new(179), Register.new(179)]
  s.ins MOV, byte(0), ushort(179), Register.new(179)
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MSGSET, uint(1052), byte(4), line(s, 1052)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1053), byte(4), line(s, 1053)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1054), byte(4), line(s, 1054)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1055), byte(4), line(s, 1055)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [Register.new(179), Register.new(179)]
  s.ins MOV, byte(0), ushort(179), Register.new(179)
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MSGSET, uint(1056), byte(4), line(s, 1056)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1057), byte(4), line(s, 1057)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1058), byte(4), line(s, 1058)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1059), byte(4), line(s, 1059)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 84, 84
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [499, Register.new(243), 1400]
  s.ins MOV, byte(0), ushort(243), 499
  s.ins CALL, :addr_0x604e, [1300]
  s.ins MSGSET, uint(1060), byte(127), line(s, 1060)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1061), byte(127), line(s, 1061)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1062), byte(127), line(s, 1062)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1063), byte(127), line(s, 1063)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1064), byte(127), line(s, 1064)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1065), byte(127), line(s, 1065)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1066), byte(127), line(s, 1066)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x60a7, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 84, 84
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(243)]
  s.ins MOV, byte(0), ushort(243), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1067), byte(4), line(s, 1067)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1068), byte(4), line(s, 1068)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1069), byte(4), line(s, 1069)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1070), byte(4), line(s, 1070)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1071), byte(4), line(s, 1071)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1072), byte(4), line(s, 1072)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1073), byte(4), line(s, 1073)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1074), byte(4), line(s, 1074)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins MSGSET, uint(1075), byte(4), line(s, 1075)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [5, 100, 0, 1]
  s.ins MSGSET, uint(1076), byte(4), line(s, 1076)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1077), byte(4), line(s, 1077)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [Register.new(179), Register.new(179)]
  s.ins MOV, byte(0), ushort(179), Register.new(179)
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins MSGSET, uint(1078), byte(127), line(s, 1078)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1079), byte(127), line(s, 1079)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1080), byte(4), line(s, 1080)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1081), byte(4), line(s, 1081)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1082), byte(4), line(s, 1082)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1083), byte(4), line(s, 1083)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1084), byte(4), line(s, 1084)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1085), byte(4), line(s, 1085)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1086), byte(127), line(s, 1086)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1087), byte(127), line(s, 1087)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1088), byte(127), line(s, 1088)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1089), byte(4), line(s, 1089)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1090), byte(4), line(s, 1090)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1091), byte(4), line(s, 1091)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1092), byte(4), line(s, 1092)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1093), byte(4), line(s, 1093)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1094), byte(4), line(s, 1094)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1095), byte(4), line(s, 1095)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1096), byte(4), line(s, 1096)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1097), byte(4), line(s, 1097)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1098), byte(4), line(s, 1098)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1099), byte(4), line(s, 1099)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1100), byte(4), line(s, 1100)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1101), byte(4), line(s, 1101)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1102), byte(4), line(s, 1102)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1103), byte(4), line(s, 1103)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1104), byte(4), line(s, 1104)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1105), byte(4), line(s, 1105)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1106), byte(4), line(s, 1106)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [532, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 532
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MSGSET, uint(1107), byte(4), line(s, 1107)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1108), byte(4), line(s, 1108)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1109), byte(127), line(s, 1109)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1110), byte(127), line(s, 1110)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [533]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(1111), byte(4), line(s, 1111)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1112), byte(4), line(s, 1112)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1113), byte(4), line(s, 1113)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1114), byte(4), line(s, 1114)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1115), byte(4), line(s, 1115)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1116), byte(4), line(s, 1116)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1117), byte(4), line(s, 1117)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1118), byte(127), line(s, 1118)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1119), byte(127), line(s, 1119)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1120), byte(127), line(s, 1120)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1121), byte(127), line(s, 1121)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1122), byte(127), line(s, 1122)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1123), byte(127), line(s, 1123)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1124), byte(4), line(s, 1124)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1125), byte(4), line(s, 1125)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1126), byte(4), line(s, 1126)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1127), byte(4), line(s, 1127)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1128), byte(127), line(s, 1128)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1129), byte(127), line(s, 1129)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 77, 100, 0, 0]
  s.ins CALL, :addr_0x5914, [463, 0, 0, 100]
  s.ins MSGSET, uint(1130), byte(127), line(s, 1130)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1131), byte(127), line(s, 1131)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1132), byte(4), line(s, 1132)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1133), byte(4), line(s, 1133)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1134), byte(4), line(s, 1134)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1135), byte(4), line(s, 1135)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1136), byte(4), line(s, 1136)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1137), byte(4), line(s, 1137)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1138), byte(4), line(s, 1138)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1139), byte(4), line(s, 1139)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 86, 86
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [488, Register.new(245), 1400]
  s.ins MOV, byte(0), ushort(245), 488
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x4ff1, [539]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 200
  s.ins MOV, byte(0), ushort(13), 61
  s.ins MOV, byte(0), ushort(14), 501
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins LAYERSELECT, 86, 86
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 200
  s.ins MOV, byte(0), ushort(13), 61
  s.ins MOV, byte(0), ushort(14), 501
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(245)]
  s.ins MOV, byte(0), ushort(245), -1
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(1140), byte(127), line(s, 1140)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1141), byte(4), line(s, 1141)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1142), byte(4), line(s, 1142)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1143), byte(4), line(s, 1143)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 2000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1144), byte(4), line(s, 1144)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1145), byte(4), line(s, 1145)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1146), byte(4), line(s, 1146)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1147), byte(4), line(s, 1147)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :f_se_fadeout, [1, 500]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x27a3e
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x27a3e
  s.ins MOV, byte(0), ushort(88), 1
  s.ins JUMP, :addr_0x8406
  s.ins DEBUG, 's02_intA02_12 start.', byte(0)
  s.label :game_02_intA02_12_初めての、ゲーム開始１２
  s.ins CALL, :addr_0x6405, []
  s.ins CALL, :f_reset, []
  s.ins CALL, :addr_0x6238, [36, 0]
  s.ins CALL, :f_bgm_play, [6, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1148), byte(127), line(s, 1148)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1149), byte(127), line(s, 1149)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1150), byte(127), line(s, 1150)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1151), byte(127), line(s, 1151)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1152), byte(127), line(s, 1152)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1153), byte(127), line(s, 1153)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1154), byte(127), line(s, 1154)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1155), byte(4), line(s, 1155)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, 2, 302, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1156), byte(0), line(s, 1156)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 2, 300, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1157), byte(0), line(s, 1157)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(1158), byte(127), line(s, 1158)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1159), byte(127), line(s, 1159)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1160), byte(127), line(s, 1160)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1161), byte(127), line(s, 1161)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1162), byte(127), line(s, 1162)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1163), byte(127), line(s, 1163)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1164), byte(127), line(s, 1164)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1165), byte(127), line(s, 1165)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1166), byte(127), line(s, 1166)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1167), byte(127), line(s, 1167)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1168), byte(127), line(s, 1168)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1169), byte(127), line(s, 1169)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1170), byte(127), line(s, 1170)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1171), byte(127), line(s, 1171)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1172), byte(127), line(s, 1172)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1173), byte(127), line(s, 1173)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1174), byte(127), line(s, 1174)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1175), byte(127), line(s, 1175)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1176), byte(127), line(s, 1176)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1177), byte(127), line(s, 1177)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1178), byte(127), line(s, 1178)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1179), byte(127), line(s, 1179)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1180), byte(127), line(s, 1180)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1181), byte(127), line(s, 1181)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1182), byte(127), line(s, 1182)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x574d, [33, :null, 0, 0, 100, 1]
  s.ins MSGSET, uint(1183), byte(127), line(s, 1183)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1184), byte(127), line(s, 1184)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1185), byte(4), line(s, 1185)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1186), byte(127), line(s, 1186)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1187), byte(127), line(s, 1187)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1188), byte(127), line(s, 1188)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1189), byte(127), line(s, 1189)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1190), byte(127), line(s, 1190)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1191), byte(127), line(s, 1191)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 85, 100, 0, 0]
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins CALL, :addr_0x574d, [36, 5, 0, 0, 100, 1]
  s.ins MSGSET, uint(1192), byte(127), line(s, 1192)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1193), byte(127), line(s, 1193)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1194), byte(127), line(s, 1194)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1195), byte(127), line(s, 1195)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1196), byte(127), line(s, 1196)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1197), byte(127), line(s, 1197)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1198), byte(127), line(s, 1198)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1199), byte(127), line(s, 1199)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1200), byte(127), line(s, 1200)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :f_se_play, [4, 1, 100, 1000, 0]
  s.ins CALL, :addr_0x574d, [33, 6, 0, 0, 100, 1]
  s.ins MSGSET, uint(1201), byte(127), line(s, 1201)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1202), byte(127), line(s, 1202)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1203), byte(127), line(s, 1203)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1204), byte(127), line(s, 1204)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1205), byte(127), line(s, 1205)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1206), byte(127), line(s, 1206)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1207), byte(127), line(s, 1207)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1208), byte(127), line(s, 1208)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1209), byte(127), line(s, 1209)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1210), byte(127), line(s, 1210)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1211), byte(127), line(s, 1211)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1212), byte(127), line(s, 1212)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1213), byte(127), line(s, 1213)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1214), byte(127), line(s, 1214)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1215), byte(127), line(s, 1215)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1216), byte(127), line(s, 1216)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1217), byte(127), line(s, 1217)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1218), byte(127), line(s, 1218)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1219), byte(127), line(s, 1219)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1220), byte(127), line(s, 1220)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [4, 1000]
  s.ins CALL, :addr_0x574d, [36, 2, 0, 0, 100, 1]
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins MSGSET, uint(1221), byte(127), line(s, 1221)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1222), byte(127), line(s, 1222)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1223), byte(127), line(s, 1223)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1224), byte(127), line(s, 1224)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1225), byte(127), line(s, 1225)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1226), byte(127), line(s, 1226)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1227), byte(127), line(s, 1227)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1228), byte(127), line(s, 1228)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1229), byte(127), line(s, 1229)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1230), byte(127), line(s, 1230)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1231), byte(127), line(s, 1231)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 17
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MSGSET, uint(1232), byte(127), line(s, 1232)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1233), byte(127), line(s, 1233)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1234), byte(127), line(s, 1234)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1235), byte(127), line(s, 1235)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1236), byte(127), line(s, 1236)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [5, 100, 0, 1]
  s.ins MSGSET, uint(1237), byte(127), line(s, 1237)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1238), byte(127), line(s, 1238)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1239), byte(127), line(s, 1239)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1240), byte(127), line(s, 1240)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1241), byte(127), line(s, 1241)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 75, 75
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(234)]
  s.ins MOV, byte(0), ushort(234), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [1, :null]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins MOV, byte(0), ushort(13), 11
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MSGSET, uint(1242), byte(127), line(s, 1242)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1243), byte(127), line(s, 1243)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1244), byte(127), line(s, 1244)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1245), byte(127), line(s, 1245)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1246), byte(127), line(s, 1246)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1247), byte(127), line(s, 1247)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4eba, [3000, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 2000
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x5265, []
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :addr_0x5dba, []
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1248), byte(4), line(s, 1248)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1249), byte(4), line(s, 1249)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1250), byte(4), line(s, 1250)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1251), byte(4), line(s, 1251)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [452, 0, 0, 100]
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1252), byte(4), line(s, 1252)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1253), byte(4), line(s, 1253)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(1254), byte(4), line(s, 1254)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [453, 0, 0, 100]
  s.ins MSGSET, uint(1255), byte(4), line(s, 1255)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 50, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [8, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :f_se_play, [1, 63, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [9, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [3200, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [2000]
  s.ins CALL, :f_se_fadeout, [1, 500]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x292c7
  s.ins CALL, :f_reset, []
  s.ins RETSUB
end
