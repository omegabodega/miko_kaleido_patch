require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_s06_intB(s)
  s.label :addr_0x8e6ef
  s.ins MOV, byte(0), ushort(125), 1
  s.ins JUMP, :addr_0x874e
  s.ins DEBUG, 's06_intB01_01 start.', byte(0)
  s.label :game_06_intB01_01_初めての風華イベント時
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x650b, [419]
  s.ins CALL, :f_se_play, [4, 84, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4716), byte(0), line(s, 4716)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4717), byte(3), line(s, 4717)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4718), byte(0), line(s, 4718)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [420, 0, 0, 100]
  s.ins MSGSET, uint(4719), byte(0), line(s, 4719)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4720), byte(3), line(s, 4720)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4721), byte(0), line(s, 4721)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4722), byte(3), line(s, 4722)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4723), byte(3), line(s, 4723)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4724), byte(0), line(s, 4724)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4725), byte(3), line(s, 4725)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4726), byte(3), line(s, 4726)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4727), byte(0), line(s, 4727)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4728), byte(0), line(s, 4728)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4729), byte(0), line(s, 4729)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4730), byte(3), line(s, 4730)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4731), byte(3), line(s, 4731)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [419, 0, 0, 100]
  s.ins MSGSET, uint(4732), byte(0), line(s, 4732)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5914, [421, 0, 0, 100]
  s.ins MSGSET, uint(4733), byte(0), line(s, 4733)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [4, 0]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x8eba5
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x8eba5
  s.ins MOV, byte(0), ushort(126), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB01_02 start.', byte(0)
  s.label :game_06_intB01_02_初めての火凛イベント時
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x64cf, [31, 0]
  s.ins CALL, :f_bgm_play, [8, 100, 0, 1]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 0, 104, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 0, 101, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4734), byte(0), line(s, 4734)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 104, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4735), byte(1), line(s, 4735)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4736), byte(1), line(s, 4736)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 48, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 68, 68
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [484, Register.new(227), 1900]
  s.ins MOV, byte(0), ushort(227), 484
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4737), byte(1), line(s, 4737)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4738), byte(0), line(s, 4738)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 68, 68
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(227)]
  s.ins MOV, byte(0), ushort(227), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4739), byte(1), line(s, 4739)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4740), byte(1), line(s, 4740)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4741), byte(0), line(s, 4741)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 122, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4742), byte(1), line(s, 4742)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4743), byte(0), line(s, 4743)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4744), byte(0), line(s, 4744)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4745), byte(1), line(s, 4745)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 122, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4746), byte(0), line(s, 4746)
  s.ins MSGSYNC, byte(0), 1952
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 100, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4747), byte(0), line(s, 4747)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 104, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4748), byte(1), line(s, 4748)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4749), byte(0), line(s, 4749)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4750), byte(0), line(s, 4750)
  s.ins MSGSYNC, byte(0), 3413
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGSET, uint(4751), byte(1), line(s, 4751)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 119, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4752), byte(1), line(s, 4752)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4753), byte(1), line(s, 4753)
  s.ins MSGSYNC, byte(0), 4245
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4754), byte(0), line(s, 4754)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 21, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [-20, 800, 1, 400]
  s.ins MSGSET, uint(4755), byte(1), line(s, 4755)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 123, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4756), byte(0), line(s, 4756)
  s.ins MSGSYNC, byte(0), 3200
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 120, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4757), byte(1), line(s, 4757)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 105, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4758), byte(0), line(s, 4758)
  s.ins MSGSYNC, byte(0), 981
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [12, 500, 5, 250]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x8f80a
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x8f80a
  s.ins MOV, byte(0), ushort(127), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB01_03 start.', byte(0)
  s.label :game_06_intB01_03_初めての水無イベント時
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x64cf, [30, 0]
  s.ins CALL, :f_bgm_play, [6, 100, 0, 1]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 0, 103, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 0, 202, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4759), byte(1), line(s, 4759)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4760), byte(2), line(s, 4760)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4761), byte(2), line(s, 4761)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4762), byte(1), line(s, 4762)
  s.ins MSGSYNC, byte(0), 2005
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4763), byte(2), line(s, 4763)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4764), byte(1), line(s, 4764)
  s.ins MSGSYNC, byte(0), 3125
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4765), byte(2), line(s, 4765)
  s.ins MSGSYNC, byte(0), 1568
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [12, 35, 70]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4766), byte(1), line(s, 4766)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 8, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4767), byte(2), line(s, 4767)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 12, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4768), byte(2), line(s, 4768)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4769), byte(2), line(s, 4769)
  s.ins MSGSYNC, byte(0), 5301
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4770), byte(1), line(s, 4770)
  s.ins MSGSYNC, byte(0), 1648
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 119, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 123, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4771), byte(2), line(s, 4771)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4772), byte(2), line(s, 4772)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 8, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4773), byte(2), line(s, 4773)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4774), byte(1), line(s, 4774)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d78, []
  s.ins MSGSET, uint(4775), byte(2), line(s, 4775)
  s.ins MSGSYNC, byte(0), 2496
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 222, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 5898
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 217, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 221, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4776), byte(2), line(s, 4776)
  s.ins MSGSYNC, byte(0), 6858
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGSET, uint(4777), byte(2), line(s, 4777)
  s.ins MSGSYNC, byte(0), 645
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [10, 800, 1, 400]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4778), byte(1), line(s, 4778)
  s.ins MSGSYNC, byte(0), 2762
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 7381
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 104, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 223, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4779), byte(2), line(s, 4779)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 211, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4780), byte(2), line(s, 4780)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4781), byte(1), line(s, 4781)
  s.ins MSGSYNC, byte(0), 1429
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 4949
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 1
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 400
  s.ins MOV, byte(0), ushort(65), 200
  s.ins MOV, byte(0), ushort(66), 0
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x4df4, [0]
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 27, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4782), byte(2), line(s, 4782)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x90642
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x90642
  s.ins MOV, byte(0), ushort(128), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB01_04 start.', byte(0)
  s.label :game_06_intB01_04_初めての土麗美イベント時
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x64cf, [31, 0]
  s.ins CALL, :f_se_play, [4, 84, 100, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 0, 101, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 0, 207, 0, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4783), byte(2), line(s, 4783)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 111, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4784), byte(3), line(s, 4784)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4785), byte(2), line(s, 4785)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4786), byte(3), line(s, 4786)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 209, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGSET, uint(4787), byte(2), line(s, 4787)
  s.ins MSGSYNC, byte(0), 2016
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 201, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4788), byte(3), line(s, 4788)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4789), byte(2), line(s, 4789)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4790), byte(3), line(s, 4790)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 226, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4791), byte(2), line(s, 4791)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4792), byte(3), line(s, 4792)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 200, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4793), byte(2), line(s, 4793)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 211, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4794), byte(2), line(s, 4794)
  s.ins MSGSYNC, byte(0), 2736
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4795), byte(3), line(s, 4795)
  s.ins MSGSYNC, byte(0), 3056
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4796), byte(2), line(s, 4796)
  s.ins MSGSYNC, byte(0), 2048
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 101, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4797), byte(3), line(s, 4797)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4798), byte(3), line(s, 4798)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 211, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4799), byte(2), line(s, 4799)
  s.ins MSGSYNC, byte(0), 4085
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 228, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4800), byte(2), line(s, 4800)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4801), byte(3), line(s, 4801)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 45, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x60c6, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 103, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4802), byte(3), line(s, 4802)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 203, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4803), byte(2), line(s, 4803)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 500]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x617d, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 118, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4804), byte(3), line(s, 4804)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 102, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4805), byte(3), line(s, 4805)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 202, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4806), byte(2), line(s, 4806)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 206, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(4807), byte(2), line(s, 4807)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x55d3, [-709, 450]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 207, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4808), byte(2), line(s, 4808)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [4, 0]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x91318
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x91318
  s.ins MOV, byte(0), ushort(129), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB02_01 start.', byte(0)
  s.label :game_06_intB02_01_それ以外時のイベント（１回目）
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins LAYERSELECT, 59, 59
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [44, Register.new(218)]
  s.ins MOV, byte(0), ushort(218), 44
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-599, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [42]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x51ea, [210, 0, 0, 0]
  s.ins LAYERSELECT, 58, 58
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [593, Register.new(217)]
  s.ins MOV, byte(0), ushort(217), 593
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ba, [42]
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins CALL, :addr_0x51ea, [210, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 0
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [500]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4809), byte(127), line(s, 4809)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4810), byte(127), line(s, 4810)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4811), byte(127), line(s, 4811)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x91524
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x91524
  s.ins MOV, byte(0), ushort(130), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB02_02 start.', byte(0)
  s.label :game_06_intB02_02_それ以外時のイベント（２回目）
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins LAYERSELECT, 59, 59
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [44, Register.new(218)]
  s.ins MOV, byte(0), ushort(218), 44
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-599, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [36]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x51ea, [180, 0, 0, 0]
  s.ins LAYERSELECT, 58, 58
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [593, Register.new(217)]
  s.ins MOV, byte(0), ushort(217), 593
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ba, [36]
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins CALL, :addr_0x51ea, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 0
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [500]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4812), byte(127), line(s, 4812)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4813), byte(127), line(s, 4813)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4814), byte(127), line(s, 4814)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4815), byte(127), line(s, 4815)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4816), byte(127), line(s, 4816)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4817), byte(127), line(s, 4817)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4818), byte(127), line(s, 4818)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x91838
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x91838
  s.ins MOV, byte(0), ushort(131), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB02_03 start.', byte(0)
  s.label :game_06_intB02_03_それ以外時のイベント（３回目）
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins LAYERSELECT, 59, 59
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [44, Register.new(218)]
  s.ins MOV, byte(0), ushort(218), 44
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-599, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [30]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x51ea, [150, 0, 0, 0]
  s.ins LAYERSELECT, 58, 58
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [593, Register.new(217)]
  s.ins MOV, byte(0), ushort(217), 593
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ba, [30]
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins CALL, :addr_0x51ea, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 0
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [500]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4819), byte(127), line(s, 4819)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4820), byte(127), line(s, 4820)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4821), byte(127), line(s, 4821)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4822), byte(127), line(s, 4822)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4823), byte(127), line(s, 4823)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4824), byte(127), line(s, 4824)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4825), byte(127), line(s, 4825)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4826), byte(127), line(s, 4826)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4827), byte(127), line(s, 4827)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x91b9c
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x91b9c
  s.ins MOV, byte(0), ushort(132), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB02_04 start.', byte(0)
  s.label :game_06_intB02_04_それ以外時のイベント（４回目）
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins LAYERSELECT, 59, 59
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [44, Register.new(218)]
  s.ins MOV, byte(0), ushort(218), 44
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-599, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [24]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x51ea, [120, 0, 0, 0]
  s.ins LAYERSELECT, 58, 58
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [593, Register.new(217)]
  s.ins MOV, byte(0), ushort(217), 593
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ba, [24]
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins CALL, :addr_0x51ea, [120, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 0
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [500]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4828), byte(4), line(s, 4828)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4829), byte(127), line(s, 4829)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4830), byte(4), line(s, 4830)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [5, 100, 0, 1]
  s.ins MSGSET, uint(4831), byte(127), line(s, 4831)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4832), byte(4), line(s, 4832)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4833), byte(4), line(s, 4833)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4834), byte(4), line(s, 4834)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4835), byte(127), line(s, 4835)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4836), byte(4), line(s, 4836)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4837), byte(4), line(s, 4837)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4838), byte(4), line(s, 4838)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x91ff4
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x91ff4
  s.ins MOV, byte(0), ushort(133), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB02_05 start.', byte(0)
  s.label :game_06_intB02_05_それ以外時のイベント（５回目）
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins LAYERSELECT, 59, 59
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [44, Register.new(218)]
  s.ins MOV, byte(0), ushort(218), 44
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-599, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [18]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x51ea, [90, 0, 0, 0]
  s.ins LAYERSELECT, 58, 58
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [593, Register.new(217)]
  s.ins MOV, byte(0), ushort(217), 593
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ba, [18]
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins CALL, :addr_0x51ea, [90, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 0
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [500]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4839), byte(127), line(s, 4839)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4840), byte(127), line(s, 4840)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4841), byte(127), line(s, 4841)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4842), byte(127), line(s, 4842)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins MSGSET, uint(4843), byte(127), line(s, 4843)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4844), byte(127), line(s, 4844)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4845), byte(127), line(s, 4845)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4846), byte(127), line(s, 4846)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4847), byte(127), line(s, 4847)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4848), byte(127), line(s, 4848)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4849), byte(127), line(s, 4849)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4850), byte(127), line(s, 4850)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4851), byte(127), line(s, 4851)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4852), byte(127), line(s, 4852)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4853), byte(127), line(s, 4853)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4854), byte(127), line(s, 4854)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4855), byte(127), line(s, 4855)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4856), byte(127), line(s, 4856)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4857), byte(127), line(s, 4857)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4858), byte(127), line(s, 4858)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x925a0
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x925a0
  s.ins MOV, byte(0), ushort(134), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB02_06 start.', byte(0)
  s.label :game_06_intB02_06_それ以外時のイベント（６回目）
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins LAYERSELECT, 59, 59
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [44, Register.new(218)]
  s.ins MOV, byte(0), ushort(218), 44
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-599, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [12]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x51ea, [60, 0, 0, 0]
  s.ins LAYERSELECT, 58, 58
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [593, Register.new(217)]
  s.ins MOV, byte(0), ushort(217), 593
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ba, [12]
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins CALL, :addr_0x51ea, [60, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 0
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [500]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4859), byte(127), line(s, 4859)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4860), byte(127), line(s, 4860)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins MSGSET, uint(4861), byte(127), line(s, 4861)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4862), byte(127), line(s, 4862)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4863), byte(127), line(s, 4863)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4864), byte(127), line(s, 4864)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4865), byte(127), line(s, 4865)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4866), byte(127), line(s, 4866)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4867), byte(127), line(s, 4867)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 59, 59
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(218)]
  s.ins MOV, byte(0), ushort(218), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 58, 58
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(217)]
  s.ins MOV, byte(0), ushort(217), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x5aaa, []
  s.ins MSGSET, uint(4868), byte(127), line(s, 4868)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4869), byte(127), line(s, 4869)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4870), byte(127), line(s, 4870)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4871), byte(127), line(s, 4871)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4872), byte(127), line(s, 4872)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4873), byte(127), line(s, 4873)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4874), byte(127), line(s, 4874)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4875), byte(127), line(s, 4875)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4876), byte(127), line(s, 4876)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4877), byte(127), line(s, 4877)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4878), byte(127), line(s, 4878)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5be2, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 1000
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4879), byte(127), line(s, 4879)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4880), byte(127), line(s, 4880)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x92c95
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x92c95
  s.ins MOV, byte(0), ushort(135), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB02_07 start.', byte(0)
  s.label :game_06_intB02_07_それ以外時のイベント（７回目）
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins LAYERSELECT, 59, 59
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [44, Register.new(218)]
  s.ins MOV, byte(0), ushort(218), 44
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-599, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [6]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x51ea, [30, 0, 0, 0]
  s.ins LAYERSELECT, 58, 58
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [593, Register.new(217)]
  s.ins MOV, byte(0), ushort(217), 593
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ba, [6]
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins CALL, :addr_0x51ea, [30, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 0
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [500]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4881), byte(127), line(s, 4881)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4882), byte(127), line(s, 4882)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4883), byte(127), line(s, 4883)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4884), byte(127), line(s, 4884)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4885), byte(127), line(s, 4885)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x92f06
  s.ins CALL, :f_reset, []
  s.ins RETSUB
  s.label :addr_0x92f06
  s.ins MOV, byte(0), ushort(136), 1
  s.ins JUMP, :addr_0x87f4
  s.ins DEBUG, 's06_intB02_08 start.', byte(0)
  s.label :game_06_intB02_08_それ以外時のイベント（８回目）
  s.ins CALL, :addr_0x4ed5, [0, 0]
  s.ins CALL, :addr_0x6f17, [3, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins CALL, :addr_0x4eeb, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [88, Register.new(232), 1400]
  s.ins MOV, byte(0), ushort(232), 88
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins LAYERSELECT, 59, 59
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [44, Register.new(218)]
  s.ins MOV, byte(0), ushort(218), 44
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [-599, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins CALL, :addr_0x51ea, [0, 0, 0, 0]
  s.ins LAYERSELECT, 58, 58
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [593, Register.new(217)]
  s.ins MOV, byte(0), ushort(217), 593
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x51ba, [0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins CALL, :addr_0x51ea, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 0
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 73, 73
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(232)]
  s.ins MOV, byte(0), ushort(232), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 376
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [500]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(4886), byte(127), line(s, 4886)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4887), byte(127), line(s, 4887)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(4888), byte(127), line(s, 4888)
  s.ins MSGWAIT, byte(127)
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0x93113
  s.ins CALL, :f_reset, []
  s.ins RETSUB
end
