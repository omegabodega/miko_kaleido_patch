Translations and other source files of an English patch for 幻想牢獄のカレイドスコープ.

- Translation: 神子
- Editing, technical stuff: Neurochitin

Goes together with https://gitlab.com/Neurochitin/kaleido.

# Compilation

You will need:

- Ruby
- ImageMagick
- [Kaleido](https://gitlab.com/Neurochitin/kaleido), branch `miko_kaleido`, cloned locally
- [EnterExtractor](https://github.com/07th-mod/enter_extractor)
- The `data.rom` of 幻想牢獄のカレイドスコープ extracted to a folder tree, for example using EnterExtractor's `blabla.py`, or using [sdu](https://github.com/DCNick3/shin#what-else-is-in-the-box) (`sdu rom extract path/to/data.rom path/to/output_folder`)

Set the constants `KALEIDO_PATH` and `EXTRACT_PATH` in `compile.rb` to the respective folder paths on your system, and `EE_PATH` to the compiled EnterExtractor executable. Then, run

```
ruby compile.rb path/to/patch.rom
```

which will compile the scripts to a `patch.rom` at the given path. Then, copy the `patch.rom` into a suitable RomFS patch folder of your desired Switch emulator or CFW.
