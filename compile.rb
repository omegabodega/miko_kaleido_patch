class String; def c(a); a.nil? ? self : "\e[#{a}m#{self}\e[0m"; end; end
def cl(c, *a); puts a.map.with_index { |e, i| e.to_s.c(c + (i % 2) * 60) }.join; end

require 'fileutils'

# Where the patch.rom file will be written
PATCH_DEST = ARGV[0]

# Kaleido, branch 'miko_kaleido'
KALEIDO_PATH = '../kaleido'

# Compiled executable of EnterExtractor
EE_PATH = '../enter_extractor/build/EnterExtractor'

# Extracted data.rom
EXTRACT_PATH = '../extract'

# Pictures

PICTURES = {
  # Not guilty
  'tg_無罪汎用黒' => 'tg_images/raster/tg_無罪汎用黒.png',
  'tg_無罪汎用赤' => 'tg_images/raster/tg_無罪汎用赤.png',

  # Guilty
  'tg_有罪汎用黒' => 'tg_images/raster/tg_有罪汎用黒.png',
  'tg_有罪汎用赤' => 'tg_images/raster/tg_有罪汎用赤.png',

  # 02_intA02_10; line 869
  'tg_初めてのゲーム開始10_風華' => 'tg_images/raster/tg_初めてのゲーム開始10_風華.png',

  # 04_event03; line 2289
  'tg_event03_風華' => 'tg_images/raster/tg_event03_風華.png',

  # 04_event06; line 2716
  'tg_event06_風華' => 'tg_images/raster/tg_event06_風華.png',

  # 04_event11; line 3193
  'tg_event11_水無' => 'tg_images/raster/tg_event11_水無.png',

  # 04_event12; lines 3239-3240
  'tg_event12_水無1' => 'tg_images/raster/tg_event12_水無1.png',
  'tg_event12_水無2' => 'tg_images/raster/tg_event12_水無2.png',

  # 05_gameB前半06; line 4006
  'tg_gameb前06_風華' => 'tg_images/raster/tg_gameb前06_風華.png',

  # 08_intz02_06; line 5693
  'tg_intz02_06_ボク' => 'tg_images/raster/tg_intz02_06_ボク.png',

  # 08_intz02_08; lines 6127-6128
  'tg_intz02_08_全員' => 'tg_images/raster/tg_intz02_08_全員.png',
  'tg_intz02_08_ト書き' => 'tg_images/raster/tg_intz02_08_ト書き.png'
}

FileUtils.mkdir_p 'rom_source/picture'

PICTURES.each do |k, v|
  if v.nil?
    cl 33, 'Skipping picture ', k, ' (no replacement defined)'
    next
  end

  pic_filename = k + '.pic'
  system(
    EE_PATH, # executable
    File.join(EXTRACT_PATH, 'picture', pic_filename), # source pic
    File.join('rom_source/picture', pic_filename), # destination
    '-replace',
    v # source png
  )
  cl 32, 'Converted ', k, ' to .pic'
end

# GUI

FileUtils.mkdir_p 'gui/txa'

GUI = {
  splash: {
    'caution' => ['gui/splash_caution/clean.png', 'gui/splash_caution/text.png']
  },
  config: {
    'left' => ['gui/config/left.png'],
    'right' => ['gui/config/right_frames.png', 'gui/config/right_text.png'],
    'slider' => ['gui/config/slider_clean.png', 'gui/config/slider_text.png'],
    'category' => ['gui/config/category_clean.png', 'gui/config/category_text.png'],
    'default' => ['gui/config/default_clean.png', 'gui/config/default_text.png'],
    'caption' => ['gui/caption/base.png', 'gui/caption/config.png'],
    'help_config' => ['gui/help/config.png']
  },
  title: {
    'menu_focus' => ['gui/title_menu/menu_base_focus.png', 'gui/title_menu/items.png'],
    'menu_normal' => ['gui/title_menu/menu_base_normal.png', 'gui/title_menu/items.png']
  },
  adv: {
    'sysmenu_focus' => ['gui/adv_sysmenu/menu_base_focus.png', 'gui/adv_sysmenu/items.png'],
    'sysmenu_normal' => ['gui/adv_sysmenu/menu_base_normal.png', 'gui/adv_sysmenu/items.png'],
    'skip' => ['gui/adv_skip/text.png']
  },
  saveload: {
    'caption_save' => ['gui/caption/base.png', 'gui/caption/save.png'],
    'caption_load' => ['gui/caption/base.png', 'gui/caption/load.png'],
    'help_saveload' => ['gui/help/saveload.png']
  },
  logview: {
    'caption' => ['gui/caption/base.png', 'gui/caption/logview.png'],
    'help_logview' => ['gui/help/logview.png']
  },
  cgmode: {
    'caption' => ['gui/caption/base.png', 'gui/caption/cgmode.png'],
    'help_cgmode' => ['gui/help/cgmode.png']
  },
  bupmode: {
    'text' => ['gui/extras/bupmode_text.png'],
    'dressicon' => ['gui/extras/bupmode_dressicon.png'],
    'sizeicon' => ['gui/extras/bupmode_sizeicon.png'],
    'caption' => ['gui/caption/base.png', 'gui/caption/bupmode.png'],
    'help_bupmode' => ['gui/help/bupmode.png']
  },
  bgmmode: {
    'caption' => ['gui/caption/base.png', 'gui/caption/bgmmode.png'],
    'help_bgmmode' => ['gui/help/bgmmode.png']
  }
}

(0..11).each do |i|
  sym = ('cardselect' + i.to_s.rjust(2, '0')).to_sym
  GUI[sym] = {
    'card_judge1' => ['gui/cardselect/card_judge1_clean.png', 'gui/cardselect/judge_text.png'],
    'card_judge2' => ['gui/cardselect/card_judge2_clean.png', 'gui/cardselect/judge_text.png'],
    'card_pierrot' => ['gui/cardselect/card_pierrot_clean.png', 'gui/cardselect/pierrot_text.png'],
    'card_prisoner' => ['gui/cardselect/card_prisoner_clean.png', 'gui/cardselect/prisoner_text.png'],
    'cancel' => ['gui/cardselect/cancel_clean.png', 'gui/cardselect/cancel_text.png']
  }
end

image_cache = {}

GUI.each do |prefix, images|
  prefix = prefix.to_s
  images.each do |name, sources|
    key = sources.join(' ')
    out_path = File.join('gui/txa', prefix + '_' + name + '.png')

    if image_cache.key?(key)
      value = image_cache[key]
      cl 35, 'Retrieving ', out_path, ' from cache (', value, ')'
      FileUtils.cp value, out_path
    else
      cl 34, 'Compositing ', out_path, ' using ImageMagick'

      cmd = ['convert', '-background', 'none']
      cmd += sources
      cmd += ['-layers', 'flatten', '+dither', '-colors', '256', out_path]

      system *cmd
      image_cache[key] = out_path
    end
  end

  txa_filename = prefix + '.txa'
  system(
    EE_PATH,
    File.join(EXTRACT_PATH, txa_filename),
    File.join('rom_source', txa_filename),
    '-replace',
    File.join('gui/txa', prefix)
  )
  cl 32, 'Generated ', txa_filename, ' with ', images.length, ' replaced textures'
end

# Scripts & packaging

SCRIPTS = [
  './dialogue_split.txt',
  './gerokasu_script_00.txt',
  './gerokasu_script_01.txt',
  './gerokasu_script_02.txt',
  './gerokasu_script_03.txt',
  './gerokasu_script_04.txt',
  './gerokasu_script_05.txt',
  './gerokasu_script_06.txt',
  './gerokasu_script_07.txt',
  './gerokasu_script_08.txt',
  './gerokasu_script_09.txt'
]

kaleido_args = [
  'base/main.rb',
  'rom_source',
  PATCH_DEST,
  'tl.snr',
  'font/varela/manifest.rbm',
] + SCRIPTS

kaleido_cmd = ['ruby', 'kal_tl.rb'] + kaleido_args.map { |e| File.expand_path(e) }
cl 34, 'Running kaleido...'

Dir.chdir(File.join(KALEIDO_PATH, 'rom-repack')) do
  system *kaleido_cmd
end
