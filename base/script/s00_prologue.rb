require_relative '../instructions.rb'
require_relative '../registers.rb'

def write_s00_prologue(s)
  s.ins DEBUG, 's00_prologue start.', byte(0)
  s.label :game_00_prologue
  s.ins CALL, :f_reset, []
  s.ins EVBEGIN, 0
  s.ins LAYERLOAD, 255, byte(1), 0, 31, 65535, -959, -539, 1920, 1080
  s.ins LAYERCTRL, 255, 5, byte(0)
  s.ins LAYERCTRL, 255, 9, byte(0)
  s.ins LAYERCTRL, 255, 9, byte(3), 1000, 30
  s.ins LAYERWAIT, 255, byte(1), 9
  s.ins LAYERLOAD, 255, byte(6), byte(2), byte(2), 1000
  s.ins MOVIEWAIT, 255, byte(2)
  s.ins LAYERLOAD, 255, byte(1), 2, 31, 65535, -959, -539, 1920, 1080
  s.ins LAYERCTRL, 255, 9, byte(2), 30
  s.ins LAYERWAIT, 255, byte(1), 9
  s.ins LAYERUNLOAD, 255, byte(0)
  s.ins EVEND
  s.ins WAIT, byte(0), 60
  s.ins SAVEINFO, byte(0), ''
  s.ins SAVEINFO, byte(1), 'Prologue'
  s.ins AUTOSAVE
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, 4]
  s.ins CALL, :f_se_play, [3, 69, 70, 3000, 0]
  s.ins CALL, :f_bg_related, [3, 250, 250, 250, 0, 0, 0, -1999, 1500, 1500]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 1000
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :f_show_bustup_sprite, [0, 0, 1, 14, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(1), byte(0), line(s, 1)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 1000
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :f_show_bustup_sprite, [2, 0, 1, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [2, 0]
  s.ins MSGSET, uint(2), byte(2), line(s, 2)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), 0
  s.ins MOV, byte(0), ushort(R_default_transition_duration), 1000
  s.ins MOV, byte(0), ushort(65), 0
  s.ins MOV, byte(0), ushort(66), -99
  s.ins CALL, :f_show_bustup_sprite, [3, 0, 1, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [3, 0]
  s.ins MSGSET, uint(3), byte(3), line(s, 3)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 28, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, 1, 128, 1, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x51ba, [5]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(4), byte(1), line(s, 4)
  s.ins MSGSYNC, byte(0), 1077
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 250]
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5720, []
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x51ba, [5]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 28, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [80, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [120, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :f_se_play, [0, 20, 100, 0, 0]
  s.ins MSGSET, uint(5), byte(1), line(s, 5)
  s.ins MSGSYNC, byte(0), 1994
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [12, 35, 70]
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [220, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 29, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [35, 35, 500]
  s.ins MSGSET, uint(6), byte(2), line(s, 6)
  s.ins MSGSYNC, byte(0), 2112
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6ccf, [45, 500, 250]
  s.ins MSGSYNC, byte(0), 3904
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 129, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSYNC, byte(0), 5354
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6d32, [15, 120, 240]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 1, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(7), byte(3), line(s, 7)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 123, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x6c90, [50, 400, 1, 200]
  s.ins MSGSET, uint(8), byte(2), line(s, 8)
  s.ins MSGSYNC, byte(0), 3328
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 109, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_se_play, [0, 19, 100, 0, 0]
  s.ins MSGSET, uint(9), byte(0), line(s, 9)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 9, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [700, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-699, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [1, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-249, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :f_bgm_play, [3, 100, 0, 1]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(10), byte(127), line(s, 10)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(11), byte(127), line(s, 11)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-299, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [3000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [280, 0, 0, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [300, 10000, 12288, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(12), byte(127), line(s, 12)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(13), byte(127), line(s, 13)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 20, 100, 0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(14), byte(127), line(s, 14)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [0, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x527a, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [220, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [1]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, 0, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(15), byte(1), line(s, 15)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(16), byte(1), line(s, 16)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 9, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(17), byte(2), line(s, 17)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins MSGSET, uint(18), byte(3), line(s, 18)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x6bb1, [0, 1]
  s.ins MSGSET, uint(19), byte(0), line(s, 19)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x604e, [1800]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1800
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x51ea, [64, 0, 0, 0]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x51ea, [64, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :addr_0x6bb1, [0, 1]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [5, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins CALL, :addr_0x51ba, [0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(20), byte(127), line(s, 20)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(21), byte(127), line(s, 21)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(22), byte(127), line(s, 22)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 45, 45
  s.ins MOV, byte(0), ushort(20), 1
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 46, 46
  s.ins MOV, byte(0), ushort(20), 1
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, :null, :null, 2, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(23), byte(127), line(s, 23)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x60a7, []
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [3, 4]
  s.ins CALL, :addr_0x5670, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 9, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [700, 0, 0, 0]
  s.ins CALL, :addr_0x51ea, [0, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 128, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-249, 0, 0, 0]
  s.ins CALL, :addr_0x51ea, [0, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 113, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-699, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(24), byte(1), line(s, 24)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(25), byte(1), line(s, 25)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(26), byte(3), line(s, 26)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 123, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(27), byte(0), line(s, 27)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 9, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(28), byte(2), line(s, 28)
  s.ins MSGSYNC, byte(0), 8192
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 69, 70, 1000, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, 3]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-1199, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-1399, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [130, 0, 0, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [4500, 60000, 12288, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 800
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(29), byte(127), line(s, 29)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(30), byte(127), line(s, 30)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(31), byte(127), line(s, 31)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 114, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(32), byte(1), line(s, 32)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(33), byte(3), line(s, 33)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_se_play, [4, 68, 70, 1000, 0]
  s.ins MSGSET, uint(34), byte(127), line(s, 34)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(35), byte(127), line(s, 35)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(36), byte(127), line(s, 36)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 300
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [3000, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins CALL, :addr_0x527a, []
  s.ins MSGSET, uint(37), byte(127), line(s, 37)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(38), byte(127), line(s, 38)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [4, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 30, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-449, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(39), byte(2), line(s, 39)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 15, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [450, 0, 0, 0]
  s.ins MSGSET, uint(40), byte(0), line(s, 40)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 2000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [2500, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [1400, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 0, 0, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [4200, 60000, 12288, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 500
  s.ins MOV, byte(0), ushort(13), 10
  s.ins MOV, byte(0), ushort(14), 768
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins MSGSET, uint(41), byte(127), line(s, 41)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(42), byte(127), line(s, 42)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(43), byte(127), line(s, 43)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(44), byte(127), line(s, 44)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(45), byte(127), line(s, 45)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(46), byte(127), line(s, 46)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(47), byte(127), line(s, 47)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 3, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(48), byte(3), line(s, 48)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(49), byte(3), line(s, 49)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 107, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(50), byte(1), line(s, 50)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(51), byte(2), line(s, 51)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-6499, 1500, 5, -1]
  s.ins CALL, :f_set_sprite_y_offset, [700, 1500, 5, -1]
  s.ins CALL, :f_set_sprite_zoom, [220, 1500, 5, -1]
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(52), byte(0), line(s, 52)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [100, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [80, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-10, 300, 131, 0]
  s.ins MSGSET, uint(53), byte(1), line(s, 53)
  s.ins MSGSYNC, byte(0), 1312
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [5, 5, 1000]
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(54), byte(3), line(s, 54)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [5, 5, 1000]
  s.ins MSGSET, uint(55), byte(127), line(s, 55)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(56), byte(127), line(s, 56)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(57), byte(127), line(s, 57)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 80, 50, 500, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [150, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(58), byte(127), line(s, 58)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(59), byte(127), line(s, 59)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :f_se_play, [0, 82, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [4850]
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins MOV, byte(0), ushort(11), 1
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(13), 61
  s.ins MOV, byte(0), ushort(14), 501
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [6, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-299, 100, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-4199, 100, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [250, 100, 0, 0]
  s.ins CALL, :addr_0x51dc, [10]
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_sprite_transform, [200, 200, 1500]
  s.ins CALL, :f_se_play, [1, 74, 100, 0, 0]
  s.ins CALL, :f_se_play, [2, 77, 100, 0, 0]
  s.ins CALL, :addr_0x4df4, [1650]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x51dc, [0]
  s.ins MSGSET, uint(60), byte(127), line(s, 60)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [3, 70, 100, 1000, 0]
  s.ins MSGSET, uint(61), byte(127), line(s, 61)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 30, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(62), byte(2), line(s, 62)
  s.ins MSGSYNC, byte(0), 5034
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 2000, 5, -1]
  s.ins CALL, :addr_0x5265, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 23, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(63), byte(0), line(s, 63)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(64), byte(1), line(s, 64)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-499, 1000, 5, -1]
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(65), byte(3), line(s, 65)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_fadeout, [3, 1000]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins CALL, :f_bgm_play, [1, 100, 0, 1]
  s.ins MSGSET, uint(66), byte(127), line(s, 66)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(67), byte(127), line(s, 67)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(68), byte(127), line(s, 68)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(69), byte(127), line(s, 69)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 109, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins MSGSET, uint(70), byte(0), line(s, 70)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 128, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins MSGSET, uint(71), byte(2), line(s, 71)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [-999, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [-599, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [200, 0, 0, 0]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_x_offset, [200, 30000, 12288, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(72), byte(127), line(s, 72)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(73), byte(127), line(s, 73)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(74), byte(127), line(s, 74)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(75), byte(127), line(s, 75)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [7, :null]
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [0, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [100, 0, 0, 0]
  s.ins CALL, :addr_0x527a, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(76), byte(3), line(s, 76)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 103, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins MSGSET, uint(77), byte(1), line(s, 77)
  s.ins MSGSYNC, byte(0), 3456
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 127, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(78), byte(0), line(s, 78)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 128, 1, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(79), byte(2), line(s, 79)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5892, [451, 0, 0, 100]
  s.ins MSGSET, uint(80), byte(4), line(s, 80)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(81), byte(127), line(s, 81)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(82), byte(127), line(s, 82)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(83), byte(127), line(s, 83)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(84), byte(4), line(s, 84)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(85), byte(0), line(s, 85)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(86), byte(4), line(s, 86)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5016, []
  s.ins LAYERSELECT, 6, 6
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 9, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 115, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-574, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(87), byte(3), line(s, 87)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 40, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(88), byte(1), line(s, 88)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 7, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(89), byte(2), line(s, 89)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(90), byte(127), line(s, 90)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(91), byte(127), line(s, 91)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 114, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(92), byte(1), line(s, 92)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 9, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(93), byte(2), line(s, 93)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(94), byte(3), line(s, 94)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 16, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [0, 0, 0, 0]
  s.ins CALL, :addr_0x55d3, [0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(95), byte(0), line(s, 95)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(96), byte(0), line(s, 96)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :addr_0x5670, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 18, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(97), byte(1), line(s, 97)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 9, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(98), byte(3), line(s, 98)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 128, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(99), byte(2), line(s, 99)
  s.ins MSGSYNC, byte(0), 2944
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 11, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [575, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(100), byte(0), line(s, 100)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 117, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(101), byte(1), line(s, 101)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(102), byte(0), line(s, 102)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins CALL, :addr_0x4eba, [0, 0]
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(103), byte(4), line(s, 103)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_bgm_play, [4, 100, 0, 1]
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(104), byte(127), line(s, 104)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(105), byte(127), line(s, 105)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 139, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(106), byte(1), line(s, 106)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 15, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(107), byte(0), line(s, 107)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(108), byte(4), line(s, 108)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(109), byte(4), line(s, 109)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 112, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(110), byte(2), line(s, 110)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(111), byte(4), line(s, 111)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(112), byte(3), line(s, 112)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 16, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(113), byte(0), line(s, 113)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(114), byte(4), line(s, 114)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(115), byte(4), line(s, 115)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(116), byte(4), line(s, 116)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(117), byte(1), line(s, 117)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 110, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(118), byte(1), line(s, 118)
  s.ins MSGSYNC, byte(0), 5568
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 120, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGCLOSE, byte(1)
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-749, 1500, 5, -1]
  s.ins CALL, :addr_0x5265, []
  s.ins MSGSET, uint(119), byte(127), line(s, 119)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(120), byte(127), line(s, 120)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(121), byte(127), line(s, 121)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(122), byte(4), line(s, 122)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(123), byte(4), line(s, 123)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(124), byte(127), line(s, 124)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(125), byte(127), line(s, 125)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(126), byte(127), line(s, 126)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(127), byte(127), line(s, 127)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(128), byte(4), line(s, 128)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x5952, []
  s.ins CALL, :addr_0x4df4, [4000]
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MSGSET, uint(129), byte(127), line(s, 129)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 80, 80
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(239)]
  s.ins MOV, byte(0), ushort(239), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 81, 81
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(240)]
  s.ins MOV, byte(0), ushort(240), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 82, 82
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(241)]
  s.ins MOV, byte(0), ushort(241), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 83, 83
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(242)]
  s.ins MOV, byte(0), ushort(242), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(130), byte(127), line(s, 130)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 106, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(131), byte(3), line(s, 131)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(132), byte(0), line(s, 132)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(133), byte(4), line(s, 133)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERCTRL, -6, 5, byte(1), 1900
  s.ins LAYERSELECT, 78, 78
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [581, Register.new(237), 1400]
  s.ins MOV, byte(0), ushort(237), 581
  s.ins CALL, :f_set_sprite_x_offset, [-219, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-20, 0, 0, 0]
  s.ins LAYERSELECT, 79, 79
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [582, Register.new(238), 1400]
  s.ins MOV, byte(0), ushort(238), 582
  s.ins CALL, :f_set_sprite_x_offset, [220, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-20, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(134), byte(4), line(s, 134)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 78, 78
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(237)]
  s.ins MOV, byte(0), ushort(237), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 79, 79
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(238)]
  s.ins MOV, byte(0), ushort(238), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 76, 76
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [580, Register.new(235), 1400]
  s.ins MOV, byte(0), ushort(235), 580
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-20, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(135), byte(4), line(s, 135)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 76, 76
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(235)]
  s.ins MOV, byte(0), ushort(235), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 77, 77
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [583, Register.new(236), 1400]
  s.ins MOV, byte(0), ushort(236), 583
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-20, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(136), byte(4), line(s, 136)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 78, 78
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [581, Register.new(237), 1400]
  s.ins MOV, byte(0), ushort(237), 581
  s.ins CALL, :f_set_sprite_x_offset, [-659, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-20, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins LAYERSELECT, 79, 79
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [582, Register.new(238), 1400]
  s.ins MOV, byte(0), ushort(238), 582
  s.ins CALL, :f_set_sprite_x_offset, [-219, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-20, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1500
  s.ins LAYERSELECT, 76, 76
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [580, Register.new(235), 1400]
  s.ins MOV, byte(0), ushort(235), 580
  s.ins CALL, :f_set_sprite_x_offset, [220, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-20, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins LAYERSELECT, 77, 77
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [583, Register.new(236), 1400]
  s.ins MOV, byte(0), ushort(236), 583
  s.ins CALL, :f_set_sprite_x_offset, [660, 0, 0, 0]
  s.ins CALL, :f_set_sprite_y_offset, [100, 0, 0, 0]
  s.ins CALL, :addr_0x5153, [-20, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(137), byte(4), line(s, 137)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(138), byte(4), line(s, 138)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 78, 78
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [581, Register.new(237), 1400]
  s.ins MOV, byte(0), ushort(237), 581
  s.ins CALL, :addr_0x5153, [0, 400, 5, -1]
  s.ins LAYERCTRL, -6, 5, byte(1), 1200
  s.ins LAYERSELECT, 79, 79
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [582, Register.new(238), 1400]
  s.ins MOV, byte(0), ushort(238), 582
  s.ins CALL, :addr_0x5153, [0, 400, 5, -1]
  s.ins LAYERCTRL, -6, 5, byte(1), 1100
  s.ins MSGSET, uint(139), byte(4), line(s, 139)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(140), byte(4), line(s, 140)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(141), byte(4), line(s, 141)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 76, 76
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [580, Register.new(235), 1400]
  s.ins MOV, byte(0), ushort(235), 580
  s.ins CALL, :addr_0x5153, [0, 400, 5, -1]
  s.ins LAYERCTRL, -6, 5, byte(1), 1000
  s.ins LAYERSELECT, 77, 77
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [583, Register.new(236), 1400]
  s.ins MOV, byte(0), ushort(236), 583
  s.ins CALL, :addr_0x5153, [0, 400, 5, -1]
  s.ins LAYERCTRL, -6, 5, byte(1), 900
  s.ins MSGSET, uint(142), byte(4), line(s, 142)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 78, 78
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [581, Register.new(237), 1400]
  s.ins MOV, byte(0), ushort(237), 581
  s.ins LAYERCTRL, -6, 5, byte(1), 1600
  s.ins LAYERSELECT, 79, 79
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [582, Register.new(238), 1400]
  s.ins MOV, byte(0), ushort(238), 582
  s.ins LAYERCTRL, -6, 5, byte(1), 1500
  s.ins LAYERSELECT, 77, 77
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [583, Register.new(236), 1400]
  s.ins MOV, byte(0), ushort(236), 583
  s.ins CALL, :f_set_sprite_x_offset, [220, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins CALL, :addr_0x51ea, [255, 255, 255, 255]
  s.ins LAYERSELECT, 76, 76
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [580, Register.new(235), 1400]
  s.ins MOV, byte(0), ushort(235), 580
  s.ins CALL, :f_set_sprite_x_offset, [660, 0, 0, 0]
  s.ins LAYERCTRL, -6, 5, byte(1), 1300
  s.ins CALL, :addr_0x51ea, [255, 255, 255, 255]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 0
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [100]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 77, 77
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [583, Register.new(236), 1400]
  s.ins MOV, byte(0), ushort(236), 583
  s.ins CALL, :addr_0x51ea, [0, 0, 0, 0]
  s.ins LAYERSELECT, 76, 76
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [580, Register.new(235), 1400]
  s.ins MOV, byte(0), ushort(235), 580
  s.ins CALL, :addr_0x51ea, [0, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [400]
  s.ins MSGSET, uint(143), byte(4), line(s, 143)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(144), byte(4), line(s, 144)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(145), byte(4), line(s, 145)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(146), byte(4), line(s, 146)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(147), byte(4), line(s, 147)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(148), byte(4), line(s, 148)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(149), byte(4), line(s, 149)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(150), byte(4), line(s, 150)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 1
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(151), byte(127), line(s, 151)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(152), byte(127), line(s, 152)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(153), byte(127), line(s, 153)
  s.ins MSGWAIT, byte(127)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 32
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(154), byte(127), line(s, 154)
  s.ins MSGWAIT, byte(127)
  s.ins MSGCLOSE, byte(0)
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx11), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MOV, byte(0), ushort(R_text_positioning_mode_rx12), 0
  s.ins CALL, :f_update_text_positioning_mode, []
  s.ins MSGSET, uint(155), byte(4), line(s, 155)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 113, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(156), byte(1), line(s, 156)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(157), byte(4), line(s, 157)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(158), byte(4), line(s, 158)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :f_se_play, [0, 57, 100, 0, 0]
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 78, 78
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(237)]
  s.ins MOV, byte(0), ushort(237), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 79, 79
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(238)]
  s.ins MOV, byte(0), ushort(238), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 76, 76
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(235)]
  s.ins MOV, byte(0), ushort(235), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 77, 77
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(236)]
  s.ins MOV, byte(0), ushort(236), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5089, [500, Register.new(244), 1400]
  s.ins MOV, byte(0), ushort(244), 500
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x5038, [531, Register.new(179)]
  s.ins MOV, byte(0), ushort(179), 531
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERCTRL, -6, 5, byte(1), 1400
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(159), byte(127), line(s, 159)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(160), byte(127), line(s, 160)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(161), byte(127), line(s, 161)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(162), byte(127), line(s, 162)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 85, 85
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x50cf, [Register.new(244)]
  s.ins MOV, byte(0), ushort(244), -1
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins LAYERSELECT, 20, 20
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :addr_0x506d, [Register.new(179)]
  s.ins MOV, byte(0), ushort(179), -1
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERUNLOAD, -6, byte(1)
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(163), byte(4), line(s, 163)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 2, :null, 121, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(164), byte(3), line(s, 164)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 2, :null, 24, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 200
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(165), byte(2), line(s, 165)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(166), byte(4), line(s, 166)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 2, :null, 28, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(167), byte(1), line(s, 167)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 13, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(168), byte(1), line(s, 168)
  s.ins MSGSYNC, byte(0), 1653
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 22, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(169), byte(127), line(s, 169)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(170), byte(127), line(s, 170)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [700, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-699, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-249, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 109, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [1, 1, 1]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [1, 1, 1]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [1, 1, 1]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :addr_0x532a, [1, 1, 1]
  s.ins MSGSET, uint(171), byte(127), line(s, 171)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(172), byte(127), line(s, 172)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(173), byte(127), line(s, 173)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(174), byte(127), line(s, 174)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_hide_all_sprites, []
  s.ins LAYERSELECT, 0, 0
  s.ins MOV, byte(0), ushort(20), 1
  s.ins CALL, :f_set_sprite_y_offset, [-2499, 0, 0, 0]
  s.ins CALL, :f_set_sprite_zoom, [180, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(175), byte(4), line(s, 175)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 2, :null, 17, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(176), byte(0), line(s, 176)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 116, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(177), byte(0), line(s, 177)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 64, :null, :null, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(178), byte(4), line(s, 178)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(179), byte(127), line(s, 179)
  s.ins MSGWAIT, byte(127)
  s.ins MSGSET, uint(180), byte(127), line(s, 180)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4e7c, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [:null, :null]
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [700, 0, 0, 0]
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 139, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-699, 0, 0, 0]
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, :null, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [-249, 0, 0, 0]
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 108, :null, 1, 1]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins CALL, :f_set_sprite_x_offset, [250, 0, 0, 0]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 400
  s.ins CALL, :addr_0x4e83, []
  s.ins MSGSET, uint(181), byte(0), line(s, 181)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 109, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(182), byte(1), line(s, 182)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 4, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(183), byte(3), line(s, 183)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 4, 4
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [3, 0, :null, 20, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(184), byte(3), line(s, 184)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 11, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(185), byte(0), line(s, 185)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 22, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(186), byte(2), line(s, 186)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 25, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(187), byte(2), line(s, 187)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 3, 3
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [2, 0, :null, 212, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(188), byte(2), line(s, 188)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 106, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(189), byte(1), line(s, 189)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 2, 2
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [1, 0, :null, 9, :null, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(190), byte(1), line(s, 190)
  s.ins MSGWAIT, byte(127)
  s.ins LAYERSELECT, 1, 1
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_bustup_sprite, [0, 0, :null, 108, 0, :null, 0]
  s.ins MOV, byte(0), ushort(20), 0
  s.ins MSGSET, uint(191), byte(0), line(s, 191)
  s.ins MSGWAIT, byte(127)
  s.ins CALL, :addr_0x4eba, [2000, 0]
  s.ins MSGCLOSE, byte(1)
  s.ins CALL, :addr_0x4e7c, []
  s.ins CALL, :f_init_graphics, []
  s.ins SESTOPALL, 0
  s.ins CALL, :addr_0x4eeb, []
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 100
  s.ins MOV, byte(0), ushort(R_rx3f_transition_related), -1
  s.ins CALL, :f_show_background, [0, :null]
  s.ins MOV, byte(0), ushort(11), 0
  s.ins MOV, byte(0), ushort(12), 2000
  s.ins CALL, :addr_0x4e83, []
  s.ins WIPEWAIT
  s.ins CALL, :addr_0x4df4, [1000]
  s.ins EVBEGIN, 0
  s.ins LAYERLOAD, 255, byte(6), byte(2), byte(3), 1, 1000
  s.ins MOVIEWAIT, 255, byte(2)
  s.ins LAYERUNLOAD, 255, byte(0)
  s.ins EVEND
  s.ins WAIT, byte(0), 60
  s.ins JUMP_COND, byte(2), Register.new(0), 0, :addr_0xe942
  s.ins CALL, :f_reset, []
  s.ins RETSUB
end
